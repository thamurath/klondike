#ifndef _KLONDIKE_MODEL_CARD_HPP__
#define _KLONDIKE_MODEL_CARD_HPP__



#include <better_enums.hpp>
#include <ostream>

namespace com::jlom::klondike::model
{
    template<typename T>
    struct CardTraits;

    BETTER_ENUM(FACE, std::uint8_t
                , UP
                , DOWN
    );

    template <typename SpecificCard>
    class Card
    {
      public:
        typedef typename CardTraits<SpecificCard>::FigureType FigureType;
        typedef typename CardTraits<SpecificCard>::SuitType SuitType;
        typedef typename CardTraits<SpecificCard>::ColorType ColorType;

        Card(void) ;
        virtual ~Card(void);

        auto GetFigure(void) const
        {
            return static_cast<SpecificCard const*>(this)->getFigure();
        }
        auto GetSuit(void) const
        {
            return static_cast<SpecificCard const*>(this)->getSuit();
        }
        auto GetColor(void) const
        {
            return static_cast<SpecificCard const*>(this)->getColor();
        }

        bool IsFaceUp(void) const;
        void TurnUp(void);
        void TurnDown(void);

      private:
        FACE face;
    };
    template <typename SpecificCard>
    std::ostream& operator<<(std::ostream& os, com::jlom::klondike::model::Card<SpecificCard> const& card);
}  // namespace com::jlom::klondike::model


#include "src/Card_impl.hpp"

#endif  //_KLONDIKE_MODEL_CARD_HPP__
