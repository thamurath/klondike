#ifndef KLONDIKE_PILEVIEW_HPP
#define KLONDIKE_PILEVIEW_HPP

#include "OutView.hpp"

#include "CardView.hpp"

#include <vector>
#include <string>

namespace com::jlom::klondike::view
{
    class PileView : public OutView
    {
    public:
        explicit PileView(std::string id);
        virtual ~PileView() =default;

        void DecodeStatus(StatusDecoder const &statusDecoder, Status const &status) override;
        void Show(void) override ;

    private:
        std::vector<CardView> cards;
    };
}
#endif //KLONDIKE_PILEVIEW_HPP
