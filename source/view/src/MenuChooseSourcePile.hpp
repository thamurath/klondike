#ifndef KLONDIKE_MENUCHOOSESOURCEPILE_HPP
#define KLONDIKE_MENUCHOOSESOURCEPILE_HPP

#include "MenuChoosePile.hpp"

namespace com::jlom::klondike::view
{
    class MenuChooseSourcePile: public MenuChoosePile
    {
    private:
        void DecodeOption(MenuContext& menuContext) override ;
    };

}

#endif //KLONDIKE_MENUCHOOSESOURCEPILE_HPP
