#ifndef KLONDIKE_MODEL_FOUNDATION_IMPL_HPP
#define KLONDIKE_MODEL_FOUNDATION_IMPL_HPP

#ifndef __KLONDIKE_MODEL_FOUNDATION_HPP__
#error "Should not include this file"
#endif

#include "better_enums.hpp"

namespace com::jlom::klondike::model
{

    template <typename Card, template <typename C> class Heap>
    auto Foundation<Card, Heap>::IsEmpty() const
    {
        return heap.IsEmpty();
    }

    template <typename Card, template <typename C> class Heap>
    auto Foundation<Card, Heap>::Size(void) const
    {
        return heap.Size();
    }

    template <typename Card, template <typename C> class Heap>
    auto Foundation<Card, Heap>::Get(void) const
    {
        assert(!heap.IsEmpty());
        return heap.Get();
    }

    template <typename Card, template <typename C> class Heap>
    auto Foundation<Card, Heap>::GetCards(std::size_t const& numCards) const
    {
        assert (heap.Size() <= numCards);
        return heap.GetCards(numCards);
    };

    template <typename Card, template <typename C> class Heap>
    void Foundation<Card, Heap>::Pop(void)
    {
        assert(false);
    };

    template <typename Card, template <typename C> class Heap>
    auto Foundation<Card, Heap>::Push(Card&& card)
    {
        if (heap.IsEmpty())
        {
            assert(First<typename Card::FigureType>() == card.GetFigure());
        }
        else
        {
            assert(card.GetSuit() == heap.Get()->GetSuit());
            assert(isNext<typename Card::FigureType>(card.GetFigure(), heap.Get()->GetFigure()));
        }
        return heap.Push(std::move(card));
    }

    template <typename Card, template <typename C> class Heap>
    template <typename StatusEncoder>
    auto Foundation<Card, Heap>::EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const
    {
        assert(nullptr != statusEncoder);
        return heap.EncodeStatus(id, statusEncoder);
    }

    /// Transactional Interface
    template <typename Card, template <typename C> class Heap>
    typename Foundation<Card, Heap>::state_type Foundation<Card, Heap>::begin(void) const
    {
        return Foundation<Card,Heap>::state_type(heap.GetAllCards());
    }

    template <typename Card, template <typename C> class Heap>
    void Foundation<Card, Heap>::commit(void)
    {
        //nothing to do
    }

    template <typename Card, template <typename C> class Heap>
    void Foundation<Card, Heap>::rollback(typename Foundation<Card, Heap>::state_type const& state)
    {
        auto cards = state.cards;
        heap.Reset(std::move(cards));
    }

}//namespace com::jlom::klondike::model

#endif //KLONDIKE_FOUNDATION_IMPL_HPP
