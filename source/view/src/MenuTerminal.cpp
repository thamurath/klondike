#include "MenuTerminal.hpp"

namespace com::jlom::klondike::view
{

    Command MenuTerminal::Handle(MenuContext &menuContext)
    {
        return menuContext.Builder().Build();
    }
}