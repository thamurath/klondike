#include "Action.hpp"


namespace com::jlom::klondike
{
    Action::Action(ACTION_TYPE type
            , std::string const& sourceId
            , std::string const& destinationId
            , std::size_t const& numCards)
    : sourceId(sourceId)
    , destinationId(destinationId)
    , numCards(numCards)
    , type(type)
    {

    }

    std::string Action::GetSourceId(void) const
    {
        return sourceId;
    }
    std::string Action::GetDestinationId(void) const
    {
        return destinationId;
    }
    std::size_t Action::GetNumCards(void) const
    {
        return numCards;
    }

    ACTION_TYPE Action::GetType(void) const
    {
        return type;
    }


}
