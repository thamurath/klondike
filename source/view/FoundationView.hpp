#ifndef KLONDIKE_FOUNDATIONVIEW_HPP
#define KLONDIKE_FOUNDATIONVIEW_HPP

#include "OutView.hpp"


#include "CardView.hpp"

#include <string>
#include <optional.hpp>
namespace com::jlom::klondike::view
{
    class FoundationView : public OutView
    {
    public:
        explicit FoundationView(std::string id);
        virtual ~FoundationView() = default;

        void DecodeStatus(StatusDecoder const &statusDecoder, Status const &status) override;
        void Show(void) override ;

    private:
        std::optional<CardView> topCard;
        std::uint32_t cardsFaceDown;
    };
}

#endif //KLONDIKE_FOUNDATIONVIEW_HPP
