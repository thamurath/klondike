#ifndef KLONDIKE_COMMANDBUILDER_HPP
#define KLONDIKE_COMMANDBUILDER_HPP

#include <common/Command.hpp>
#include <common/ElementTypes.hpp>

#include <optional>

namespace com::jlom::klondike::view
{
    class CommandBuilder final
    {
    public:
        CommandBuilder(void) = default;
        ~CommandBuilder() = default;

        // no copy
        CommandBuilder(CommandBuilder const& ) = delete;
        CommandBuilder& operator=(CommandBuilder const& ) = delete;

        // movement
        CommandBuilder(CommandBuilder&& ) = default;
        CommandBuilder& operator=(CommandBuilder&& ) = default;

        CommandBuilder& SetType(const CommandType &type);
        CommandBuilder& SetSource(std::string const& src);
        CommandBuilder& SetDestination(std::string const& dst);
        CommandBuilder& SetSourceType(ElementType const& srcType);
        CommandBuilder& SetDestinationType(ElementType const& dstType);
        CommandBuilder& SetNumberOfCards(std::size_t const& numCards);

        Command Build(void);



        std::optional<std::string> GetSource(void) const;

    private:
        std::optional<CommandType> type;
        std::optional<std::string> src;
        std::optional<std::string> dst;
        std::optional<ElementType> srcType;
        std::optional<ElementType> dstType;
        std::size_t numCards;
    };
}

#endif //KLONDIKE_COMMANDBUILDER_HPP
