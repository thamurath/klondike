#ifndef KLONDIKE_MODEL_DEFAULTFACTORY_IMPL_HPP
#define KLONDIKE_MODEL_DEFAULTFACTORY_IMPL_HPP

#ifndef _KLONDIKE_MODEL_DEFAULT_FACTORY_HPP__
#error "Should not include this file directly"
#endif

namespace com::jlom::klondike::model
{
    template <typename Card, typename StatusEncoder, typename InformationHub>
    typename DefaultFactory<Card, StatusEncoder, InformationHub>::TableType
    DefaultFactory<Card, StatusEncoder, InformationHub>::createTable(DefaultFactory const& factory, StatusEncoder* encoder, InformationHub* hub) const
    {
        return DefaultFactory::TableType(factory, encoder, hub);
    }
    template<typename Card, typename StatusEncoder, typename InformationHub>
    typename DefaultFactory<Card, StatusEncoder, InformationHub>::DeckType
    DefaultFactory<Card, StatusEncoder, InformationHub>::createDeck(void) const
    {
        std::vector<CardType> deckCards;
        for (SuitType suit : SuitType::_values())
        {
            for (FigureType figure : FigureType::_values())
            {
                deckCards.emplace_back( figure, suit);
            }
        }
        return static_cast<DefaultFactory::DeckType>(std::move(deckCards));
    }

    template <typename Card, typename StatusEncoder, typename InformationHub>
    typename DefaultFactory<Card, StatusEncoder, InformationHub>::PileType
    DefaultFactory<Card, StatusEncoder, InformationHub>::createPile(std::uint8_t const& id
                                                                  , std::vector<CardType>&& cards) const
    {
        return PileType(id, std::move(cards));
    }

    template<typename Card, typename StatusEncoder, typename InformationHub>
    typename DefaultFactory<Card, StatusEncoder, InformationHub>::FoundationType
    DefaultFactory<Card, StatusEncoder, InformationHub>::createFoundation(void) const
    {
        return FoundationType();
    }

    template <typename Card, typename StatusEncoder, typename InformationHub>
    typename DefaultFactory<Card, StatusEncoder, InformationHub>::WasteType
    DefaultFactory<Card, StatusEncoder, InformationHub>::createWaste(void) const
    {
        return WasteType();
    }
}

#endif //KLONDIKE_MODEL_DEFAULTFACTORY_IMPL_HPP
