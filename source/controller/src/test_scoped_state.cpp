#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "ScopedState.hpp"

#include "test_dummyTable.hpp"

struct Card{};

TEST_CASE("ScopedState: Rollback", "tools, scopedState")
{
    namespace controller = com::jlom::klondike::controller;
    namespace test = controller::unittest;

    const std::string id("uno");
    using Table_t = test::Table<Card>;
    Table_t table;

    {
        controller::ScopedState scopedState(table, std::string(id), table.Begin(id));

    }
    CHECK( table.GetCounterTable(Table_t::OK_BEGIN_COUNTER).size() == 1);
    CHECK( table.GetCounterTable(Table_t::OK_ROLLBACK_COUNTER).size() == 1);
    CHECK( table.GetCounterTable(Table_t::TOTAL_COMMIT_COUNTER).size() == 0);
    INFO(table );
}

TEST_CASE("ScopedState: Commit", "tools, scopedState")
{
    namespace controller = com::jlom::klondike::controller;
    namespace test = controller::unittest;

    const std::string id = "uno";

    using Table_t = test::Table<Card>;
    Table_t table;

    {
        controller::ScopedState scopedState(table, id, table.Begin(id));
        scopedState.Deactivate();
    }
    CHECK( table.GetCounterTable(Table_t::OK_BEGIN_COUNTER).size() == 1);
    CHECK( table.GetCounterTable(Table_t::OK_COMMIT_COUNTER).size() == 1);
    CHECK( table.GetCounterTable(Table_t::TOTAL_ROLLBACK_COUNTER).size() == 0);
    INFO(table );
}

