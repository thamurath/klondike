#ifndef KLONDIKE_TYPE_TRAITS_HPP
#define KLONDIKE_TYPE_TRAITS_HPP

#include <type_traits>

template<typename...>
struct void_
{
    using type = void;
};

template<typename... Args>
using Void = typename void_<Args...>::type;

template<typename T, typename = void>
struct has_const_iterator : std::false_type {};

template<typename T>
struct has_const_iterator<T, Void<typename T::const_iterator>> : std::true_type {};

template<typename T, typename = void>
struct has_iterator : std::false_type {};

template<typename T>
struct has_iterator<T, Void<typename T::iterator>> : std::true_type {};

struct has_begin_impl
{
    template<typename T
            , typename Begin = decltype(std::declval<const T&>().begin())
    >
    static std::true_type test(int);
    template<typename...>
    static std::false_type test(...);
};

struct has_begin_end_impl
{
    template<typename T
            , typename Begin = decltype(std::declval<const T&>().begin())
            , typename End   = decltype(std::declval<const T&>().end())
    >
    static std::true_type test(int);
    template<typename...>
    static std::false_type test(...);
};

template<typename T>
struct has_begin_end : decltype(has_begin_end_impl::test<T>(0)) {};

template<typename T>
struct has_begin: decltype(has_begin_impl::test<T>(0)) {};



#endif //KLONDIKE_TYPE_TRAITS_HPP
