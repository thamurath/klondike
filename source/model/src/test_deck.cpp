#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "pretty_print.h"

#include "Deck.hpp"
#include "english_model/Card.hpp"
#include "CardHeap.hpp"

#include <vector>
#include <iostream>
template<typename Card>
std::vector<Card> GetCompleteDeck(void)
{
    namespace model = com::jlom::klondike::model;

    using Suit = typename model::CardTraits<Card>::SuitType;
    using Figure = typename model::CardTraits<Card>::FigureType;
    std::vector<Card> ret;
    for (Suit suit : Suit::_values())
    {
        for (Figure figure : Figure::_values())
        {
            ret.push_back({figure, suit});
        }
    }

    return ret;
};


#include <common/JsonStatusEncoder.hpp>
#include "JsonCardStatusEncoder.hpp"
TEST_CASE("simple test","[model] [deck]")
{
    namespace common = com::jlom::klondike;
    namespace model = common::model;

    using CardType = model::english_model::Card;
    using DeckType = model::Deck<CardType , model::CardHeap>;


    std::vector<CardType> cards = GetCompleteDeck<CardType>();

    const auto cardsSize = cards.size();

    DeckType deck(std::move(cards));

    auto* encoder = new common::JsonStatusEncoder;

    std::cout << deck.EncodeStatus("deck",encoder) << std::endl;

    delete encoder;
    REQUIRE(deck.Size() == cardsSize);
}

