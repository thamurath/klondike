
#include "DeckView.hpp"

#include <json.hpp>
#include <source/view/StatusDecoder.hpp>

#include <spdlog/spdlog.h>
namespace com::jlom::klondike::view
{
    DeckView::DeckView(std::string id)
    : OutView(id)
    , numCards(0)
    {

    }

    void DeckView::DecodeStatus(const StatusDecoder &statusDecoder, Status const &status)
    {
        spdlog::get("console")->debug("DeckView::DecodeStatus");
        auto cards = statusDecoder.Decode(GetId(),status);
        numCards = std::size(cards);
        spdlog::get("console")->debug("DeckView::DecodeStatus Numcards{}", numCards);
    }
    void DeckView::Show(void)
    {
        spdlog::get("console")->debug("DeckView::Show");
        std::stringstream ss;
        ss << "id:" << GetId()
           << ",numCards:" << numCards;

        GetView().ShowText(ss.str());
    }

}