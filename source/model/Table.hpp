#ifndef KLONDIKE_MODEL_TABLE_HPP__
#define KLONDIKE_MODEL_TABLE_HPP__

#include "src/ModelFactory.hpp"
#include "Transactional.hpp"

#include "src/JsonCardStatusEncoder.hpp"

#include <common/StatusNotification.hpp>

#include "type_name.hpp"
#include "hotchpotch.hpp"

#include "exceptions.hpp"
#include "spdlog/spdlog.h"

#include <memory>
#include <utility>
#include <variant>
#include <map>
#include <vector>
#include <type_traits>
#include <sstream>
#include <optional.hpp>
#include <string>

namespace com::jlom::klondike::model
{
    template<typename T>
    class optional_reference_wrapper final
    {
    public:
        optional_reference_wrapper(std::reference_wrapper<T> incoming)
                : internal(incoming)
        {

        }

        optional_reference_wrapper(std::nullopt_t) noexcept
        : internal()
        {

        }
        optional_reference_wrapper() noexcept
        : internal()
        {}
        T& operator*()
        {
            return internal->get();
        }
        const T& operator*() const
        {
            return internal->get();
        }
        T* operator->()
        {
            return &internal->get();
        }
        const T* operator->() const
        {
            return &internal->get();
        }

        explicit operator bool(void) const
        {
            return internal.has_value();
        }

    private:
        std::optional<std::reference_wrapper<T>> internal;
    };
    template<typename Card_T, typename Deck_T, typename Pile_T, typename Waste_T, typename Foundation_T
    , typename StatusEncoder, typename InformationHub>
    class Table final
    {
        template <typename ...Ts>
        struct typeList
        {
            using value_type  = std::variant<Ts...>;
            using ret_type = std::variant<typename Ts::state_type...>;
        };
        using variant_types = typeList<Deck_T,Pile_T,Waste_T,Foundation_T>;
        using value_type = typename variant_types::value_type;
        using ret_type = typename variant_types::ret_type;

        using ModelIndex = std::map<std::string, value_type >;


    public:
        using CardModelType = Card_T;
        using DeckModelType = Deck_T;
        using WasteModelType = Waste_T;
        using PileModelType = Pile_T;
        using FoundationModelType = Foundation_T;

        using SuitType = typename Card_T::SuitType;
        using FigureType = typename Card_T::FigureType;
        using ColorType = typename Card_T::ColorType;



        template <typename T>
        explicit Table(ModelFactory<T> const& factory,StatusEncoder* encoder, InformationHub* hub)
        : statusEncoder(encoder)
        , informationHub(hub)
        {
            m_model.insert(std::make_pair("deck", factory.CreateDeck()));
            shuffle_deck();
            m_model.insert(std::make_pair("waste", factory.CreateWaste()));

            m_model.insert(getPile("pile_1",1, factory));
            m_model.insert(getPile("pile_2",2, factory));
            m_model.insert(getPile("pile_3",3, factory));
            m_model.insert(getPile("pile_4",4, factory));
            m_model.insert(getPile("pile_5",5, factory));
            m_model.insert(getPile("pile_6",6, factory));
            m_model.insert(getPile("pile_7",7, factory));

            const std::string foundation("foundation_");
            for ( std::size_t idx(0); idx < SuitType::_size(); ++idx)
            {
                std::stringstream ss;
                ss << foundation << idx+1;
                m_model.insert(getFoundation(ss.str(), factory));
            }
        }
        ~Table(void) = default;

        auto Begin(std::string const& id)
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                throw exceptions::ElementNotFound(id);
            }
            return std::visit([](auto&& arg) -> ret_type
                              {
                                  using T = std::decay_t<decltype(arg)>;
                                  if constexpr (std::is_base_of_v<Transactional<T>,T>)
                                  {
                                      return arg.Begin();
                                  }
                                  else
                                  {
                                      throw exceptions::NonTransactionalItem(debug::type_name<T>());
                                  }
                              }
                    , ite->second
            );
        }

        void Commit(std::string const& id)
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                throw exceptions::ElementNotFound(id);
            }

            std::visit([](auto&& arg)
                              {
                                  using T = std::decay_t<decltype(arg)>;
                                  if constexpr (std::is_base_of_v<Transactional<T>,T>)
                                  {
                                      arg.Commit();
                                  }
                                  else
                                  {
                                      throw exceptions::NonTransactionalItem(debug::type_name<T>());
                                  }
                              }
                    , ite->second
            );
        }


        void Rollback(std::string const& id, ret_type const& state)
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                throw exceptions::ElementNotFound(id);
            }

            std::visit([&state](auto&& arg)
                              {
                                  using T = std::decay_t<decltype(arg)>;
                                  if constexpr (std::is_base_of_v<Transactional<T>,T>)
                                  {
                                      arg.Rollback(std::get<typename T::state_type>(state));
                                  }
                                  else
                                  {
                                      throw exceptions::NonTransactionalItem(debug::type_name<T>());
                                  }
                              }
                    , ite->second
            );
        }


        std::int32_t EncodeStatus(void)
        {
            assert(nullptr != statusEncoder);
            assert(nullptr != informationHub);

            auto ret = statusEncoder->Encode();
            for (auto ite = std::begin(m_model); ite != std::end(m_model); ++ite)
            {
                ret = statusEncoder->Append(ret, std::visit([&ite, this](auto &&arg) {
                    return arg.EncodeStatus(ite->first, statusEncoder);
                }, ite->second)
                );
            }

            spdlog::get("console")->debug("Sending StatusNotification: {}", ret.dump());
            informationHub->PostNotification(new StatusNotification(ret.dump()));
        }
        std::int32_t Push(std::string const& id, CardModelType&& card)
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                throw exceptions::ElementNotFound(id);
            }

            return std::visit([&card](auto&& arg)
                       {
                           return arg.Push(std::move(card));

                       }, ite->second);
        }

        std::optional<Card_T> Get(std::string const& id) const
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                return {};
            }

            return std::visit([](auto&& arg) -> std::optional<Card_T>
                              {
                                  return arg.Get();
                              }, ite->second);
        }

        void Pop(std::string const& id)
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                throw exceptions::ElementNotFound(id);
            }
            return std::visit([](auto &&arg)
                              {
                                  arg.Pop();
                              }, ite->second);
        }

        template <typename T>
        optional_reference_wrapper<T> Get(std::string const& ai_name)
        {
            auto ite = m_model.find(ai_name);
            if (std::end(m_model) == ite)
            {
                return {};
            }
            try
            {
                return std::ref(std::get<T>(ite->second));
            }
            catch (std::bad_variant_access const&)
            {
                return {};
            }

        }

        auto GetCards(std::string const& id, std::size_t const& numCards)
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                throw exceptions::ElementNotFound(id);
            }

            return std::visit([&numCards](auto &&arg)
                              {
                                  using T = std::decay_t<decltype(arg)>;
                                  return arg.GetCards(numCards);
                              }, ite->second);
        }

        bool IsEmpty(std::string const& id)
        {
            spdlog::get("console")->trace("Table IsEmpty: {}", id);
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                spdlog::get("console")->error("Table IsEmpty: exception ElementNotFound {}", id);
                throw exceptions::ElementNotFound(id);
            }
            spdlog::get("console")->debug("Table IsEmpty: id:{} Found ", id);
            return std::visit([&id](auto &&arg)
                              {
                                  using T = std::decay_t<decltype(arg)>;
                                  spdlog::get("console")->trace("Table IsEmpty: id{} type:{}", id, debug::type_name<T>());
                                  return arg.IsEmpty();
                              }, ite->second);
        }

        std::size_t Size(std::string const& id)
        {
            auto ite = m_model.find(id);
            if (std::end(m_model) == ite)
            {
                throw exceptions::ElementNotFound(id);
            }
            return std::visit([](auto &&arg)
                              {
                                  using T = std::decay_t<decltype(arg)>;
                                  return arg.Size();
                              }, ite->second);
        }
    protected:
    private:
        ModelIndex m_model;
        StatusEncoder* statusEncoder;
        InformationHub* informationHub;

        template <typename T>
        std::vector<Card_T> extract_cards(std::size_t const& number,T& deck)
        {
            std::vector<Card_T> cards;
            cards.reserve(number);
            for (std::size_t idx(0); idx < number; ++idx)
            {
                cards.push_back(*deck.Get());
                deck.Pop();
            }
            return cards;
        }

        void shuffle_deck(void)
        {
            (*Get<Deck_T>("deck")).Shuffle();
        }

        template <typename T>
        auto getPile(std::string const &id, std::size_t const& numCards, const ModelFactory <T> &factory)
        {
            namespace hp = com::jlom::klondike::hotchpotch;
            return std::make_pair(id, factory.CreatePile(numCards,hp::TurnUpFirst(extract_cards(numCards, *Get<Deck_T>("deck")))));
        }

        template <typename T>
        auto getFoundation(std::string const &id, const ModelFactory<T> &factory)
        {
            return std::make_pair(id, factory.CreateFoundation());
        }
    };
}


#endif //KLONDIKE_MODEL_TABLE_HPP__
