#ifndef KLONDIKE_HUMANINTERACTIONVIEW_HPP
#define KLONDIKE_HUMANINTERACTIONVIEW_HPP

#include "PlayerInteraction.hpp"

namespace com::jlom::klondike::view
{
    class HumanInteractionView : public PlayerInteraction
    {
    public:
        explicit HumanInteractionView(std::string const& id);

        Command GetCommand() override ;
    };
}

#endif //KLONDIKE_HUMANINTERACTIONVIEW_HPP
