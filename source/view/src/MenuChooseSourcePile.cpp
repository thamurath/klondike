#include "MenuChooseSourcePile.hpp"

#include <cassert>
namespace com::jlom::klondike::view
{
    void MenuChooseSourcePile::DecodeOption(MenuContext& menuContext)
    {
        ///@jlom TODO: This needs access to configuration for the foundation ids
        menuContext.Builder().SetSourceType(ElementType::PILE);
        switch (menuContext.GetMenuOption())
        {
            case 1:
            {
                menuContext.Builder().SetSource("pile_1");
                break;
            }
            case 2:
            {
                menuContext.Builder().SetSource("pile_2");
                break;
            }
            case 3:
            {
                menuContext.Builder().SetSource("pile_3");
                break;
            }
            case 4:
            {
                menuContext.Builder().SetSource("pile_4");
                break;
            }
            case 5:
            {
                menuContext.Builder().SetSource("pile_5");
                break;
            }
            case 6:
            {
                menuContext.Builder().SetSource("pile_6");
                break;
            }
            case 7:
            {
                menuContext.Builder().SetSource("pile_7");
                break;
            }
            default:
                assert(false);
        }
    }
}