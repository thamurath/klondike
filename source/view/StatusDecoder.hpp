#ifndef KLONDIKE_STATUSENCODER_HPP
#define KLONDIKE_STATUSENCODER_HPP

#include "source/common/Status.hpp"
#include "CardView.hpp"

#include <string>
#include <vector>
namespace com::jlom::klondike::view
{
    class StatusDecoder
    {
    public:
        virtual  std::vector <CardView> Decode(std::string const &id, Status status) const = 0;

        virtual ~StatusDecoder() = default;
    };
}

#endif //KLONDIKE_STATUSENCODER_HPP
