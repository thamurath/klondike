#include "MenuItem.hpp"

#include <spdlog/spdlog.h>

namespace com::jlom::klondike::view
{
    Command MenuItem::Handle(MenuContext &menuContext)
    {
        spdlog::get("console")->trace("Entering MenuItem::Handle");
        auto options = GetOptions();
        ShowMenuOptions(menuContext, options.showText);
        menuContext.SetMenuOption(GetOptionNumber(menuContext, options.minOption, options.maxOption));
        DecodeOption(menuContext);

        return DelegateHanlding(menuContext);
    }

    void MenuItem::ShowMenuOptions(MenuContext& menuContext, std::string const &menuOptions)
    {
        spdlog::get("console")->trace("Entering MenuItem::ShowMenuOptions");
        menuContext.GetPlayerInteraction().ShowText(menuOptions);
        menuContext.GetPlayerInteraction().ShowText("Choose One Option");
        spdlog::get("console")->trace("Exit MenuItem::ShowMenuOptions");
    }

    MenuOption MenuItem::GetOptionNumber(MenuContext& menuContext, std::uint32_t min, std::uint32_t max)
    {
        spdlog::get("console")->trace("Entering MenuItem::GetOptionNumber");
        return menuContext.GetPlayerInteraction().GetNumber(min,max);
    }
}

