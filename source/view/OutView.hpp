#ifndef KLONDIKE_OUTVIEW_HPP
#define KLONDIKE_OUTVIEW_HPP

#include "View.hpp"
#include "StatusDecoder.hpp"

#include <common/Status.hpp>

namespace com::jlom::klondike::view
{
    class OutView : public View
    {
    public:
        explicit OutView(std::string const& id) : View(id) {}
        virtual void DecodeStatus(StatusDecoder const &statusDecoder, Status const &status) = 0;
        virtual void Show(void) = 0;

        virtual ~OutView() = default;
    };
}

#endif //KLONDIKE_OUTVIEW_HPP
