#include "exceptions.hpp"


namespace com::jlom::klondike::model::exceptions
{
    ElementNotFound::ElementNotFound(std::string const &id)
    : id(id)
    {

    }

    const char* ElementNotFound::what() const noexcept
    {
        using namespace std::string_literals;
        return ("ElementNotFound: "s + id).c_str();
    }


    NonTransactionalItem::NonTransactionalItem(std::string const &id)
            : id(id)
    {

    }

    const char* NonTransactionalItem::what() const noexcept
    {
        using namespace std::string_literals;
        return ("NonTransactionalItem: "s + id).c_str();
    }
}