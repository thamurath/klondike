#ifndef KLONDIKE_CONTROLLER_OPERATIONPOLICIES_HPP
#define KLONDIKE_CONTROLLER_OPERATIONPOLICIES_HPP

#include <common/Action.hpp>

#include <spdlog/spdlog.h>

#include <cstdlib>


namespace com::jlom::klondike::controller::policies::operation
{
    class TurnUp
    {
    public:
        template <typename Card>
        Card UnaryOp(Card&& card)
        {
            spdlog::get("console")->debug("TurnUp:UnaryOp card: {}", card.GetFigure()._to_string());
            card.TurnUp();
            return card;
        }
        template <typename Card>
        Card&& Transform(Card&& card)
        {
            spdlog::get("console")->debug("TurnUp:Transform card: {}", card.GetFigure()._to_string());
            card.TurnUp();
            return std::move(card);
        }
    };

    class TurnDown
    {
    public:
        template <typename Card>
        Card UnaryOp(Card&& card)
        {
            spdlog::get("console")->info("TurnDown:UnaryOp card: {}", card.GetFigure()._to_string());
            card.TurnDown();
            return card;
        }
        template <typename Card>
        Card&& Transform(Card&& card)
        {
            spdlog::get("console")->info("TurnDown:Transform card: {}", card.GetFigure()._to_string());
            card.TurnDown();
            return std::move(card);
        }
    };

    class NoOperation
    {
    public:
        template <typename Card>
        Card UnaryOp(Card&& card)
        {
            spdlog::get("console")->info("NoOperation:UnaryOp card: {}", card.GetFigure()._to_string());
            return card;
        }
        template <typename Card>
        Card&& Transform(Card&& card)
        {
            spdlog::get("console")->info("NoOperation:Transform card: {}", card.GetFigure()._to_string());
            return std::move(card);
        }
    };
}


#endif //KLONDIKE_CONTROLLER_OPERATIONPOLICIES_HPP