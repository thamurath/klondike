
#include "ChainableController.hpp"


namespace com::jlom::klondike::controller
{

        std::int32_t ChainableController::operator()(Action const &action)
        {
            if (shouldHandle(action))
            {
                if ( canHandle(action))
                {
                    return handle(action);
                }
                else
                {
                    return -1;
                }
            }
            else if (next)
            {
                return (*next)(action);
            }
            return -1;
        }

        void ChainableController::SetHandler(std::unique_ptr<ChainableController> &&handler)
        {
            next = std::move(handler);
        }

        void ChainableController::AddHandler(std::unique_ptr<ChainableController> &&handler)
        {
            if (!next)
            {
                SetHandler(std::move(handler));
            }
            else
            {
                next->AddHandler(std::move(handler));
            }
        }
}
