#include "CardView.hpp"

#include <string>
namespace com::jlom::klondike::view
{
    CardView::CardView(std::string figure, std::string suit, std::string color)
    : figure(figure)
    , suit(suit)
    , color(color)
    , faceUp(false)
    {

    }

    CardView::CardView(void)
    : CardView("","","")
    {

    }

    std::ostream& operator<< (std::ostream& os, CardView const& obj)
    {
        os << "f:" << obj.figure
           << "|s:" << obj.suit
           << "|c: " << obj.color
           << "|up: " << std::boolalpha << obj.faceUp;

        return os;
    }

    void CardView::SetFigure(std::string const& ai_figure)
    {
        figure = ai_figure;
    }
    void CardView::SetSuit(std::string const& ai_suit)
    {
        suit= ai_suit;
    }
    void CardView::SetColor(std::string const& ai_color)
    {
        color = ai_color;
    }

    void CardView::SetFaceUp(bool faceup)
    {
        faceUp = faceup;
    }

    bool CardView::IsFaceUp(void) const
    {
        return faceUp;
    }
}


