#ifndef KLONDIKE_CONTROLLER_INFORMATIONHUB_HPP
#define KLONDIKE_CONTROLLER_INFORMATIONHUB_HPP

#include <common/NotificationBase.hpp>
#include <common/ObserverAdapter.hpp>

#include <Poco/NotificationCenter.h>
#include <memory>

namespace com::jlom::klondike::controller
{
    class InformationHub final
    {
    public:
        InformationHub(void) = default;
        ~InformationHub() = default;


        template<typename T, typename C>
        void AddObserver(ObserverAdapter<T,C> const& observer)
        {
            notificationCenter.addObserver(observer);
        }


        void PostNotification(NotificationBase* notificationPtr)
        {
            std::unique_ptr<NotificationBase> p(notificationPtr);
            notificationCenter.postNotification(notificationPtr);
        }

        // avoid copy
        InformationHub(InformationHub const& ) = delete;
        InformationHub& operator=(InformationHub const& ) = delete;

        // avoid move
        InformationHub(InformationHub&& ) = delete;
        InformationHub& operator=(InformationHub&& ) = delete;

    protected:
    private:
        Poco::NotificationCenter notificationCenter;
    };
}

#endif //KLONDIKE_CONTROLLER_INFORMATIONHUB_HPP
