#ifndef KLONDIKE_CONTROLLER_CHAINFACTORY_HPP
#define KLONDIKE_CONTROLLER_CHAINFACTORY_HPP

#include "ChainableController.hpp"

#include <better_enums.hpp>

#include <memory>
#include <cstdlib>

namespace com::jlom::klondike::controller
{
    template <typename ControllerFactory>
    class ChainFactory
    {
    public:
        ChainFactory(ControllerFactory const& controllerFactory)
        : controllerFactory(controllerFactory)
        {

        }
        template <typename AllowedActions>
        std::unique_ptr<ChainableController> Create(void)
        {
            auto chainController = controllerFactory.GetController(First<AllowedActions>());

            for (std::size_t index (1); index < AllowedActions::_values().size(); ++index)
            {
                chainController->AddHandler(controllerFactory.GetController(AllowedActions::_values()[index]) );
            }

            return chainController;
        }

    private:
        ControllerFactory const& controllerFactory;
    };
}
#endif //KLONDIKE_CONTROLLER_CHAINFACTORY_HPP
