
#ifndef KLONDIKE_VIEWTECHIMPLEMENTATION_HPP
#define KLONDIKE_VIEWTECHIMPLEMENTATION_HPP

#include <string>
namespace com::jlom::klondike::view
{
    class ViewTechImplementation
    {
    public:
        virtual void ShowText(std::string const& text) = 0;
        virtual std::string GetInput(void) = 0;
        virtual std::uint32_t GetNumber(const uint32_t &min, const uint32_t &max) = 0;

        virtual ~ViewTechImplementation() = default;
    };
}

#endif //KLONDIKE_VIEWTECHIMPLEMENTATION_HPP
