#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <source/view/CardViewJsonEncoderHelper.hpp>

#include <json.hpp>

#include <fstream>
#include <iostream>

TEST_CASE("simple json test") {

    // for convenience
    using json = nlohmann::json;
    // create an empty structure (null)
    json j;

// add a number that is stored as double (note the implicit conversion of j to an object)
    j["pi"] = 3.141;

// add a Boolean that is stored as bool
    j["happy"] = true;

// add a string that is stored as std::string
    j["name"] = "Niels";

// add another null object by passing nullptr
    j["nothing"] = nullptr;

// add an object inside the object
    j["answer"]["everything"] = 42;

// add an array that is stored as std::vector (using an initializer list)
    j["list"] = { 1, 0, 2 };

// add another object (using an initializer list of pairs)
    j["object"] = { {"currency", "USD"}, {"value", 42.99} };

// instead, you could also write (which looks very similar to the JSON above)
    json j2 = {
            {"pi", 3.141},
            {"happy", true},
            {"name", "Niels"},
            {"nothing", nullptr},
            {"answer", {
                           {"everything", 42}
                   }},
            {"list", {1, 0, 2}},
            {"object", {
                           {"currency", "USD"},
                         {"value", 42.99}
                   }}
    };


    REQUIRE(j == j2);
}

#include <source/view/CardView.hpp>
#include <vector>

TEST_CASE("status testing")
{
    namespace view = com::jlom::klondike::view;

    try {
        std::string status_s = R"({"deck":[{"color":"BLACK","faceup":false,"figure":"KING","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"FOUR","suit":"SPADES"},{"color":"BLACK","faceup":false,"figure":"TEN","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"THREE","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"NINE","suit":"SPADES"},{"color":"RED","faceup":false,"figure":"KING","suit":"DIAMONDS"},{"color":"RED","faceup":false,"figure":"TWO","suit":"HEARTS"},{"color":"BLACK","faceup":false,"figure":"JACK","suit":"SPADES"},{"color":"RED","faceup":false,"figure":"SIX","suit":"HEARTS"},{"color":"BLACK","faceup":false,"figure":"FOUR","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"SEVEN","suit":"CLUBS"},{"color":"RED","faceup":false,"figure":"JACK","suit":"DIAMONDS"},{"color":"RED","faceup":false,"figure":"THREE","suit":"HEARTS"},{"color":"RED","faceup":false,"figure":"ACE","suit":"HEARTS"},{"color":"BLACK","faceup":false,"figure":"THREE","suit":"SPADES"},{"color":"BLACK","faceup":false,"figure":"FIVE","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"ACE","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"SIX","suit":"SPADES"},{"color":"BLACK","faceup":false,"figure":"NINE","suit":"CLUBS"},{"color":"RED","faceup":false,"figure":"FOUR","suit":"DIAMONDS"},{"color":"RED","faceup":false,"figure":"FOUR","suit":"HEARTS"},{"color":"RED","faceup":false,"figure":"NINE","suit":"HEARTS"},{"color":"RED","faceup":false,"figure":"TWO","suit":"DIAMONDS"},{"color":"BLACK","faceup":false,"figure":"TWO","suit":"CLUBS"}],"foundation_0":[],"foundation_1":[],"foundation_2":[],"foundation_3":[],"pile_1":[{"color":"RED","faceup":true,"figure":"SEVEN","suit":"HEARTS"}],"pile_2":[{"color":"BLACK","faceup":false,"figure":"TWO","suit":"SPADES"},{"color":"BLACK","faceup":true,"figure":"TEN","suit":"SPADES"}],"pile_3":[{"color":"BLACK","faceup":false,"figure":"SEVEN","suit":"SPADES"},{"color":"BLACK","faceup":false,"figure":"FIVE","suit":"SPADES"},{"color":"RED","faceup":true,"figure":"ACE","suit":"DIAMONDS"}],"pile_4":[{"color":"RED","faceup":false,"figure":"SIX","suit":"DIAMONDS"},{"color":"BLACK","faceup":false,"figure":"KING","suit":"SPADES"},{"color":"BLACK","faceup":false,"figure":"EIGHT","suit":"SPADES"},{"color":"RED","faceup":true,"figure":"TEN","suit":"DIAMONDS"}],"pile_5":[{"color":"BLACK","faceup":false,"figure":"QUEEN","suit":"SPADES"},{"color":"RED","faceup":false,"figure":"TEN","suit":"HEARTS"},{"color":"RED","faceup":false,"figure":"THREE","suit":"DIAMONDS"},{"color":"RED","faceup":false,"figure":"FIVE","suit":"DIAMONDS"},{"color":"RED","faceup":true,"figure":"QUEEN","suit":"DIAMONDS"}],"pile_6":[{"color":"BLACK","faceup":false,"figure":"JACK","suit":"CLUBS"},{"color":"RED","faceup":false,"figure":"SEVEN","suit":"DIAMONDS"},{"color":"RED","faceup":false,"figure":"QUEEN","suit":"HEARTS"},{"color":"RED","faceup":false,"figure":"EIGHT","suit":"HEARTS"},{"color":"RED","faceup":false,"figure":"KING","suit":"HEARTS"},{"color":"RED","faceup":true,"figure":"NINE","suit":"DIAMONDS"}],"pile_7":[{"color":"RED","faceup":false,"figure":"JACK","suit":"HEARTS"},{"color":"RED","faceup":false,"figure":"EIGHT","suit":"DIAMONDS"},{"color":"RED","faceup":false,"figure":"FIVE","suit":"HEARTS"},{"color":"BLACK","faceup":false,"figure":"QUEEN","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"EIGHT","suit":"CLUBS"},{"color":"BLACK","faceup":false,"figure":"SIX","suit":"CLUBS"},{"color":"BLACK","faceup":true,"figure":"ACE","suit":"SPADES"}],"waste":[]})";
        std::ifstream status("status.json");
        nlohmann::json json_status;
        //status >> json_status;
        json_status = nlohmann::json::parse(status_s);

        std::cout << json_status << std::endl;


        auto element = json_status.find("deck");
        if (json_status.end() == element)
        {
            throw "element not found";
        }
        std::cout << "element: " << *element << std::endl;

        std::vector<view::CardView> ret;
        for( auto& card : *element)
        {
            view::CardView cardView = card;
            std::cout << "from: " << card << std::endl;
            std::cout << "get: " << cardView << std::endl;

            ret.push_back(cardView);
        }
    }
    catch (...)
    {
        std::cerr << "error while opening file: Copy status.json to build folder" << std::endl;
        REQUIRE(false);
    }

}