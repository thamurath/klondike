#include "View.hpp"

#include <cassert>

#include <spdlog/spdlog.h>
namespace com::jlom::klondike::view
{
    View::View(std::string const& id)
            : viewTechImplementation(nullptr)
            , id(id)
    {
        spdlog::get("console")->trace("View Constructor: id:{}", id);
    }

    View::~View(void)
    {
        delete viewTechImplementation;
    }

    void View::SetViewTech(ViewTechImplementation* techView)
    {
        assert(nullptr != techView);

        delete viewTechImplementation;

        viewTechImplementation = techView;
    }

    ViewTechImplementation& View::GetView(void) const
    {
        assert(nullptr != viewTechImplementation);

        return *viewTechImplementation;
    }

    std::string const& View::GetId() const
    {
        spdlog::get("console")->trace("View GetId: id:{}", id);
        return id;
    }

}
