#ifndef KLONDIKE_CONTROLLER_CONTROLLERFACTORY_IMPL_HPP
#define KLONDIKE_CONTROLLER_CONTROLLERFACTORY_IMPL_HPP

#ifndef KLONDIKE_CONTROLLER_CONTROLLERFACTORY_HPP
#error "Should not include this file directly"
#endif

#include "GeneralMoveController.hpp"
#include "ShowStatusController.hpp"
#include "ResignController.hpp"

#include "policies/DestinationCheckPolicies.hpp"
#include "policies/SourceCheckPolicies.hpp"
#include "policies/PreCheckPolicies.hpp"
#include "policies/OperationPolicies.hpp"
#include "policies/TransferenceOrderPolicies.hpp"

#include <common/Action.hpp>

namespace com::jlom::klondike::controller
{
        template<typename ModelInterface>
        ControllerFactory<ModelInterface>::ControllerFactory(ModelInterface& modelInterface)
        : modelInterface(modelInterface)
        {

        }
        
        template <typename ModelInterface>
        std::unique_ptr<ChainableController> ControllerFactory<ModelInterface>::GetController(ACTION_TYPE type) const
        {

            using RedealControllerType = GenericMoveController<ModelInterface
                                                                , policies::precheck::SourceNotEmpty_DestinationEmpty
                                                                , policies::sourcecheck::AllCards_AllFaceUp
                                                                , policies::destinationcheck::AllFaceDown
                                                                , policies::operation::TurnDown
                                                                , policies::transference_order::ReverseOrder>;

            using DeckToWasteControllerType = GenericMoveController<ModelInterface
                                                                , policies::precheck::SourceNotEmpty
                                                                , policies::sourcecheck::NotEmpty_OneCard_FaceDown
                                                                , policies::destinationcheck::OneCard_FaceUp
                                                                , policies::operation::TurnUp
                                                                , policies::transference_order::NoChangeOrder>;

            using WasteToPileControllerType = GenericMoveController<ModelInterface
                                                                , policies::precheck::SourceNotEmpty
                                                                , policies::sourcecheck::NotEmpty_OneCard_FaceUp
                                                                , policies::destinationcheck::ToPile
                                                                , policies::operation::NoOperation
                                                                , policies::transference_order::NoChangeOrder>;

            using PileToPileControllerType = GenericMoveController<ModelInterface
                                                                , policies::precheck::SourceNotEmpty
                                                                , policies::sourcecheck::EnoughCards_AllFaceUp
                                                                , policies::destinationcheck::ToPile
                                                                , policies::operation::NoOperation
                                                                , policies::transference_order::NoChangeOrder>;

            using WastToFoundationControllerType = GenericMoveController<ModelInterface
                                                                , policies::precheck::SourceNotEmpty
                                                                , policies::sourcecheck::NotEmpty_OneCard_FaceUp
                                                                , policies::destinationcheck::ToFoundation
                                                                , policies::operation::NoOperation
                                                                , policies::transference_order::NoChangeOrder>;

            using PileTurnUpCardControllerType = GenericMoveController<ModelInterface
                                                                , policies::precheck::SameHeap_NotEmpty
                                                                , policies::sourcecheck::NotEmpty_OneCard_FaceDown
                                                                , policies::destinationcheck::PileTurnUp
                                                                , policies::operation::TurnUp
                                                                , policies::transference_order::NoChangeOrder>;

            using PileToFoundationControllerType = GenericMoveController<ModelInterface
                                                               , policies::precheck::SourceNotEmpty
                                                               , policies::sourcecheck::NotEmpty_OneCard_FaceUp
                                                               , policies::destinationcheck::ToFoundation
                                                               , policies::operation::NoOperation
                                                               , policies::transference_order::NoChangeOrder>;

            switch (type)
            {
                case ACTION_TYPE::REDEAL:
                {
                    return std::make_unique<RedealControllerType>(modelInterface, type);
                }
                case ACTION_TYPE::DECK_TO_WASTE:
                {
                    return std::make_unique<DeckToWasteControllerType>(modelInterface, type);
                }
                case ACTION_TYPE::WASTE_TO_PILE:
                {
                    return std::make_unique<WasteToPileControllerType>(modelInterface, type);
                }
                case ACTION_TYPE::PILE_TO_PILE:
                {
                    return std::make_unique<PileToPileControllerType>(modelInterface, type);
                }
                case ACTION_TYPE::WASTE_TO_FOUNDATION:
                {
                    return std::make_unique<WastToFoundationControllerType>(modelInterface, type);
                }
                case ACTION_TYPE::PILE_TURNUP_CARD:
                {
                    return std::make_unique<PileTurnUpCardControllerType>(modelInterface, type);
                }
                case ACTION_TYPE::PILE_TO_FOUNDATION:
                {
                    return std::make_unique<PileToFoundationControllerType>(modelInterface, type);
                }
                case ACTION_TYPE::REQUEST_GAME_STATUS:
                {
                    return std::make_unique<ShowStatusController<ModelInterface>>(modelInterface, type);
                }
                case ACTION_TYPE::RESIGN:
                {
                    return std::make_unique<ResignController>(type);
                }
            }
        }

}


#endif //KLONDIKE_CONTROLLER_CONTROLLERFACTORY_IMPL_HPP