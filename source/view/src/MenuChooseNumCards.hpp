#ifndef KLONDIKE_MENUCHOOSENUMCARDS_HPP
#define KLONDIKE_MENUCHOOSENUMCARDS_HPP

#include "MenuItem.hpp"


namespace com::jlom::klondike::view
{
    class MenuChooseNumCards : public MenuItem
    {
    private:

        MenuItem::Options GetOptions(void) override ;
        void DecodeOption(MenuContext& menuContext) override;
    };
}

#endif //KLONDIKE_MENUCHOOSENUMCARDS_HPP
