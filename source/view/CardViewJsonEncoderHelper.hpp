#ifndef KLONDIKE_CARDVIEWEJSONNCODERHELPER_HPP
#define KLONDIKE_CARDVIEWEJSONNCODERHELPER_HPP

#include "CardView.hpp"

#include <json.hpp>
#include <string>
namespace com::jlom::klondike::view
{

    void from_json(const nlohmann::json& j, CardView& card)
    {
        card.SetSuit(j.at("suit").get<std::string>());
        card.SetFigure(j.at("figure").get<std::string>());
        card.SetColor(j.at("color").get<std::string>());
        card.SetFaceUp(j.at("faceup").get<bool>());
    }
}
#endif //KLONDIKE_CARDVIEWEJSONNCODERHELPER_HPP
