#ifndef KLONDIKE_GUIVIEWTECH_HPP
#define KLONDIKE_GUIVIEWTECH_HPP

#include "ViewTechImplementation.hpp"

namespace com::jlom::klondike::view
{
    class GUIViewTech : public ViewTechImplementation
    {
    public:
        std::string GetInput(void) override;
        void ShowText(std::string const& text) override ;
        std::uint32_t GetNumber(const uint32_t &min, const uint32_t &max) override ;
    };
}

#endif //KLONDIKE_GUIVIEWTECH_HPP
