#include "GUIViewTech.hpp"

#include <iostream>

namespace com::jlom::klondike::view
{
    std::string GUIViewTech::GetInput(void)
    {
        std::cout << "We are out of GUI so you will have to deal with a black screen  ...." << std::endl;
        std::string input;
        getline(std::cin, input);
        return input;
    }
    void GUIViewTech::ShowText(std::string const& text)
    {
        std::cout << "We are out of GUI so you will have to deal with a black screen  ...." << std::endl;
        std::cout << text << std::endl;
    }

    std::uint32_t GUIViewTech::GetNumber(const uint32_t &min, const uint32_t &max)
    {
        std::cout << "We are out of GUI so you will have to deal with a black screen  ...." << std::endl;

    }
}

