#ifndef __KLONDIKE_MODEL_WASTE_HPP__
#define __KLONDIKE_MODEL_WASTE_HPP__

#include "Card.hpp"
#include "Transactional.hpp"

#include <vector>

namespace com::jlom::klondike::model
{

    template <typename Card>
    class WasteState
    {
    public:

        ~WasteState() = default;


        WasteState(WasteState const& rhs) = delete;
        WasteState& operator=(WasteState const& rhs) = delete;

        WasteState(WasteState && rhs) noexcept = default;
        WasteState& operator=(WasteState && rhs) noexcept = default;


    private:
        template <typename T, template <typename C> class H>
        friend class Waste;

        explicit WasteState(std::vector<Card>const& cards)
                : m_cards(cards)
        {}
        std::vector<Card> m_cards;
    };

    template <typename Card, template <typename C> class Heap>
    class Waste : public Transactional<Waste<Card, Heap>>
    {
    public:
        Waste(void) = default;
        virtual ~Waste(void) = default;

        //avoid copy
        Waste(Waste const&) = delete;
        Waste& operator=(Waste const&) = delete;

        //allow movement
        Waste(Waste&&) noexcept = default;
        Waste& operator=(Waste&&) noexcept = default;

        auto IsEmpty(void) const;
        auto Size(void) const;
        auto Get(void) const;
        auto GetCards(std::size_t const& numCards) const;

        void Pop(void);
        auto Push(Card&& card);


        template <typename StatusEncoder>
        auto EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const;

        /// Transactional Interface
        using state_type = WasteState<Card>;
    protected:
    private:
        Heap<Card> heap;

        /// Transactional Interface
        friend class Transactional<Waste>;
        state_type begin(void) const ;
        void commit(void) ;
        void rollback(state_type const& state) ;
    };
}//namespace com::jlom::klondike::model

#include "src/Waste_impl.hpp"

#endif // __KLONDIKE_MODEL_WASTE_HPP__