#ifndef KLONDIKE_STATUSDECODERSELECTOR_HPP
#define KLONDIKE_STATUSDECODERSELECTOR_HPP

#include "StatusDecoder.hpp"

#include <string>

namespace com::jlom::klondike::view
{

    class StatusDecoderFactory
    {
        using Configuration = std::string;

    public:

        StatusDecoder *GetStatusDecoder(Configuration const &configuration) const;
    };
}

#endif //KLONDIKE_STATUSDECODERSELECTOR_HPP
