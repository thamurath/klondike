#ifndef KLONDIKE_HOTCHPOTCH_HPP
#define KLONDIKE_HOTCHPOTCH_HPP

namespace com::jlom::klondike::hotchpotch
{
    template<typename Card, template<typename T, typename... Args> class Container>
    auto TurnUpFirst(Container<Card> &&cards)
    {
        cards.back().TurnUp();
        return cards;
    }
}
#endif //KLONDIKE_HOTCHPOTCH_HPP
