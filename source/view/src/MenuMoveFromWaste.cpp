#include "MenuMoveFromWaste.hpp"

#include <spdlog/spdlog.h>

namespace com::jlom::klondike::view
{
    MenuItem::Options MenuMoveFromWaste::GetOptions(void)
    {
        spdlog::get("console")->debug("Entering MenuMoveFromWaste::GetOptions");
        MenuItem::Options options;

        options.showText = R"(
Menu Move from Waste to:
1.- Foundation
2.- Pile
 )";

        options.minOption = 1;
        options.maxOption = 2;

        spdlog::get("console")->debug("Exit MenuMoveFromWaste::GetOptions");
        return options;
    }
    void MenuMoveFromWaste::DecodeOption(MenuContext& menuContext)
    {
        spdlog::get("console")->debug("Entering MenuMoveFromWaste::DecodeOption option {}", menuContext.GetMenuOption());
        menuContext.Builder().SetSource("waste").SetSourceType(ElementType::WASTE);
        spdlog::get("console")->debug("Exit MenuMoveFromWaste::DecodeOption ");
    }
}

