#ifndef KLONDIKE_RESIGNCONTROLLER_HPP
#define KLONDIKE_RESIGNCONTROLLER_HPP


#include "ChainableController.hpp"

#include <common/ReturnCodes.hpp>

namespace com::jlom::klondike::controller
{

    class ResignController : public ChainableController
    {
    public:
        ResignController(ACTION_TYPE type)
        : type (type)
        {

        }
        virtual ~ResignController(void) = default;

        // no copy
        ResignController(ResignController const& ) = delete;
        ResignController& operator=(ResignController const& ) = delete;

        // no move
        ResignController(ResignController&& ) noexcept = delete;
        ResignController& operator=(ResignController && ) noexcept = delete;

    private:
        ACTION_TYPE type;

        bool shouldHandle(Action const& action )  override
        {
            return (type == action.GetType());
        }

        bool canHandle(Action const& action) override
        {
            return true;
        }

        std::int32_t handle(Action const& action )
        {
            return RESIGN;
        }
    };
}
#endif //KLONDIKE_RESIGNCONTROLLER_HPP
