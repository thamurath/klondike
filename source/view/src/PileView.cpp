
#include "PileView.hpp"
#include "StatusDecoder.hpp"

#include <common/Status.hpp>

#include <spdlog/spdlog.h>

#include <sstream>
namespace com::jlom::klondike::view
{
    PileView::PileView(std::string id)
    : OutView(id)
    {

    }

    void PileView::DecodeStatus(const StatusDecoder &statusDecoder, Status const &status)
    {
        spdlog::get("console")->trace("PileView::DecodeStatus id:{} ", GetId());
        cards = statusDecoder.Decode(GetId(), status);
        spdlog::get("console")->trace("PileView::DecodeStatus id:{} cards:{}", GetId(), cards.empty());
    }

    void PileView::Show(void)
    {
        //std::size_t numCardsFaceDown(0);
        std::stringstream ss;
        ss << "id:" << GetId() << ",cards:";
        for (auto const& card : cards)
        {
            if (!card.IsFaceUp())
            {
                ss << "<->||";
            }
            else
            {
                ss << card << "||";
            }
        }
        GetView().ShowText(ss.str());
    }
}

