#ifndef KLONDIKE_VIEW_TABLEVIEW_HPP
#define KLONDIKE_VIEW_TABLEVIEW_HPP

#include "PlayerInteraction.hpp"
#include "OutView.hpp"
#include "StatusDecoder.hpp"
#include "CommandBuilder.hpp"

#include <controller/InformationHub.hpp>
#include <controller/ViewInterface.hpp>

#include <common/StatusNotification.hpp>
#include <common/ReturnCodes.hpp>


#include <vector>
#include <memory>
#include <string>


namespace com::jlom::klondike::view
{
    class TableView final
    {
    public:
        using Configuration = std::string;
        TableView(Configuration const& config, controller::InformationHub& informationHub);
        ~TableView(void) = default;

        // no copy
        TableView(TableView const&) = delete;
        TableView& operator=(TableView const&) = delete;

        // no movement ( maybe too restrictive)
        TableView(TableView&&) = delete;
        TableView& operator=(TableView&&) = delete;

        void HandleNotification(StatusNotification* statusNotification);

        template <typename T>
        std::int32_t Run(controller::ViewInterface<T>& interface)
        {
            const auto cmdShowStatus = CommandBuilder().SetType(CommandType::SHOW_STATUS).Build();
            interface.ProcessCommand(cmdShowStatus);
            std::int32_t retCode;

            do {
                retCode = interface.ProcessCommand(playerInteraction->GetCommand());
                interface.ProcessCommand(cmdShowStatus);
            }while ( (WIN != retCode) && (RESIGN != retCode) );

            return retCode;
        }

    private:
        std::vector<std::unique_ptr<OutView>> outViews;
        std::unique_ptr<PlayerInteraction> playerInteraction;
        std::unique_ptr<StatusDecoder> statusDecoder;
        controller::InformationHub& informationHub;





    };
}

#endif //KLONDIKE_VIEW_TABLEVIEW_HPP
