#ifndef KLONDIKE_INTERACTIONTECHNOLOGYSELECTOR_HPP
#define KLONDIKE_INTERACTIONTECHNOLOGYSELECTOR_HPP

#include "ViewTechAbstractFactory.hpp"

namespace com::jlom::klondike::view
{
    class InteractionTechnologySelector
    {
        ///@jlom TODO implement configuration
        using Configuration = std::string;
    public:
        ViewTechAbstractFactory* GetViewTechnologyFactory(Configuration const& configuration);
    };
}

#endif //KLONDIKE_INTERACTIONTECHNOLOGYSELECTOR_HPP
