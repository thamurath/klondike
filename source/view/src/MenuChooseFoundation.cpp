#include "MenuChooseFoundation.hpp"

#include <cassert>
namespace com::jlom::klondike::view
{
    MenuItem::Options MenuChooseFoundation::GetOptions(void)
    {
        MenuItem::Options options;

        options.showText = R"(
Menu Choose Foundation
1.- Foundation one
2.- Foundation two
3.- Foundation three
4.- Foundation four
 )";
        options.minOption = 1;
        options.maxOption = 4;

        return options;
    }

    void MenuChooseFoundation::DecodeOption(MenuContext& menuContext)
    {
        ///@jlom TODO: This needs access to configuration for the foundation ids
        menuContext.Builder().SetDestinationType(ElementType::FOUNDATION).SetNumberOfCards(1);
        switch (menuContext.GetMenuOption())
        {
            case 1:
            {
                menuContext.Builder().SetDestination("foundation_1");
                break;
            }
            case 2:
            {
                menuContext.Builder().SetDestination("foundation_2");
                break;
            }
            case 3:
            {
                menuContext.Builder().SetDestination("foundation_3");
                break;
            }
            case 4:
            {
                menuContext.Builder().SetDestination("foundation_4");
                break;
            }
            default:
                assert(false);
        }
    }
}

