#ifndef KLONDIKE_MENUMOVECARD_HPP
#define KLONDIKE_MENUMOVECARD_HPP

#include "MenuItem.hpp"

namespace com::jlom::klondike::view
{
    class MenuMoveCard : public MenuItem
    {
    private:
        MenuItem::Options GetOptions(void) override ;
        void DecodeOption(MenuContext& menuContext) override ;
    };
}

#endif //KLONDIKE_MENUMOVECARD_HPP
