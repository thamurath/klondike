#ifndef KLONDIKE_MENUITEM_HPP
#define KLONDIKE_MENUITEM_HPP

#include "Menu.hpp"

#include <common/Command.hpp>

#include <string>
#include <cinttypes>
namespace com::jlom::klondike::view
{
    class MenuItem : public Menu
    {
    public:
        Command Handle(MenuContext &menuContext) override ;

    protected:
        struct Options
        {
            std::string showText;
            std::uint32_t maxOption;
            std::uint32_t minOption;
        };

    private:
        void ShowMenuOptions(MenuContext &menuContext, std::string const &menuOptions);
        MenuOption GetOptionNumber(MenuContext &menuContext, std::uint32_t min, std::uint32_t max);


        virtual Options GetOptions(void) = 0;
        virtual void DecodeOption(MenuContext& menuContext) = 0;
    };
}

#endif //KLONDIKE_MENUITEM_HPP
