#include "StatusDecoderSelector.hpp"

#include <cassert>

namespace com::jlom::klondike::view
{
    StatusDecoder* StatusDecoderSelector::GetStatusDecoder(const std::string &configuration)
    {
        if (configuration == "json")
        {
            return new JsonStatusEncoder;
        }

        assert(false);
    }
}