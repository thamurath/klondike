#ifndef KLONDIKE_MODEL_EXCEPTIONS_HPP
#define KLONDIKE_MODEL_EXCEPTIONS_HPP

#include <exception>
#include <string>

namespace com::jlom::klondike::model::exceptions
{
    class ElementNotFound : public std::exception
    {
    public:
        explicit ElementNotFound(std::string const& id);
        virtual ~ElementNotFound(void) = default;

        const char* what() const noexcept override ;


    private:
        std::string id;
    };


    class NonTransactionalItem : public std::exception
    {
    public:
        explicit NonTransactionalItem(std::string const& id);
        virtual ~NonTransactionalItem(void) = default;

        const char* what() const noexcept override ;


    private:
        std::string id;
    };
}


#endif //KLONDIKE_MODEL_EXCEPTIONS_HPP
