

#include "KlondikeApp.hpp"

int main(int argc, char** argv)
{
    com::jlom::klondike::KlondikeApp app;
    return app.Run(argc, argv);
}
