#ifndef KLONDIKE_MENU_HPP
#define KLONDIKE_MENU_HPP

#include "MenuContext.hpp"
#include "PlayerInteraction.hpp"

#include <common/Command.hpp>

#include <memory>


namespace com::jlom::klondike::view
{

    class Menu
    {
    public:
        Menu(void) = default;
        virtual ~Menu(void) = default;

        Command GetCommand(PlayerInteraction& playerInteraction);

        void AddChild(std::unique_ptr<Menu> child);

    protected:
        PlayerInteraction& GetPlayerInteraction(void);
        Command DelegateHanlding(MenuContext &menuContext);
        Command DelegateHanlding(std::unique_ptr<Menu> const& delegate, MenuContext& menuContext);

    private:
        std::unique_ptr<Menu> child;

        virtual Command Handle(MenuContext &menuContext) = 0;
    };
}

#endif //KLONDIKE_MENU_HPP
