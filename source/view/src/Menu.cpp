#include "Menu.hpp"

#include "CommandBuilder.hpp"


#include <spdlog/spdlog.h>

#include <cassert>
#include <utility>

namespace com::jlom::klondike::view
{
    Command Menu::GetCommand(PlayerInteraction& ai_playerInteraction)
    {
        ///~jlom TODO: esta manera de pasar la viewTech no me gusta nada de nada
        /// quizas podria pasarla en el constructor de Menu y que sea la quedase
        /// durante toda la vida del objeto ... total tampoco creo que cambie demaasiado

        spdlog::get("console")->trace("Entering GetCommand");
        MenuContext menuContext(CommandBuilder{}, MenuOption{}, ai_playerInteraction);
        return Handle(menuContext);
    }


    void Menu::AddChild(std::unique_ptr<Menu> child)
    {
        this->child = std::move(child);
    }

    Command Menu::DelegateHanlding(MenuContext& menuContext)
    {
        spdlog::get("console")->trace("Entering Menu::DelegateHanlding Child");
        assert(child);
        return DelegateHanlding(child, menuContext);
    }

    Command Menu::DelegateHanlding(std::unique_ptr<Menu> const& delegate, MenuContext& menuContext)
    {
        spdlog::get("console")->trace("Entering Menu::DelegateHanlding ");
        return delegate->Handle(menuContext);
    }
}