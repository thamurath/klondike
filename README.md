# README #

### What is this repository for? ###

* Quick summary

Just a simplistic clone of klondike for a master.

__Note: develop is the main branch of development.__

* Version

1.0 - Basic interaction.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Ensure you have internet conectivity ...

Just download the repo. Once git clone has finished.
```
> mkdir build
> cd build
> cmake ..
```

this should take a while the first time cause it has to download/build some  3pp libraries

```
> make klondike
```


* Configuration
* Dependencies

This klondike version depends on:

- spdlog -> For logging and tracing

- better_enums -> To enchance c++ enums

- Poco NotificationCenter -> To share notifications

- Catch -> To unittesting
