#ifndef _KLONDIKE_INCLUDE_OPTIONAL_HPP__
#define _KLONDIKE_INCLUDE_OPTIONAL_HPP__

/**
 * __cplusplus values:
 * 199711L -> c++98
 * 201103L -> c++11
 * 201402L -> c++14
 * >201402L -> c++1z ( experimental right now(201512))
 *
 * @see https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
 * __cplusplus:
 * This macro is defined when the C++ compiler is in use. You can use __cplusplus to test whether a header
 * is compiled by a C compiler or a C++ compiler. This macro is similar to __STDC_VERSION__, in that it
 * expands to a version number. Depending on the language standard selected, the value of the macro is
 * 199711L for the 1998 C++ standard, 201103L for the 2011 C++ standard, 201402L for the 2014 C++ standard,
 * or an unspecified value strictly larger than 201402L for the experimental languages enabled by
 * -std=c++1z and -std=gnu++1z.
 **/
/// from c++11 to c++14 both included... need experimental
#if (__cplusplus >= 201103L) && ( __cplusplus <= 201402L)
#include <experimental/optional>
namespace std::optional = std::experimental::optional;
#else
#include <optional>
#endif

#endif //_KLONDIKE_INCLUDE_OPTIONAL_HPP__
