#ifndef KLONDIKE_RETURNCODES_HPP
#define KLONDIKE_RETURNCODES_HPP

namespace com::jlom::klondike
{
    enum ReturnCodes
    {
        ERROR = -1,
        SUCCESS = 0,
        WIN,
        RESIGN,
    };
}

#endif //KLONDIKE_RETURNCODES_HPP
