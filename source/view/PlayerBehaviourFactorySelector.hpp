#ifndef KLONDIKE_PLAYERBEHAVIOURFACTORYSELECTOR_HPP
#define KLONDIKE_PLAYERBEHAVIOURFACTORYSELECTOR_HPP

#include "PlayerInteractionAbstractFactory.hpp"

#include <string>
namespace com::jlom::klondike::view
{
    class PlayerBehaviourFactorySelector
    {
        ///@jlom TODO implement configuration
        using Configuration = std::string;
    public:
        PlayerInteractionAbstractFactory* GetPlayerInteractionFactory(Configuration const& configuration) const;

    };
}

#endif //KLONDIKE_PLAYERBEHAVIOURFACTORYSELECTOR_HPP
