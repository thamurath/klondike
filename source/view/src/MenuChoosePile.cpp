#include "MenuChoosePile.hpp"

#include <cassert>
namespace com::jlom::klondike::view
{
    MenuItem::Options MenuChoosePile::GetOptions(void)
    {
        MenuItem::Options options;

        options.showText = R"(
Menu Choose Pile
1.- Pile one
2.- Pile two
3.- Pile three
4.- Pile four
5.- Pile five
6.- Pile six
7.- Pile seven
 )";
        options.minOption = 1;
        options.maxOption = 7;

        return options;
    }

}