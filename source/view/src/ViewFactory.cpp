#include "ViewFactory.hpp"

#include "DeckView.hpp"
#include "PileView.hpp"
#include "FoundationView.hpp"
#include "WasteView.hpp"

#include "HumanInteractionView.hpp"
#include "DummyAIInteractionView.hpp"

#include "ConsoleViewTechFactory.hpp"
#include "GUIViewTechFactory.hpp"

#include <cassert>

namespace com::jlom::klondike::view
{

    ViewFactory::ViewFactory(ViewTechAbstractFactory *viewTechAbstractFactory)
    : viewTechAbstractFactory(viewTechAbstractFactory)
    {
        assert(nullptr != viewTechAbstractFactory);
    }
    DeckView* ViewFactory::GetDeck(std::string const& id) const
    {
        return GenerateView<DeckView>(id);
    }

    PileView* ViewFactory::GetPile(std::string const& id) const
    {
        return GenerateView<PileView>(id);
    }

    WasteView* ViewFactory::GetWaste(std::string const& id) const
    {
        return GenerateView<WasteView>(id);
    }

    FoundationView* ViewFactory::GetFoundation(std::string const& id) const
    {
        return GenerateView<FoundationView>(id);
    }

    template <typename T>
    T* ViewFactory::GenerateView(std::string const& id) const
    {
        auto view = new T(id);
        view->SetViewTech(viewTechAbstractFactory->GetViewTechnology());
        return view;
    }
}

