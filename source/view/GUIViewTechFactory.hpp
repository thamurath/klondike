#ifndef KLONDIKE_GUIVIEWTECHFACTORY_HPP
#define KLONDIKE_GUIVIEWTECHFACTORY_HPP

#include "ViewTechAbstractFactory.hpp"

namespace com::jlom::klondike::view
{
    class GUIViewTechFactory : public ViewTechAbstractFactory
    {
    public:
        ViewTechImplementation* GetViewTechnology() override ;


    };
}

#endif //KLONDIKE_GUIVIEWTECHFACTORY_HPP
