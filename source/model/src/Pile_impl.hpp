#ifndef KLONDIKE_MODEL_PILE_IMPL_HPP
#define KLONDIKE_MODEL_PILE_IMPL_HPP

#ifndef __KLONDIKE_MODEL_PILE_HPP__
#error "Should not include this file"
#endif

#include "better_enums.hpp"
#include "optional.hpp"

#include <cassert>
#include <algorithm>
#include <string>
#include <utility>

namespace com::jlom::klondike::model
{
    template <typename Card, template <typename C> class Heap>
    Pile<Card, Heap>::Pile(std::uint8_t ai_id, std::vector<Card>&& cards)
            : m_id(ai_id)
            , heap(std::move(cards))
    {
        assert(m_id > 0);
        assert(m_id == heap.Size());
        assert(heap.Get()->IsFaceUp());
    }

    template <typename Card, template <typename C> class Heap>
    auto Pile<Card, Heap>::IsEmpty(void) const
    {
        return heap.IsEmpty();
    }

    template <typename Card, template <typename C> class Heap>
    auto Pile<Card, Heap>::Size(void) const
    {
        return heap.Size();
    }

    template <typename Card, template <typename C> class Heap>
    auto Pile<Card, Heap>::Get(void) const
    {
        assert(!heap.IsEmpty());
        return heap.Get();
    }

    template <typename Card, template <typename C> class Heap>
    auto Pile<Card, Heap>::GetCards(std::size_t const& numCards) const
    {
        return heap.GetCards(numCards);
    };

    template <typename Card, template <typename C> class Heap>
    void Pile<Card,Heap>::Pop(void)
    {
        assert(!heap.IsEmpty());
        heap.Pop();
    }

    template <typename Card, template <typename C> class Heap>
    auto Pile<Card, Heap>::Push(Card&& card)
    {
        if (!heap.IsEmpty())
        {
            auto lastCard = heap.Get();
            if ( lastCard->IsFaceUp() )
            {
                assert( lastCard->GetColor() != card.GetColor());
                assert( isPrevious<typename Card::FigureType>(card.GetFigure(), lastCard->GetFigure()));
            }
        }
        return heap.Push(std::move(card));
    }

    template <typename Card, template <typename C> class Heap>
    template <typename StatusEncoder>
    auto Pile<Card, Heap>::EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const
    {
        assert(nullptr != statusEncoder);
        return heap.EncodeStatus(id, statusEncoder);
    }

    template <typename Card, template <typename C> class Heap>
    typename Pile<Card, Heap>::state_type Pile<Card, Heap>::begin(void) const
    {
        return Pile::state_type(m_id, heap.GetAllCards());
    }

    template <typename Card, template <typename C> class Heap>
    void Pile<Card, Heap>::commit(void)
    {
        //nothing to do.
    }

    template <typename Card, template <typename C> class Heap>
    void Pile<Card, Heap>::rollback(typename Pile<Card, Heap>::state_type const& state)
    {
        assert(state.m_id == m_id);
        auto cards = state.m_cards;
        heap.Reset(std::move(cards));
    }
}

#endif //KLONDIKE_PILE_IMPL_HPP
