#include "AIInteractionFactory.hpp"


#include "DummyAIInteractionView.hpp"

namespace com::jlom::klondike::view
{
    PlayerInteraction* AIInteractionFactory::GetPlayerInteraction(std::string const &id)
    {
        return new DummyAIInteractionView(id);
    }
}

