#ifndef KLONDIKE_CONTROLLER_CONTROLLERFACTORY_HPP
#define KLONDIKE_CONTROLLER_CONTROLLERFACTORY_HPP

#include "ChainableController.hpp"

#include <memory>

namespace com::jlom::klondike::controller
{
    template<typename ModelInterface>
    class ControllerFactory final
    {
    public:
        ControllerFactory(ModelInterface& modelInterface);
        ~ControllerFactory(void) = default;

        //avoid copy
        ControllerFactory(ControllerFactory const&) = delete;
        ControllerFactory& operator=(ControllerFactory const&) = delete;

        //avoid movement
        ControllerFactory(ControllerFactory&&) noexcept = delete;
        ControllerFactory& operator=(ControllerFactory&&) noexcept = delete;

        std::unique_ptr<ChainableController> GetController(ACTION_TYPE type) const;

    protected:
    private:
        ModelInterface& modelInterface;

        
    };
}

#include "src/ControllerFactory_impl.hpp"

#endif //KLONDIKE_CONTROLLER_CONTROLLERFACTORY_HPP