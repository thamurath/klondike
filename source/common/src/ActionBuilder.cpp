#include "ActionBuilder.hpp"

namespace com::jlom::klondike
{
    ActionBuilder& ActionBuilder::SetSourceId(std::string sourceId)
    {
        this->sourceId = sourceId;
        return *this;
    }

    ActionBuilder& ActionBuilder::SetDestinationId( std::string destinationId)
    {
        this->destinationId = destinationId;
        return *this;
    }

    ActionBuilder& ActionBuilder::SetNumCards(std::size_t numCards)
    {
        this->numCards = numCards;
        return *this;
    }

    ActionBuilder& ActionBuilder::SetActionType(ACTION_TYPE type)
    {
        this->type = type;
        return *this;
    }

    Action ActionBuilder::Build(void) const
    {
        return Action(*type, sourceId.value_or(""), destinationId.value_or(""), numCards.value_or(0));
    }
}