#ifndef KLONDIKE_MENUCONTEXT_HPP
#define KLONDIKE_MENUCONTEXT_HPP

#include "CommandBuilder.hpp"
#include "PlayerInteraction.hpp"

#include <cinttypes>

namespace com::jlom::klondike::view
{


    using MenuOption = std::uint32_t;
    class MenuContext
    {
    public:
        MenuContext(CommandBuilder commandBuilder, MenuOption menuOption, PlayerInteraction& playerInteraction);

        // no copy
        MenuContext(MenuContext const&) = delete;
        MenuContext& operator=(MenuContext const&) = delete;

        // no move
        MenuContext(MenuContext&&) = delete;
        MenuContext& operator=(MenuContext&&) = delete;

        CommandBuilder& Builder(void);
        MenuOption const& GetMenuOption(void) const;
        PlayerInteraction& GetPlayerInteraction(void);

        void SetMenuOption(MenuOption const& menuOption);

    private:
        CommandBuilder commandBuilder;
        MenuOption menuOption;
        PlayerInteraction& playerInteraction;
    };
}

#endif //KLONDIKE_MENUCONTEXT_HPP
