#ifndef KLONDIKE_WASTEVIEW_HPP
#define KLONDIKE_WASTEVIEW_HPP

#include "OutView.hpp"

#include "CardView.hpp"
#include "StatusDecoder.hpp"

#include <vector>
#include <string>

namespace com::jlom::klondike::view
{
    class WasteView : public OutView
    {
    public:
        explicit WasteView(std::string id);
        virtual ~WasteView() = default;

        void DecodeStatus(const StatusDecoder &statusDecoder, Status const &status) override ;
        void Show(void) override ;

    private:
        std::vector<CardView> cards;
    };
}

#endif //KLONDIKE_WASTEVIEW_HPP
