
#include "TableView.hpp"

#include "PlayerBehaviourFactorySelector.hpp"
#include "InteractionTechnologySelector.hpp"
#include "StatusDecoderFactory.hpp"

#include "ViewFactory.hpp"
#include "DeckView.hpp"
#include "WasteView.hpp"
#include "PileView.hpp"
#include "FoundationView.hpp"

#include "PlayerInteraction.hpp"

#include "CommandBuilder.hpp"

#include <common/ObserverAdapter.hpp>
#include <common/ReturnCodes.hpp>

#include <string>
#include <sstream>
#include <memory>
#include <cassert>
namespace com::jlom::klondike::view
{

    TableView::TableView(Configuration const &config, controller::InformationHub& informationHub)
    : informationHub(informationHub)

    {
        StatusDecoderFactory statusDecoderFactory;
        statusDecoder = std::move(std::unique_ptr<StatusDecoder>(statusDecoderFactory.GetStatusDecoder(config)));


        PlayerBehaviourFactorySelector playerBehaviourFactorySelector;
        auto factory = playerBehaviourFactorySelector.GetPlayerInteractionFactory(config);
        assert(nullptr != factory);
        playerInteraction = std::move(std::unique_ptr<PlayerInteraction>(factory->GetPlayerInteraction("incoming")));
        delete factory;

        InteractionTechnologySelector interactionTechnologySelector;
        auto viewTechFactory = interactionTechnologySelector.GetViewTechnologyFactory(config);

        playerInteraction->SetViewTech(viewTechFactory->GetViewTechnology());
        ViewFactory viewFactory(viewTechFactory);


        ///@jlom TODO : read configuration to see how many elements should we build of each one and the ids...
        std::unique_ptr<DeckView> ptrDeck(viewFactory.GetDeck("deck"));
        outViews.push_back(std::move(ptrDeck));

        std::unique_ptr<WasteView> ptrWaste(viewFactory.GetWaste("waste"));
        outViews.push_back(std::move(ptrWaste));

        const std::string pileId("pile_");
        for(std::uint32_t idx(1); idx < 8; ++idx)
        {
            std::stringstream ss;
            ss << pileId << idx;

            spdlog::get("console")->debug("TableView creating Pile: id:{}", ss.str());

            std::unique_ptr<PileView> ptrPile(viewFactory.GetPile(ss.str()));
            outViews.push_back(std::move(ptrPile));
        }

        const std::string foundationId("foundation_");
        for(std::uint32_t idx(1); idx < 5; ++idx)
        {
            std::stringstream ss;
            ss << foundationId << idx;

            spdlog::get("console")->debug("TableView creating Foundation: id:{}", ss.str());

            std::unique_ptr<FoundationView> ptrFoundation(viewFactory.GetFoundation(ss.str()));
            outViews.push_back(std::move(ptrFoundation));
        }
        spdlog::get("console")->debug("TableView AddingObserver: ");
        informationHub.AddObserver(ObserverAdapter<TableView, StatusNotification>(*this, &TableView::HandleNotification));
        spdlog::get("console")->debug("TableView Done: ");
    }


    void TableView::HandleNotification(StatusNotification* statusNotification)
    {
        spdlog::get("console")->debug("TableView::HandleNotification {}", statusNotification->GetStatus().str());
        for( auto& outview : outViews)
        {
            outview->DecodeStatus(*statusDecoder, statusNotification->GetStatus());
        }

        for(auto& outview : outViews)
        {
            outview->Show();
        }
    }


}