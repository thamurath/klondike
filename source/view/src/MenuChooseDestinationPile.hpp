#ifndef KLONDIKE_MENUCHOOSEDESTINATIONPILE_HPP
#define KLONDIKE_MENUCHOOSEDESTINATIONPILE_HPP

#include "MenuChoosePile.hpp"

namespace com::jlom::klondike::view
{
    class MenuChooseDestinationPile: public MenuChoosePile
    {
    private:
        void DecodeOption(MenuContext& menuContext) override ;

    };

}
#endif //KLONDIKE_MENUCHOOSEDESTINATIONPILE_HPP
