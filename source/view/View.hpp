#ifndef KLONDIKE_VIEW_IVIEW_HPP
#define KLONDIKE_VIEW_IVIEW_HPP

#include "ViewTechImplementation.hpp"

namespace com::jlom::klondike::view
{
    class View
    {
    public:
        explicit View(std::string const& id);
        virtual ~View();

        void SetViewTech(ViewTechImplementation* viewTechImplementation);

       


    protected:
        ViewTechImplementation& GetView(void) const;
        std::string const& GetId(void) const;
    private:
        ViewTechImplementation* viewTechImplementation;
        std::string id;
    };
}

#endif //KLONDIKE_VIEW_IVIEW_HPP
