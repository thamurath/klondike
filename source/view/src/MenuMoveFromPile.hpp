#ifndef KLONDIKE_MENUMOVEFROMPILE_HPP
#define KLONDIKE_MENUMOVEFROMPILE_HPP


#include "MenuItem.hpp"

namespace com::jlom::klondike::view
{
    class MenuMoveFromPile : public MenuItem
    {
    private:
        MenuItem::Options GetOptions(void) override ;
        void DecodeOption(MenuContext& menuContext) override ;

    };
}
#endif //KLONDIKE_MENUMOVEFROMPILE_HPP
