#include "MenuMoveCard.hpp"

namespace com::jlom::klondike::view
{
    MenuItem::Options MenuMoveCard::GetOptions(void)
    {
        MenuItem::Options options;

        options.showText = R"(
Move Card Menu: Source
1.- Deck
2.- Waste
3.- Pile
 )";
        options.minOption = 1;
        options.maxOption = 3;

        return options;
    }

    void MenuMoveCard::DecodeOption(MenuContext &menuContext)
    {
        // if source is either Deck or Waste the number of cards will be one
        if ((1 == menuContext.GetMenuOption()) || (2 == menuContext.GetMenuOption()) )
        {
            menuContext.Builder().SetNumberOfCards(1);
        }
    }
}

