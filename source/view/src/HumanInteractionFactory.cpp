#include "HumanInteractionFactory.hpp"

#include "HumanInteractionView.hpp"

namespace com::jlom::klondike::view
{
    PlayerInteraction * HumanInteractionFactory::GetPlayerInteraction(std::string const &id)
    {
        return new HumanInteractionView(id);
    }
}

