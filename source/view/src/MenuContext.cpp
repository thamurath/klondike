#include "MenuContext.hpp"

#include "CommandBuilder.hpp"

namespace com::jlom::klondike::view
{
    MenuContext::MenuContext(CommandBuilder commandBuilder, MenuOption menuOption, PlayerInteraction& playerInteraction)
    : commandBuilder(std::move(commandBuilder))
    , menuOption(menuOption)
    , playerInteraction(playerInteraction)
    {

    }

    CommandBuilder& MenuContext::Builder(void)
    {
        return commandBuilder;
    }

    PlayerInteraction& MenuContext::GetPlayerInteraction(void)
    {
        return playerInteraction;
    }

    MenuOption const& MenuContext::GetMenuOption(void) const
    {
        return menuOption;
    }

    void MenuContext::SetMenuOption(MenuOption const &menuOption)
    {
        this->menuOption = menuOption;
    }
}