#ifndef KLONDIKE_MODEL_CARDHEAP_IMPL_HPP
#define KLONDIKE_MODEL_CARDHEAP_IMPL_HPP

#ifndef KLONDIKE_MODEL_CARDHEAP_HPP
#error "Should not include this file"
#endif

#include <cassert>

namespace com::jlom::klondike::model
{
    template <typename Card>
    CardHeap<Card>::CardHeap(std::vector<Card>&& cards)
    : cards(std::move(cards))
    {

    }
    template <typename Card>
    bool CardHeap<Card>::IsEmpty(void) const
    {
        return cards.empty();
    }

    template <typename Card>
    std::size_t CardHeap<Card>::Size(void) const
    {
        return cards.size();
    }
    template <typename Card>
    std::optional<Card> CardHeap<Card>::Get(void) const
    {
        if (cards.empty())
        {
            return {};
        }
        return cards.back();
    }

    template <typename Card>
    std::vector<Card> CardHeap<Card>::GetCards(std::size_t const& numCards) const
    {
        std::vector<Card> ret;
        ret.reserve(numCards);
        std::size_t num = std::min(numCards, cards.size());
        const std::size_t diff = cards.size() - num;
        std::copy(std::begin(cards) + diff, std::end(cards), std::back_inserter(ret));
        return ret;
    }

    template <typename Card>
    std::vector<Card> CardHeap<Card>::GetAllCards(void) const
    {
        return GetCards(cards.size());
    }

    template <typename Card>
    void CardHeap<Card>::Pop(void)
    {
        cards.pop_back();
    }

    template< typename Card>
    std::int32_t CardHeap<Card>::Push(Card&& card)
    {
        cards.push_back(std::move(card));
        return 0;
    }

    template<typename Card>
    void CardHeap<Card>::Reset(std::vector<Card>&& card)
    {
        std::swap(card, cards);
    }

    template <typename Card>
    template <typename StatusEncoder>
    auto CardHeap<Card>::EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const
    {
        assert(nullptr != statusEncoder);
        return statusEncoder->Encode(id, GetAllCards());
    }
}

#endif //KLONDIKE_MODEL_CARDHEAP_IMPL_HPP
