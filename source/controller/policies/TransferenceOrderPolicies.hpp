#ifndef KLONDIKE_CONTROLLER_TRANSFERENCEORDERPOLICIES_HPP
#define KLONDIKE_CONTROLLER_TRANSFERENCEORDERPOLICIES_HPP

#include <common/Action.hpp>

#include "type_traits.hpp"
#include "spdlog/spdlog.h"
#include "pretty_print.h"

#include <algorithm>
#include <cstdlib>
#include <ostream>

namespace com::jlom::klondike::controller::policies::transference_order
{
    class ReverseOrder
    {
    public:
        template<typename Container>
        void ChangeOrder(Container& cards)
        {
            static_assert(has_iterator<Container>::value, "Must have a iterator typedef: ");
            static_assert(has_begin_end<Container>::value, "Must have begin and end member functions");

            std::reverse(std::begin(cards), std::end(cards));

            spdlog::get("console")->info("ReverseOrder:");
        }
    };

    class NoChangeOrder
    {
    public:
        template<typename Container>
        void ChangeOrder(Container& cards)
        {
            //nothing to  do
            spdlog::get("console")->debug("NoChangeOrder:");
        }
    };
}

#endif //KLONDIKE_CONTROLLER_TRANSFERENCEORDERPOLICIES_HPP