#include "JsonStatusDecoder.hpp"

#include <view/CardViewJsonEncoderHelper.hpp>

#include <spdlog/spdlog.h>

#include <vector>

namespace com::jlom::klondike
{
    std::vector<view::CardView> JsonStatusDecoder::Decode(std::string const &id, view::Status status) const
    {
        spdlog::get("console")->trace("JsonStatusDecoder::Decode id{} Status: {}", id, status.str());
        using json = nlohmann::json;

        auto jsonStatus = nlohmann::json::parse(status.str());
        spdlog::get("console")->trace("JsonStatusDecoder::Decode jsonStatus{}", jsonStatus.dump());

        auto element = jsonStatus.find(id);
        if (jsonStatus.end() == element)
        {
            spdlog::get("console")->error("JsonStatusDecoder::Decode Element not found {}", id);
            throw "element not found";
        }
        spdlog::get("console")->trace("JsonStatusDecoder::Decode element{}", element->dump());

        std::vector <view::CardView> cards;

        for (auto &card : *element)
        {
            spdlog::get("console")->trace("JsonStatusDecoder::Decode card{}", card.dump());
            view::CardView cardView = card;
            std::stringstream ss;
            ss << cardView;
            spdlog::get("console")->trace("JsonStatusDecoder::Decode cardView{}", ss.str());
            cards.push_back(cardView);
        }
        return cards;
    }
}