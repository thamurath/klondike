#ifndef KLONDIKE_CARDVIEW_HPP
#define KLONDIKE_CARDVIEW_HPP

#include <ostream>
#include <string>

namespace com::jlom::klondike::view
{
    class CardView
    {
    public:
        CardView(void);
        CardView(std::string figure, std::string suit, std::string color);

        void SetFigure(std::string const& figure);
        void SetSuit(std::string const& suit);
        void SetColor(std::string const& color);
        void SetFaceUp(bool faceup);

        bool IsFaceUp(void) const;

    private:
        std::string figure;
        std::string suit;
        std::string color;
        bool faceUp;

        friend std::ostream& operator<< (std::ostream& os, CardView const& obj);
    };


}

#endif //KLONDIKE_CARDVIEW_HPP
