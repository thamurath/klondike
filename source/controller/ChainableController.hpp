//
// Created by feanaro on 15/10/17.
//

#ifndef KLONDIKE_CONTROLLER_CHAINCONTROLLER_HPP
#define KLONDIKE_CONTROLLER_CHAINCONTROLLER_HPP

#include <common/Action.hpp>

#include <memory>

namespace com::jlom::klondike::controller
{
    class ChainableController
    {
    public:
        ChainableController(void) = default;
        virtual ~ChainableController(void) = default;

        std::int32_t operator()(Action const& action);

        void SetHandler(std::unique_ptr<ChainableController>&& handler );
        void AddHandler(std::unique_ptr<ChainableController>&& handler );

        //avoid copy
        ChainableController(ChainableController const&) = delete;
        ChainableController& operator=(ChainableController const&) = delete;

        //avoid movement
        ChainableController(ChainableController&&) noexcept = delete;
        ChainableController& operator=(ChainableController&&) noexcept = delete;

    private:
        std::unique_ptr<ChainableController> next;

        virtual bool shouldHandle(Action const& action )  = 0;
        virtual bool canHandle(Action const& action) = 0;
        virtual std::int32_t handle(Action const& action ) = 0;
    };
}


#endif //KLONDIKE_CONTROLLER_CHAINCONTROLLER_HPP
