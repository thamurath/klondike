#ifndef KLONDIKE_MODEL_CARDHEAP_HPP
#define KLONDIKE_MODEL_CARDHEAP_HPP

#include <vector>
#include <cstdlib>
#include <optional.hpp>

namespace com::jlom::klondike::model
{
    template <typename Card>
    class CardHeap final
    {
    public:
        CardHeap(void) = default;
        CardHeap(std::vector<Card>&& cards);

        ~CardHeap() = default;
        //no copy
        CardHeap(CardHeap const&) = delete;
        CardHeap& operator=(CardHeap const&) = delete;
        // no movement
        CardHeap(CardHeap&&) noexcept = default;
        CardHeap& operator=(CardHeap&&) noexcept = default;



        bool IsEmpty(void) const;
        std::size_t Size(void) const;
        std::optional<Card> Get(void) const;
        std::vector<Card> GetCards(std::size_t const& numCards) const;
        std::vector<Card> GetAllCards(void) const;
        void Pop(void);
        std::int32_t Push(Card&& card);

        void Reset(std::vector<Card>&& card);

        template <typename StatusEncoder>
        auto EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const;

    protected:
    private:
        std::vector<Card> cards;

    };
}

#include "src/CardHeap_impl.hpp"
#endif //KLONDIKE_MODEL_CARDHEAP_HPP
