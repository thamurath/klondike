#include "MenuInitial.hpp"

#include <spdlog/spdlog.h>

#include <cassert>

namespace com::jlom::klondike::view
{
    MenuItem::Options MenuInitial::GetOptions(void)
    {
        spdlog::get("console")->trace("Entering MenuInitial::GetOptions");
        MenuItem::Options options;

        options.showText = R"(
Initial Menu:
1.- Move command
2.- Resign
3.- Redeal

Choose an option:
 )";
        options.minOption = 1;
        options.maxOption = 3;

        spdlog::get("console")->trace("Exit MenuInitial::GetOptions-> text:{}, min:{}, max:{}",
        options.showText, options.minOption,options.maxOption);
        return  options;
    }
    void MenuInitial::DecodeOption(MenuContext& menuContext)
    {
        spdlog::get("console")->trace("Entering MenuInitial::DecodeOption option:{}", menuContext.GetMenuOption());
        switch (menuContext.GetMenuOption())
        {
            case 1:
            {

                menuContext.Builder().SetType(CommandType::MOVE);
                break;
            }
            case 2:
            {
                menuContext.Builder().SetType(CommandType::RESIGN);
                break;
            }
            case 3:
            {
                menuContext.Builder().SetType(CommandType::REDEAL);
                break;
            }
            default:
                assert(false);
        }
        spdlog::get("console")->trace("Exit MenuInitial::DecodeOption");
    }
}

