#ifndef KLONDIKE_STATUSNOTIFICATION_HPP
#define KLONDIKE_STATUSNOTIFICATION_HPP

#include "NotificationBase.hpp"
#include "Status.hpp"

namespace com::jlom::klondike
{
    class StatusNotification : public NotificationBase
    {
    public:
        StatusNotification(view::Status status)
                : status(std::move(status))
        {

        }

        view::Status const& GetStatus(void) const
        {
            return status;
        }
    private:
        view::Status status;
    };
}

#endif //KLONDIKE_STATUSNOTIFICATION_HPP
