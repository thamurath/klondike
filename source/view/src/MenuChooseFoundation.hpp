#ifndef KLONDIKE_MENUCHOOSEFOUNDATION_HPP
#define KLONDIKE_MENUCHOOSEFOUNDATION_HPP

#include "MenuItem.hpp"

namespace com::jlom::klondike::view
{
    class MenuChooseFoundation: public MenuItem
    {
    private:

        MenuItem::Options GetOptions(void) override ;
        void DecodeOption(MenuContext& menuContext) override ;
    };

}

#endif //KLONDIKE_MENUCHOOSEFOUNDATION_HPP
