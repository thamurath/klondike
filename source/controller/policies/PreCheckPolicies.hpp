#ifndef KLONDIKE_CONTROLLER_PRECHECKPOLICIES_HPP
#define KLONDIKE_CONTROLLER_PRECHECKPOLICIES_HPP

#include <common/Action.hpp>

#include <spdlog/spdlog.h>

#include <cstdlib>

namespace com::jlom::klondike::controller::policies::precheck
{
    class SourceNotEmpty
    {
    public:
        template <typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& modelInterface, Action const& action)
        {
            spdlog::get("console")->debug("SourceNotEmpty:action:{} sourceId: {}"
                    , action.GetType()._to_string()
                    , action.GetSourceId()
            );
            if (modelInterface.IsEmpty(action.GetSourceId()) )
            {
                spdlog::get("console")->error("SourceNotEmpty: source :{}", action.GetSourceId());
                return -1;
            }

            return 0;
        }
    };

    class SourceEmpty : SourceNotEmpty
    {
    public:
        template <typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& modelInterface, Action const& action)
        {
            if (0 == SourceNotEmpty::CheckPreAction(modelInterface, action))
            {
                spdlog::get("console")->error("SourceEmpty: source :{}", action.GetSourceId());
                return -1;
            }
            return 0;
        }
    };

    class DestinationNotEmpty
    {
    public:
        template <typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& modelInterface, Action const& action)
        {
            if (modelInterface.IsEmpty(action.GetDestinationId()) )
            {
                spdlog::get("console")->error("DestinationNotEmpty: source :{}", action.GetDestinationId());
                return -1;
            }

            return 0;
        }
    };

    class DestinationEmpty : DestinationNotEmpty
    {
    public:
        template <typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& modelInterface, Action const& action)
        {
            if (0 == DestinationNotEmpty::CheckPreAction(modelInterface, action))
            {
                spdlog::get("console")->error("DestinationEmpty: source :{}", action.GetDestinationId());
                return -1;
            }
            return 0;
        }
    };

    class SameHeap
    {
    public:
        template <typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& modelInterface, Action const& action)
        {
            spdlog::get("console")->debug("SameHeap: source :{}, destination:{}"
                    , action.GetSourceId()
                    , action.GetDestinationId()
            );
            if (action.GetDestinationId() != action.GetSourceId())
            {
                spdlog::get("console")->error("SameHeap: source :{}, destination:{}"
							  , action.GetSourceId()
							  , action.GetDestinationId()
							  );
                return -1;
            }

            return 0;
        }
    };

    class SameHeap_NotEmpty : SourceNotEmpty, SameHeap
    {
    public:
        template <typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& modelInterface, Action const& action)
        {
            spdlog::get("console")->debug("SameHeap_NotEmpty: source :{}, destination:{}"
                    , action.GetSourceId()
                    , action.GetDestinationId()
            );
            if ( (0 != SameHeap::CheckPreAction(modelInterface, action)) ||
                    (0 != SourceNotEmpty::CheckPreAction(modelInterface, action))
                    )
            {
                spdlog::get("console")->error("SameHeap_NotEmpty: source :{}, destination:{}"
							  , action.GetSourceId()
							  , action.GetDestinationId()
							  );
                return -1;
            }
            return 0;
        }
    };

    class SourceNotEmpty_DestinationEmpty : SourceNotEmpty, DestinationEmpty
    {
    public:
        template <typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& modelInterface, Action const& action)
        {
            if (
                    (0 != SourceNotEmpty::CheckPreAction(modelInterface, action)) ||
                    (0 != DestinationEmpty::CheckPreAction(modelInterface, action))
                )
            {
                spdlog::get("console")->error("SourceNotEmpty_DestinationEmpty: source:{}, destination:{}"
							  , action.GetSourceId()
							  , action.GetDestinationId()
							  );
                return -1;
            }
            return 0;
        }
    };

    class NothingToCheck
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckPreAction(ModelInterface& , Action const& action)
        {
            spdlog::get("console")->error("NothingToCheck: source:{}, destination:{}"
						      , action.GetSourceId()
						      , action.GetDestinationId()
						      );
            return 0;
        }
    };

}

#endif //KLONDIKE_CONTROLLER_PRECHECKPOLICIES_HPP
