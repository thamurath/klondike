#ifndef KLONDIKE_INITIALMENU_HPP
#define KLONDIKE_INITIALMENU_HPP

#include "MenuItem.hpp"

namespace com::jlom::klondike::view
{
    class MenuInitial : public MenuItem
    {
    private:
        MenuItem::Options GetOptions(void) override ;
        void DecodeOption(MenuContext& menuContext) override ;

    };
}

#endif //KLONDIKE_INITIALMENU_HPP
