#ifndef KLONDIKE_JSONTRANSCODERFACTORY_HPP
#define KLONDIKE_JSONTRANSCODERFACTORY_HPP

#include "StatusTranscoderFactory.hpp"
#include "JsonStatusEncoder.hpp"
#include "JsonStatusDecoder.hpp"

namespace com::jlom::klondike
{

    class JsonStatusEncoderFactory : public StatusTranscoderFactory<JsonStatusEncoderFactory>
    {
    private:
        friend class StatusTranscoderFactory<JsonStatusEncoderFactory>;

        auto createStatusEncoder(void) const
        {
            return new JsonStatusEncoder;
        }

    };
}

#endif //KLONDIKE_JSONSTATUSENCODERFACTORY_HPP
