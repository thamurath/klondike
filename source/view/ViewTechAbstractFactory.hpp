#ifndef KLONDIKE_VIEWTECHABSTRACTFACTORY_HPP
#define KLONDIKE_VIEWTECHABSTRACTFACTORY_HPP


#include "ViewTechImplementation.hpp"

namespace com::jlom::klondike::view
{
    class ViewTechAbstractFactory
    {
    public:

        virtual ViewTechImplementation* GetViewTechnology(void) = 0;


        virtual ~ViewTechAbstractFactory() = default;
    };
}

#endif //KLONDIKE_VIEWTECHABSTRACTFACTORY_HPP
