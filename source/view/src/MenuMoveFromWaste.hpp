#ifndef KLONDIKE_MENUMOVEFROMWASTE_HPP
#define KLONDIKE_MENUMOVEFROMWASTE_HPP

#include "MenuItem.hpp"

namespace  com::jlom::klondike::view
{
    class MenuMoveFromWaste : public MenuItem
    {
    private:
        MenuItem::Options GetOptions(void) override ;
        void DecodeOption(MenuContext& menuContext) override ;
    };
}

#endif //KLONDIKE_MENUMOVEFROMWASTE_HPP
