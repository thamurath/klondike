
#ifndef KLONDIKE_JSONSTATUSENCODER_HPP
#define KLONDIKE_JSONSTATUSENCODER_HPP

#include "Status.hpp"

#include <view/StatusDecoder.hpp>
#include <view/CardView.hpp>

#include <json.hpp>

#include <string>
#include <vector>
namespace com::jlom::klondike
{
    class JsonStatusDecoder : public view::StatusDecoder
    {
    public:
        std::vector <view::CardView> Decode(std::string const &id, view::Status status) const override ;
    };

}
#endif //KLONDIKE_JSONSTATUSENCODER_HPP
