#ifndef KLONDIKE_PLAYERINTERACTION_HPP
#define KLONDIKE_PLAYERINTERACTION_HPP

#include "View.hpp"
#include <common/Command.hpp>

namespace com::jlom::klondike::view
{
    class PlayerInteraction : public View
    {
    public:
        explicit PlayerInteraction(std::string const& id) : View(id) {}
        virtual ~PlayerInteraction(void) = default;

        virtual Command GetCommand(void) = 0;


        virtual void ShowText(std::string const& text)
        {
            GetView().ShowText(text);
        }

        virtual std::uint32_t GetNumber( std::uint32_t const& min, std::uint32_t const& max)
        {
            return GetView().GetNumber(min,max);
        }

    };
}

#endif //KLONDIKE_PLAYERINTERACTION_HPP
