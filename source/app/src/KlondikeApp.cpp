#include "KlondikeApp.hpp"

#include <model/english_model/Card.hpp>
#include <model/DefaultFactory.hpp>

#include <view/TableView.hpp>

#include <controller/InformationHub.hpp>
#include <controller/ControllerFactory.hpp>
#include <controller/ChainFactory.hpp>

#include <common/JsonStatusEncoderFactory.hpp>

#include <type_name.hpp>

#include <spdlog/spdlog.h>

#include <string>

namespace com::jlom::klondike
{
    int KlondikeApp::Run(int argc, char **argv)
    {

        namespace common = com::jlom::klondike;
        namespace model = common::model;
        namespace controller = common::controller;

        const std::string configuration("");

        // Log data should be readede from configuration
        // Create a file rotating logger with 5mb size max and 3 rotated files
        auto console = spdlog::rotating_logger_mt("console", "/tmp/klondike", 1048576 * 5, 3);
        //auto console = spdlog::stdout_color_mt("console");
        console->set_level(spdlog::level::debug);
        console->flush_on(spdlog::level::debug);
        console->debug("Entering Klondike Application");

        // Creating StatusEncoder for model status
        using StatusEncodingFactory = common::JsonStatusEncoderFactory;
        StatusEncodingFactory statusEncodingFactory;
        auto statusEncoder = statusEncodingFactory.CreateStatusEncoder();

        // Controller InformationHub to communicate model status to views.
        controller::InformationHub informationHub;

        //Choosing model factory and creating model
        using EnglishModelFactory = model::DefaultFactory<model::english_model::Card, std::remove_pointer_t<decltype(statusEncoder)>, controller::InformationHub>; // aqui estamos poniendo un tipo determinado.
        EnglishModelFactory modelFactory;
        auto table = modelFactory.CreateTable(statusEncoder, &informationHub);

        // ControllerFactory will know how to create controllers for the Actions
        controller::ControllerFactory controllerFactory(table);

        //and ChainFactory will actually create the controllers and bind them into a ChainOfResponsibility
        controller::ChainFactory chainFactory(controllerFactory);

        auto handler = chainFactory.Create<common::ACTION_TYPE>();

        controller::ViewInterface viewControllerInterface(*handler);

        //Create views
        view::TableView tableView(configuration, informationHub);

        /// And now with everything created ...
        return tableView.Run(viewControllerInterface);
    }
}