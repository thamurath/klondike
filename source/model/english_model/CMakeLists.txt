cmake_minimum_required(VERSION 3.7)
project(klondike_model)

# por si queremos cambiarlo para este proyecto en particular
SET(CMAKE_VERBOSE_MAKEFILE OFF)

# Decision: Public headers goes directly in the project root folder
SET(PROJECT_INCLUDE_DIR ${PROJECT_SOURCE_DIR})

# Source files goes in the /src folder
SET(PROJECT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src)


# Esta es una manera de declarar una posible opcion de compilacion que podemos pasarle a CMAKE
option(SEPARATE_BUILD OFF)

# If we are going to build this project alone ( SEPARETE_BUILD ON )
# then we need to add the common include folder to the include path
# because the root CMakeLIsts file it is not execute.
IF(SEPARATE_BUILD)
    IF(SEPARATE_BUILD STREQUAL "ON")
        include_directories("${CMAKE_SOURCE_DIR}/../include")
        include_directories("${CMAKE_SOURCE_DIR}/../lib")
        include_directories("${CMAKE_SOURCE_DIR}/../lib/3pp")
    else()
        include_directories("${SEPARATE_BUILD)") #WTF!?!?
    ENDIF()
ENDIF(SEPARATE_BUILD)

#=== Targets ===
# == Model library ==
set(ENGLISH_MODEL_SOURCES ${PROJECT_SOURCE_DIR}/Card.cpp
        )
set(ENGLISH_MODEL_HEADERS
        ${PROJECT_INCLUDE_DIR}/Card.hpp
        )
target_sources(model PRIVATE ${ENGLISH_MODEL_SOURCES} ${ENGLISH_MODEL_HEADERS})


# == Test DefaultFactory ==
#set(TEST_MODEL_DEFAULT_FACTORY_SOURCES ${PROJECT_SOURCE_DIR}/test_defaultFactory.cpp )
#add_executable(test_model_defaultFactory ${TEST_MODEL_DEFAULT_FACTORY_SOURCES})
#add_dependencies(test_model_defaultFactory catch)
#target_link_libraries(test_model_defaultFactory model)
#add_test(test_model_defaultFactory ${CMAKE_BINARY_DIR}/test_model_defaultFactory)