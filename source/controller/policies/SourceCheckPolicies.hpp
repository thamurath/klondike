#ifndef KLONDIKE_CONTROLLER_SOURCECHECKPOLICIES_HPP
#define KLONDIKE_CONTROLLER_SOURCECHECKPOLICIES_HPP

#include <common/Action.hpp>

#include <spdlog/spdlog.h>

#include <cstdlib>

namespace com::jlom::klondike::controller::policies::sourcecheck
{
    class NotEmpty
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            spdlog::get("console")->debug("NotEmpty: source:{}, numCards:{}", sourceId, numCards);
            if ( modelInterface.IsEmpty(sourceId) )
            {
                spdlog::get("console")->error("NotEmpty: source:{}, numCards:{}", sourceId, numCards);
                return -1;
            }

            return 0;
        }
    };

    class OneCard
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            spdlog::get("console")->debug("OneCard: source:{}, numCards:{}", sourceId, numCards);
            if (1 != numCards)
            {
                spdlog::get("console")->error("OneCard: source:{}, numCards:{}", sourceId, numCards);
                return -1;
            }
            return 0;
        }
    };

    class EnoughCards
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            spdlog::get("console")->debug("EnoughCards: source:{}, size: {}, numCards:{}"
							  , sourceId
							  , modelInterface.Size(sourceId)
							  , numCards);
            if (modelInterface.Size(sourceId) < numCards)
            {
                spdlog::get("console")->error("EnoughCards: source:{}, size: {}, numCards:{}"
							  , sourceId
							  , modelInterface.Size(sourceId)
							  , numCards);
                return -1;
            }
            return 0;
        }
    };

    class AllCards
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            if ( modelInterface.Size(sourceId) != numCards )
            {
                spdlog::get("console")->error("AllCards: source:{}, size: {}, numCards:{}"
							  , sourceId
							  , modelInterface.Size(sourceId)
							  , numCards);
                return -1;
            }
            return 0;
        }
    };

    class AllFaceUp
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            auto cards = modelInterface.GetCards(sourceId, numCards);
            spdlog::get("console")->debug("AllFaceUp: source:{}, size: {}, numCards:{}"
                    , sourceId
                    , std::size(cards)
                    , numCards);
            
            if (std::all_of(std::begin(cards), std::end(cards), [](auto const& card) -> bool { 
                    spdlog::get("console")->debug("AllFaceUp: card {} {} -> IsFaceUp {} "
                        , card.GetFigure()._to_string(), card.GetSuit()._to_string()
                        , card.IsFaceUp()
                        ); 
                    return card.IsFaceUp(); 
                }) 
                )
            {
                return 0;
            }
            spdlog::get("console")->error("AllFaceUp: source:{}, size: {}, numCards:{}"
                    , sourceId
                    , std::size(cards)
                    , numCards);
            return -1;
        }
    };

    class AllFaceDown
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            spdlog::get("console")->debug("AllFaceDown: source:{}, numCards:{}"
                    , sourceId
                    , numCards);

            auto cards = modelInterface.GetCards(sourceId, numCards);
            if (std::all_of(std::begin(cards), std::end(cards), [](auto const& card) -> bool { return !card.IsFaceUp(); }) )
            {
                return 0;
            }

            spdlog::get("console")->error("AllFaceDown: source:{}, size: {}, numCards:{}"
						      , sourceId
						      , std::size(cards)
						      , numCards);
            return -1;
        }
    };

    class NotEmpty_OneCard_FaceDown : NotEmpty, OneCard, AllFaceDown
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            spdlog::get("console")->debug("NotEmpty_OneCard_FaceDown: source:{}, numCards:{}"
                    , sourceId
                    , numCards);
            if (
                    (0 != NotEmpty::CheckBeforeGet(modelInterface, sourceId, numCards)) ||
                    (0 != OneCard::CheckBeforeGet(modelInterface, sourceId, numCards))  ||
                    (0 != AllFaceDown::CheckBeforeGet(modelInterface,sourceId,numCards))
                )
            {
                spdlog::get("console")->error("NotEmpty_OneCard_FaceDown: source:{}, numCards:{}"
							  , sourceId
							  , numCards);
                return -1;
            }
            return 0;
        }
    };

    class NotEmpty_OneCard_FaceUp : NotEmpty, OneCard, AllFaceUp
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            if (
                    (0 != NotEmpty::CheckBeforeGet(modelInterface, sourceId, numCards)) ||
                    (0 != OneCard::CheckBeforeGet(modelInterface, sourceId, numCards))  ||
                    (0 != AllFaceUp::CheckBeforeGet(modelInterface,sourceId,numCards))
                    )
            {
                spdlog::get("console")->error("NotEmpty_OneCard_FaceUp: source:{}, size: {}, numCards:{}"
                        , sourceId
                        , numCards);
                return -1;
            }
            return 0;
        }
    };

    class AllCards_AllFaceUp : AllCards, AllFaceUp
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface &modelInterface, std::string const &sourceId, std::size_t const &numCards)
        {
            if (
                    (0 != AllCards::CheckBeforeGet(modelInterface, sourceId, numCards)) ||
                    (0 != AllFaceUp::CheckBeforeGet(modelInterface, sourceId, numCards))
                )
            {
                spdlog::get("console")->error("AllCards_AllFaceUp: source:{}, size: {}, numCards:{}"
							  , sourceId
							  , numCards);
                return -1;
            }
            return 0;
        }
    };

    class EnoughCards_AllFaceUp : EnoughCards, AllFaceUp
    {
    public:
        template<typename ModelInterface>
        std::int32_t CheckBeforeGet(ModelInterface& modelInterface, std::string const& sourceId, std::size_t const& numCards)
        {
            if (
                    (0 != EnoughCards::CheckBeforeGet(modelInterface, sourceId, numCards)) ||
                    (0 != AllFaceUp::CheckBeforeGet(modelInterface, sourceId, numCards))
                )
            {
        
                spdlog::get("console")->error("EnoughCards_AllFaceUp: source:{}, size: {}, numCards:{}"
							  , sourceId
							  , numCards);
                return -1;
            }
            return 0;
        }
    };
}

#endif //KLONDIKE_CONTROLLER_SOURCECHECKPOLICIES_HPP
