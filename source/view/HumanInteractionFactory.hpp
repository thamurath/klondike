#ifndef KLONDIKE_HUMANINTERACTIONFACTORY_HPP
#define KLONDIKE_HUMANINTERACTIONFACTORY_HPP

#include "PlayerInteraction.hpp"
#include "PlayerInteractionAbstractFactory.hpp"


namespace com::jlom::klondike::view
{
    class HumanInteractionFactory : public PlayerInteractionAbstractFactory
    {
    public:
        PlayerInteraction* GetPlayerInteraction(std::string const& id) override ;
    };
}

#endif //KLONDIKE_HUMANINTERACTIONFACTORY_HPP
