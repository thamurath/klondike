
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "english_model/Card.hpp"

#include "Foundation.hpp"
#include "CardHeap.hpp"

#include <iostream>

TEST_CASE("Foundation simple case") {

    namespace model = com::jlom::klondike::model;
    namespace specific_model = model::english_model;
    using SpecificCard = specific_model::Card;

    model::Foundation<SpecificCard , model::CardHeap> heartsFoundation;


    heartsFoundation.Push({model::CardTraits<SpecificCard>::FigureType::ACE, model::CardTraits<SpecificCard>::SuitType::DIAMONDS});
    heartsFoundation.Push({model::CardTraits<SpecificCard>::FigureType::TWO, model::CardTraits<SpecificCard>::SuitType::DIAMONDS});
    auto card = heartsFoundation.Get();
    REQUIRE(card.has_value());
    std::cout << (*card) << std::endl;
}
