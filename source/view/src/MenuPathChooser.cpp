#include "MenuPathChooser.hpp"

#include <spdlog/spdlog.h>

namespace com::jlom::klondike::view
{
    Command MenuPathChooser::Handle(MenuContext &menuContext)
    {
        spdlog::get("console")->trace("Entering MenuPathChooser::Handle option {}", menuContext.GetMenuOption());
        auto ite = table.find(menuContext.GetMenuOption());
        if (std::end(table) != ite)
        {
            spdlog::get("console")->trace("Entering MenuPathChooser::Handle option {} FOUND", menuContext.GetMenuOption());
            return DelegateHanlding(ite->second, menuContext);
        }

        spdlog::get("console")->error("Entering MenuPathChooser::Handle option {} NOT FOUND", menuContext.GetMenuOption());
    }

    void MenuPathChooser::AddOption(std::uint32_t optionNumber, std::unique_ptr<Menu> menuOption)
    {
        table.insert_or_assign(optionNumber, std::move(menuOption));
    }
}

