#include "MenuFillIn.hpp"

#include <spdlog/spdlog.h>

namespace com::jlom::klondike::view
{
    Command MenuFillIn::Handle(MenuContext &menuContext)
    {
        spdlog::get("console")->trace("Entering MenuFillIn::Handle");
        FillIn(menuContext);
        return DelegateHanlding(menuContext);
    }
}