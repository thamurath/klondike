#ifndef KLONDIKE_MENUFILLIN_HPP
#define KLONDIKE_MENUFILLIN_HPP

#include "Menu.hpp"

namespace com::jlom::klondike::view
{
    class MenuFillIn : public Menu
    {
    private:
        Command Handle(MenuContext &menuContext) override ;

        virtual void FillIn(MenuContext& menuContext) = 0;
    };
}

#endif //KLONDIKE_MENUFILLIN_HPP
