#ifndef KLONDIKE_ACTIONBUILDER_HPP
#define KLONDIKE_ACTIONBUILDER_HPP

#include "Action.hpp"

#include <optional>

namespace com::jlom::klondike
{
    class ActionBuilder final
    {
    public:
        ActionBuilder(void) = default;
        ~ActionBuilder() = default;

        ActionBuilder(ActionBuilder const& ) = delete;
        ActionBuilder& operator=(ActionBuilder const& ) = delete;

        ActionBuilder(ActionBuilder&& ) = delete;
        ActionBuilder& operator=(ActionBuilder&& ) = delete;

        ActionBuilder& SetSourceId(std::string sourceId);
        ActionBuilder& SetDestinationId( std::string destinationId);
        ActionBuilder& SetNumCards(std::size_t numCards);
        ActionBuilder& SetActionType(ACTION_TYPE type);

        Action Build(void) const;

    private:
        std::optional<std::string> sourceId;
        std::optional<std::string> destinationId;
        std::optional<std::size_t> numCards;
        std::optional<ACTION_TYPE> type;
    };
}
#endif //KLONDIKE_ACTIONBUILDER_HPP
