#ifndef KLONDIKE_CONTROLLER_UNITTEST_DUMMY_POLICIES_HPP
#define KLONDIKE_CONTROLLER_UNITTEST_DUMMY_POLICIES_HPP

namespace com::jlom::klondike::controller::unittest
{
    class AlwaysTrueSourcePolicy
    {
    public:
        template <typename ModelInterface>
        std::int32_t PreCheck(ModelInterface const& modelInterface, std::string const& sourceId)
        {
            return 0;
        }

        template <typename ModelInterface>
        std::int32_t CheckBeforePop(ModelInterface const& modelInterface, std::string const& sourceId)
        {
            return 0;
        }
    };

    class AlwaysTrueDestinationPolicy
    {
    public:
        template <typename ModelInterface>
        std::int32_t PreCheck(ModelInterface const& modelInterface, std::string const& destinationId)
        {
            return 0;
        }
        template <typename ModelInterface, typename Card, template <typename T, typename... Args> class Container>
        std::int32_t CheckBeforePush(ModelInterface const& modelInterface, std::string const& destinationId, Container<Card> const& cards)
        {
            return 0;
        }
    };

    class NoOpHandleOperationsPolicy
    {
    public:
        template <typename Card>
        Card handle(Card&& card)
        {
            return card;
        }
    };
}

#endif //KLONDIKE_CONTROLLER_UNITTEST_DUMMY_POLICIES_HPP
