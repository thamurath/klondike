#ifndef KLONDIKE_MENUCHOOSEPILE_HPP
#define KLONDIKE_MENUCHOOSEPILE_HPP

#include "MenuItem.hpp"

namespace com::jlom::klondike::view
{
    class MenuChoosePile: public MenuItem
    {
    private:

        MenuItem::Options GetOptions(void) override ;

    private:

    };

}

#endif //KLONDIKE_MENUCHOOSEPILE_HPP
