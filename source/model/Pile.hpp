#ifndef __KLONDIKE_MODEL_PILE_HPP__
#define __KLONDIKE_MODEL_PILE_HPP__

#include "Card.hpp"

#include "Transactional.hpp"

#include <vector>

namespace com::jlom::klondike::model
{

    template <typename Card>
    class PileState
    {
    public:
        PileState(PileState const& ) = delete;
        PileState& operator=(PileState const& ) = delete;

        PileState(PileState&& ) noexcept = default;
        PileState& operator=(PileState&& ) noexcept = default;

    private:
        template <typename T, template <typename C> class H>
        friend class Pile;

        PileState(std::uint8_t id, std::vector<Card>const& cards)
        : m_id(id)
        , m_cards(cards)
        {

        }
        std::uint8_t m_id;
        std::vector<Card> m_cards;
    };

    template<typename Card, template<typename C> class Heap>
    class Pile : public Transactional<Pile<Card,Heap>>
    {
    public:
        Pile(std::uint8_t ai_id, std::vector<Card>&& cards);
        virtual ~Pile(void) = default;

        // avoid copy
        Pile(Pile const&) = delete;
        Pile& operator=(Pile const&) = delete;
        // allow movement
        Pile(Pile&&) noexcept = default;
        Pile& operator=(Pile&&) noexcept = default;

        auto IsEmpty(void) const;
        auto Size(void) const;
        auto Get(void) const;
        auto GetCards(std::size_t const& numCards) const;

        void Pop(void);
        auto Push(Card&& card);


        template <typename StatusEncoder>
        auto EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const;

        /// Transactional Interface
        using state_type = PileState<Card>;
    protected:
    private:
        std::uint8_t m_id;
        Heap<Card> heap;

        /// Card defined types
        using FigureType = typename Card::FigureType;
        using SuitType = typename Card::SuitType;

        /// Transactional Interface
        friend class Transactional<Pile>;
        state_type begin(void) const;
        void commit(void);
        void rollback(state_type const& state);
    };
}//namespace com::jlom::klondike::model

#include "src/Pile_impl.hpp"
#endif //__KLONDIKE_MODEL_PILE_HPP__