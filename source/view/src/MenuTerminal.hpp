#ifndef KLONDIKE_MENUTERMINAL_HPP
#define KLONDIKE_MENUTERMINAL_HPP

#include "Menu.hpp"

namespace com::jlom::klondike::view
{
    class MenuTerminal : public Menu
    {
    public:
        Command Handle(MenuContext &menuContext) override ;
    };
}

#endif //KLONDIKE_MENUTERMINAL_HPP
