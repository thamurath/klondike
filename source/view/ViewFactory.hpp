#ifndef KLONDIKE_VIEWFACTORY_HPP
#define KLONDIKE_VIEWFACTORY_HPP

#include "ViewTechAbstractFactory.hpp"

#include <string>
#include <memory>

namespace com::jlom::klondike::view
{
    class DeckView;
    class PileView;
    class WasteView;
    class FoundationView;

    class ViewFactory final
    {
    public:
        ///Takes Ownership of the pointer!!
        /// @jlom TODO should use some way to reflect the ownership with smart pointers and/or move semantics
        explicit ViewFactory(ViewTechAbstractFactory* viewTechAbstractFactory);
        ~ViewFactory() = default;

        DeckView* GetDeck(std::string const& id) const ;
        PileView* GetPile(std::string const& id) const ;
        WasteView* GetWaste(std::string const& id) const ;
        FoundationView* GetFoundation(std::string const& id) const ;

    private:
        ViewTechAbstractFactory*  viewTechAbstractFactory;

        template <typename T>
        T* GenerateView(std::string const& id) const;
    };
}

#endif //KLONDIKE_VIEWFACTORY_HPP
