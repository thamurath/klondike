#ifndef KLONDIKE_MODEL_ENGLISHMODEL_CARD_HPP__
#define KLONDIKE_MODEL_ENGLISHMODEL_CARD_HPP__

#include "model/Card.hpp"

#include <better_enums.hpp>
namespace com::jlom::klondike::model
{
    namespace english_model
    {
        namespace model = com::jlom::klondike::model;

        BETTER_ENUM(FIGURE, std::uint8_t,
                    ACE = 1,
                    TWO,
                    THREE,
                    FOUR,
                    FIVE,
                    SIX,
                    SEVEN,
                    EIGHT,
                    NINE,
                    TEN,
                    JACK,
                    QUEEN,
                    KING
        );

        BETTER_ENUM(COLOR, std::uint8_t,
                    RED,
                    BLACK
        );

        BETTER_ENUM(SUIT, std::uint8_t,
                    DIAMONDS,
                    CLUBS,
                    HEARTS,
                    SPADES
        );

        class Card;
    }

    template<>
    struct CardTraits<english_model::Card>
    {
        using SuitType = english_model::SUIT;
        using FigureType = english_model::FIGURE;
        using ColorType = english_model::COLOR;
    };



    namespace english_model
    {
        class Card : public model::Card<Card>
        {
        public:
            using FigureType = CardTraits<Card>::FigureType;
            using SuitType = CardTraits<Card>::SuitType;
            using ColorType = CardTraits<Card>::ColorType;

            Card(FigureType const& figure, SuitType const& suit);
            virtual ~Card(void);

            //allow movement
            Card(Card&&) = default;
            Card& operator=(Card &&) = default;
            //allow copy
            Card(Card const&) noexcept = default;
            Card& operator=(Card const&) noexcept = default;
        protected:
        private:
            SuitType m_suit;
            FigureType m_figure;

            friend class model::Card<Card>;
            FigureType getFigure(void) const;
            SuitType getSuit(void) const;
            ColorType getColor(void) const;
            
            // helper function
            static ColorType color(SuitType const& suit);
        };
        std::ostream& operator<< (std::ostream& os, Card const& card);

    }

}
#endif //KLONDIKE_MODEL_ENGLISHMODEL_CARD_HPP__
