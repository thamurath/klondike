#ifndef KLONDIKE_JSONTRANSCODER_HPP
#define KLONDIKE_JSONTRANSCODER_HPP

#include <json.hpp>

#include <string>
namespace com::jlom::klondike
{
    class JsonStatusEncoder final
    {
    public:
        template<typename Cards>
        auto Encode(std::string const &id, Cards const &cards)
        {
            using json = nlohmann::json;

            json j;
            j[id] = json({});

            json data_array = json::array();
            for (auto const &card : cards) {
                data_array.push_back(card);
            }

            j[id] = data_array;

            return j;
        }




        auto Encode(void) const
        {
            return nlohmann::json::value_type::object({});
        }

        auto Append(nlohmann::json const &old, nlohmann::json const &toAppend)
        {
            auto j(old);
            extendJson(j, toAppend);
            return j;
        }

    private:
        void extendJson(nlohmann::json &j1, const nlohmann::json &j2)
        {
            for (const auto &j : nlohmann::json::iterator_wrapper(j2))
            {
                j1[j.key()] = j.value();
            }
        }

        nlohmann::json merge(const nlohmann::json &a, const nlohmann::json &b)
        {
            using json = nlohmann::json;

            json result = a.flatten();
            json tmp = b.flatten();

            for (json::iterator it = tmp.begin(); it != tmp.end(); ++it)
            {
                result[it.key()] = it.value();
            }

            return result.unflatten();
        }
    };
}
#endif //KLONDIKE_JSONTRANSCODER_HPP
