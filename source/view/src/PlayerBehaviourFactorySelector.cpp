#include "PlayerBehaviourFactorySelector.hpp"

#include "HumanInteractionFactory.hpp"
#include "AIInteractionFactory.hpp"

#include <cassert>

namespace com::jlom::klondike::view
{
    PlayerInteractionAbstractFactory* PlayerBehaviourFactorySelector::GetPlayerInteractionFactory(
            Configuration const &configuration) const
    {
        ///@jlom TODO Choose using configuration witch factory to return
        volatile int numPlayers = 1;
        if ( 1 == numPlayers)
        {
            return new HumanInteractionFactory();
        }
        else if ( 0 == numPlayers)
        {
            return new AIInteractionFactory();
        }

        assert(false);
    }
}
