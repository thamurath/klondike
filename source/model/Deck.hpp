#ifndef __KLONDIKE_MODEL_DECK_HPP__
#define __KLONDIKE_MODEL_DECK_HPP__

#include "Transactional.hpp"
#include "Card.hpp"


#include <cstdlib> // for std::size_t
#include <vector>
namespace com::jlom::klondike::model
{

    template <typename Card>
    class DeckState
    {
    public:
        ~DeckState() = default;

        DeckState(DeckState const&) = delete;
        DeckState& operator=(DeckState const&) = delete;

        DeckState(DeckState&&) noexcept = default;
        DeckState& operator=(DeckState&&) noexcept = default;

    private:
        template <typename T, template <typename C> class H>
        friend class Deck;

        DeckState(std::vector<Card> cards, std::size_t const& initialSize)
                : m_cards(cards)
                , m_initialSize(initialSize)
        {

        }

        std::vector<Card> m_cards;
        std::size_t m_initialSize;
    };


    template <typename Card, template<typename C> class Heap>
    class Deck : public Transactional<Deck<Card,Heap>>
    {
    public:
        explicit Deck(std::vector<Card>&& cards);
        virtual ~Deck(void) = default;

        void Shuffle(void);

        Deck(Deck const&) = delete;
        Deck&operator=(Deck const&) = delete;

        Deck(Deck&&) noexcept = default;
        Deck& operator=(Deck&&) noexcept = default;

        auto IsEmpty(void) const;
        auto Size(void) const;
        auto Get(void) const;
        auto GetCards(std::size_t const& numCards) const;

        void Pop(void);
        auto Push(Card&& card);

        template <typename StatusEncoder>
        auto EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const;

        /// Transactional Interface
        using state_type = DeckState<Card>;
    protected:
    private:
        Heap<Card> heap;
        std::size_t m_initialSize;

        /// Transactional Interface
        friend class Transactional<Deck>;
        state_type begin(void) const;
        void commit(void);
        void rollback(state_type const& state);

    };

}//namespace com::jlom::klondike::model

#include "src/Deck_impl.hpp"



#endif //__KLONDIKE_MODEL_DECK_HPP__
