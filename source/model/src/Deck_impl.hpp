//
// Created by eojojos on 10/9/17.
//

#ifndef KLONDIKE_MODEL_DECK_IMPL_HPP
#define KLONDIKE_MODEL_DECK_IMPL_HPP

#ifndef __KLONDIKE_MODEL_DECK_HPP__
#error "Should not include this file"
#endif


#include <vector>
#include <algorithm>
#include <random>
#include <cassert>

namespace com::jlom::klondike::model
{

    template<typename Card, template <typename C> class Heap>
    Deck<Card,Heap>::Deck(std::vector<Card> &&cards)
            : heap(std::move(cards)), m_initialSize(0) // no more than the initial
    {
        assert(std::all_of(std::begin(cards), std::end(cards), [](Card const &c) { return !c.IsFaceUp(); }) );
        m_initialSize = heap.Size();
    }

    template <typename Card, template <typename C> class Heap>
    void Deck<Card, Heap>::Shuffle(void)
    {
        std::random_device rd;
        std::default_random_engine random_engine(rd());
        auto cards = heap.GetAllCards();
        std::shuffle(std::begin(cards), std::end(cards), random_engine);
        heap.Reset(std::move(cards));
    }


    template<typename Card, template <typename C> class Heap>
    auto Deck<Card, Heap>::IsEmpty(void) const
    {
        return heap.IsEmpty();
    }

    template <typename Card, template <typename C> class Heap>
    auto Deck<Card, Heap>::Size(void) const
    {
        return heap.Size();
    }

    template <typename Card, template <typename C> class Heap>
    auto Deck<Card, Heap>::Get(void) const
    {
        assert(!heap.IsEmpty());
        return heap.Get();
    }

    template <typename Card, template <typename C> class Heap>
    auto Deck<Card, Heap>::GetCards(std::size_t const& numCards) const
    {
        return heap.GetCards(numCards);
    };

    template <typename Card, template <typename C> class Heap>
    void Deck<Card, Heap>::Pop(void)
    {
        assert(!heap.IsEmpty());
        heap.Pop();
    }

    template <typename Card, template <typename C> class Heap>
    auto Deck<Card, Heap>::Push(Card &&card)
    {
        assert(heap.Size() < m_initialSize);
        return heap.Push(std::move(card));
    }

    template <typename Card, template <typename C> class Heap>
    template <typename StatusEncoder>
    auto Deck<Card, Heap>::EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const
    {
        assert(nullptr != statusEncoder);
        return heap.EncodeStatus(id, statusEncoder);
    }

    template<typename Card, template <typename C> class Heap>
    typename Deck<Card, Heap>::state_type Deck<Card, Heap>::begin(void) const
    {
        return DeckState(heap.GetAllCards(), m_initialSize);
    }

    template <typename Card, template <typename C> class Heap>
    void Deck<Card, Heap>::commit(void)
    {
        //do nothing
    }

    template <typename Card, template <typename C> class Heap>
    void Deck<Card, Heap>::rollback(state_type const &state)
    {
        auto cards = state.m_cards;
        heap.Reset(std::move(cards));
        m_initialSize = state.m_initialSize;
    }
}
#endif //KLONDIKE_MODEL_DECK_IMPL_HPP
