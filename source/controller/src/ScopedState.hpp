#ifndef KLONDIKE_CONTROLLER_SCOPEDSTATE_HPP
#define KLONDIKE_CONTROLLER_SCOPEDSTATE_HPP


#include <utility>

namespace com::jlom::klondike::controller
{
    template <typename Controller, typename Id, typename State>
    class ScopedState final
    {
    public:
        ScopedState(Controller& controller, Id const& itemId, State&& state)
                : controller (controller)
                , id(itemId)
                , state(std::move(state))
                , deactivated(false)
        {
        }

        void Deactivate(void)
        {
            deactivated = true;
        }

        ~ScopedState(void)
        {
            if (deactivated)
            {
                controller.Commit(id);
            }
            else
            {
                controller.Rollback(id, std::move(state));
            }
        }
    private:
        Controller& controller;
        Id id;
        State state;
        bool deactivated;
    };
}
#endif //KLONDIKE_CONTROLLER_SCOPEDSTATE_HPP
