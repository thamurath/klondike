#ifndef KLONDIKE_BETTER_ENUMS_HPP
#define KLONDIKE_BETTER_ENUMS_HPP

#include <enum.h>

#include <utility>

template <typename Enum>
constexpr  std::int64_t index( typename Enum::_value_iterator const ite, const std::uint64_t val,  typename Enum::_enumerated const word)
{
    if( Enum::_size_constant == val) return -1;
    if ( *ite == +word) return val;
    return index<Enum>((ite+1), val+1, word);
}

template<typename Enum>
constexpr std::int64_t Index(typename Enum::_enumerated&& word)
{
    return index<Enum>(Enum::_values().begin(),0,word);

}

template<typename Enum>
constexpr bool isPrevious(typename Enum::_enumerated&& prev, typename Enum::_enumerated&& current)
{
    return Index<Enum>(std::forward<typename Enum::_enumerated>(prev))+1 == Index<Enum>(std::forward<typename Enum::_enumerated>(current));
}

template<typename Enum>
constexpr bool isNext(typename Enum::_enumerated&& next, typename Enum::_enumerated&& current)
{
    return Index<Enum>(std::forward<typename Enum::_enumerated>(next)) == 1+Index<Enum>(std::forward<typename Enum::_enumerated>(current));
}

template<typename Enum>
constexpr  Enum First(void)
{
    return *(Enum::_values().begin());
}

template<typename Enum>
constexpr  Enum Last(void)
{
    return Enum::_values()[Enum::_size()-1];
}

template <typename Enum>
std::int64_t Index(typename Enum::_enumerated const& element)
{
    std::int64_t index(0);
    for(auto ite = Enum::_values().begin(); (Enum::_values().end() != ite) && ((*ite) != (+element)); ++ite, ++index);
    return index;
}

template <typename Enum>
bool isPrevious(typename Enum::_enumerated const& prev, typename Enum::_enumerated const& current)
{
    return Index<Enum>(prev)+1 == Index<Enum>(current);
}

template <typename Enum>
bool isNext(typename Enum::_enumerated const& next, typename Enum::_enumerated const& current)
{
    return Index<Enum>(next) == 1+Index<Enum>(current);
}



#endif //KLONDIKE_BETTER_ENUMS_HPP
