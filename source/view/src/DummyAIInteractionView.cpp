#include "DummyAIInteractionView.hpp"
#include "MenuMaker.hpp"

#include <sstream>
#include <cassert>
#include <random>
namespace com::jlom::klondike::view
{

    DummyAIInteractionView::DummyAIInteractionView(std::string const &id)
            : PlayerInteraction(id)
    {

    }

    Command DummyAIInteractionView::GetCommand(void)
    {
        MenuMaker maker;
        auto menu = maker.Build();
        return menu->GetCommand(*this);
    }



    std::uint32_t DummyAIInteractionView::GetNumber(std::uint32_t const& min, std::uint32_t const& max)
    {
        std::random_device randomDevice;
        std::default_random_engine randomEngine(randomDevice());

        std::uniform_int_distribution<std::uint32_t> uniformIntDistribution(min, max);
        return uniformIntDistribution(randomEngine);
    }


}

