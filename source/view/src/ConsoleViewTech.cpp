

#include "ConsoleViewTech.hpp"

#include <iostream>
#include <string>
#include <sstream>
namespace com::jlom::klondike::view
{
    std::string ConsoleViewTech::GetInput(void)
    {
        std::string input;
        getline(std::cin, input);
        return input;
    }


    void ConsoleViewTech::ShowText(std::string const& text)
    {
        std::cout << text << std::endl;
    }

    std::uint32_t ConsoleViewTech::GetNumber(const uint32_t &min, const uint32_t &max)
    {
        std::string input;
        std::int64_t ret;

        do
        {
            getline(std::cin, input);

            std::stringstream ss(input);
            if(ss >> ret)
            {
                if ((min <= ret) && ( max >= ret) )
                {
                    return static_cast<std::uint32_t>(ret);
                }
            }
            ShowText("Invalid number, try Again");
        }
        while(true);

    }
}
