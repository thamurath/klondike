#include "StatusDecoderFactory.hpp"

#include <common/JsonStatusDecoder.hpp>
namespace com::jlom::klondike::view
{
    StatusDecoder* StatusDecoderFactory::GetStatusDecoder(Configuration const &configuration) const
    {
        ///@jlom TODO Use configuration to choose the decoder.
        return new JsonStatusDecoder;
    }
}