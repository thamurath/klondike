#include "InteractionTechnologySelector.hpp"

#include "ViewTechAbstractFactory.hpp"

#include "ConsoleViewTechFactory.hpp"
#include "GUIViewTechFactory.hpp"

namespace com::jlom::klondike::view
{
    ViewTechAbstractFactory* InteractionTechnologySelector::GetViewTechnologyFactory(const std::string &configuration)
    {
        ///@jlom TODO Elegir segun la configuracion la tecnologia adecuada
        return new ConsoleViewTechFactory();
        return new GUIViewTechFactory();
    }
}


