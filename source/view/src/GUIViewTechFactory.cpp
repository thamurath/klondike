#include "GUIViewTechFactory.hpp"

#include "GUIViewTech.hpp"

namespace com::jlom::klondike::view
{
    ViewTechImplementation* GUIViewTechFactory::GetViewTechnology(void)
    {
        return new GUIViewTech;
    }
}

