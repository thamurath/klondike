#ifndef __KLONDIKE_MODEL_FOUNDATION_HPP__
#define __KLONDIKE_MODEL_FOUNDATION_HPP__

#include "Card.hpp"
#include "Transactional.hpp"
#include <vector>

namespace com::jlom::klondike::model
{
    template <typename Card>
    class FoundationState
    {
    public:
        ~FoundationState() = default;
        FoundationState(FoundationState const&) = delete;
        FoundationState& operator=(FoundationState const&) = delete;

        FoundationState(FoundationState&&) noexcept  = default;
        FoundationState& operator=(FoundationState&&) noexcept = default;
    private:
        template <typename T, template <typename C> class H>
        friend class Foundation;

        explicit FoundationState(std::vector<Card>const& cards)
                : cards(cards)
        {

        }

        std::vector<Card> cards;
    };

    template <typename Card, template <typename C> class Heap>
    class Foundation : public Transactional<Foundation<Card, Heap>>
    {
        using SuitType = typename Card::SuitType;
        using FigureType = typename Card::FigureType;
    public:
        Foundation(void) = default;
        virtual ~Foundation(void) = default;

        //avoid copy
        Foundation(Foundation const&) = delete;
        Foundation& operator=(Foundation const&) = delete;

        //allow movement
        Foundation(Foundation&&) noexcept = default;
        Foundation& operator=(Foundation&&) noexcept = default;


        auto IsEmpty(void) const;
        auto Size(void) const;
        auto Get(void) const;
        auto GetCards(std::size_t const& numCards) const;

        void Pop(void);
        auto Push(Card&& card);

        template <typename StatusEncoder>
        auto EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const;



        /// Transactional Interface
        using state_type = FoundationState<Card>;
    protected:
    private:
        Heap<Card> heap;

        /// Transactional Interface
        friend class Transactional<Foundation>;
        state_type begin(void) const;
        void commit(void);
        void rollback(state_type const& state);

    };
}//namespace com::jlom::klondike::model

#include "src/Foundation_impl.hpp"

#endif //__KLONDIKE_MODEL_FOUNDATION_HPP__