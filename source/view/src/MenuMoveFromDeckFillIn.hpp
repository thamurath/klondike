#ifndef KLONDIKE_MENUMOVEFROMDECKFILLIN_HPP
#define KLONDIKE_MENUMOVEFROMDECKFILLIN_HPP

#include "MenuFillIn.hpp"

#include <spdlog/spdlog.h>

namespace com::jlom::klondike::view
{
    class MenuMoveFromDeckFillIn : public MenuFillIn
    {
        void FillIn(MenuContext& menuContext) override
        {
            spdlog::get("console")->debug("Entering MenuMoveFromDeckFillIn::FillIn");
            ///@jlom TODO need configuration info to know the deck and waste ids
            menuContext.Builder().SetSource("deck");
            menuContext.Builder().SetSourceType(ElementType::DECK);
            menuContext.Builder().SetDestination("waste");
            menuContext.Builder().SetDestinationType(ElementType::WASTE);
        }
    };
}

#endif //KLONDIKE_MENUMOVEFROMDECKFILLIN_HPP
