#ifndef KLONDIKE_KLONDIKEAPP_HPP
#define KLONDIKE_KLONDIKEAPP_HPP

namespace com::jlom::klondike
{
    class KlondikeApp
    {
    public:
        int Run(int argc, char** argv);
    };
}

#endif //KLONDIKE_KLONDIKEAPP_HPP
