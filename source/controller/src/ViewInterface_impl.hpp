#ifndef KLONDIKE_CONTROLLER_VIEWINTERFACE_IMPL_HPP
#define KLONDIKE_CONTROLLER_VIEWINTERFACE_IMPL_HPP


#ifndef KLONDIKE_CONTROLLER_VIEWINTERFACE_HPP
#error "Should not include this file "
#endif

#include <model/exceptions.hpp>

#include <common/Action.hpp>
#include <common/Command.hpp>
#include <common/ActionBuilder.hpp>

#include <spdlog/spdlog.h>

#include <cassert>
#include <iterator>

namespace com::jlom::klondike::controller
{
    template<typename ActionHandlerController>
    ViewInterface<ActionHandlerController>::ViewInterface(ActionHandlerController& actionHandlerController)
            : actionHandlerController(actionHandlerController)
    {
        translator.insert({std::make_tuple(ElementType::PILE, ElementType::PILE), ACTION_TYPE::PILE_TO_PILE});
        translator.insert({std::make_tuple(ElementType::DECK, ElementType::WASTE), ACTION_TYPE::DECK_TO_WASTE});
        translator.insert({std::make_tuple(ElementType::WASTE, ElementType::PILE), ACTION_TYPE::WASTE_TO_PILE});
        translator.insert({std::make_tuple(ElementType::WASTE, ElementType::FOUNDATION), ACTION_TYPE::WASTE_TO_FOUNDATION});
        translator.insert({std::make_tuple(ElementType::PILE, ElementType::FOUNDATION), ACTION_TYPE::PILE_TO_FOUNDATION});
    }

    template<typename ActionHandlerController>
    std::int32_t ViewInterface<ActionHandlerController>::ProcessCommand(Command const& command)
    {
        ActionBuilder builder;
        auto type = command.GetCommandType();
        if ( +(CommandType::RESIGN) == type)
        {
            builder.SetActionType(ACTION_TYPE::RESIGN);
        }
        else if ( +(CommandType::REDEAL) == type)
        {
            builder.SetActionType(ACTION_TYPE::REDEAL);
        }
        else if ( +(CommandType::SHOW_STATUS) == type)
        {
            builder.SetActionType(ACTION_TYPE::REQUEST_GAME_STATUS);
        }
        else if ( +(CommandType::MOVE) == type)
        {
            auto ite = translator.find(std::make_tuple(command.GetSourceType(), command.GetDestinationType()));
            assert(std::end(translator) != ite);

            ACTION_TYPE actionType = ite->second;
            /// There is an exceptional case: When movement is pile-to-pile but source and destination are
            /// the same pile and it is only one card ... that's a Turn UP card action
            if (
                    ( +(ACTION_TYPE::PILE_TO_PILE) == actionType) &&
                    (command.GetSourceId() == command.GetDestinationId()) &&
                    (1 == command.GetNumCards())
                )
            {
                actionType = ACTION_TYPE::PILE_TURNUP_CARD;
            }

            builder.SetActionType(actionType)
                    .SetSourceId(command.GetSourceId())
                    .SetDestinationId(command.GetDestinationId())
                    .SetNumCards(command.GetNumCards());
        }
        else
        {
            assert(false);
        }

        try
        {
            return actionHandlerController(builder.Build());
        }
        catch (model::exceptions::ElementNotFound& e)
        {
            spdlog::get("console")->critical(e.what());
        }
        catch (model::exceptions::NonTransactionalItem& e)
        {
            spdlog::get("console")->critical(e.what());
        }
        catch (std::exception& e)
        {
            spdlog::get("console")->critical(e.what());
        }

        return 0;
    }
}
#endif //KLONDIKE_VIEWINTERFACE_IMPL_HPP
