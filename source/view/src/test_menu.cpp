#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "HumanInteractionView.hpp"
#include "DummyAIInteractionView.hpp"
#include "MenuMaker.hpp"
#include "ConsoleViewTech.hpp"

#include <spdlog/spdlog.h>

#include <iostream>


static void ConfigureLogger(void)
{
    static bool alreadyDone (false);

    if ( alreadyDone) return;

    auto console = spdlog::stdout_color_mt("console");
    console->set_level(spdlog::level::debug);
    console->flush_on(spdlog::level::debug);
    alreadyDone = true;
}

TEST_CASE("test menu")
{
    namespace common = com::jlom::klondike;
    namespace view = common::view;


    ConfigureLogger();


    view::MenuMaker menuMaker;
    std::cout << "building" << std::endl;
    auto menu = menuMaker.Build();
    std::cout << "built" << std::endl;

    SECTION("Human")
    {
        spdlog::get("console")->info("HUMAN");
        view::HumanInteractionView humanInteractionView("player1");
        humanInteractionView.SetViewTech(new view::ConsoleViewTech);
        std::cout << "getting command" << std::endl;
        auto command = menu->GetCommand(humanInteractionView);

        std::cout << command << std::endl;
    }

    SECTION("Robot")
    {
        spdlog::get("console")->info("ROBOT");
        view::DummyAIInteractionView interactionView("robot1");
        interactionView.SetViewTech(new view::ConsoleViewTech);
        std::cout << "getting command" << std::endl;
        auto command = menu->GetCommand(interactionView);

        std::cout << command << std::endl;
    }

}
