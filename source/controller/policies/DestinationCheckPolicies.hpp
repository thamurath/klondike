#ifndef KLONDIKE_CONTROLLER_DESTINATiONCHECKPOLICIES_HPP
#define KLONDIKE_CONTROLLER_DESTINATiONCHECKPOLICIES_HPP

#include "better_enums.hpp"
#include "type_traits.hpp"
#include "spdlog/spdlog.h"

#include <common/Action.hpp>

#include <cstdlib>

namespace com::jlom::klondike::controller::policies::destinationcheck
{
    namespace  internal
    {
        class LastFigure
        {
        public:
            template <typename ModelInterface, typename Card>
            std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Card const& card)
            {
                // only the highest figure is allowed
                if ( Last<typename ModelInterface::FigureType>() != card.GetFigure() )
                {
                    spdlog::get("console")->error("LastFigure: Card figure is not the last one: {}", card.GetFigure()._to_string());
                    return -1;
                }
                return 0;
            }
        };

        class FirstFigure
        {
        public:
            template <typename ModelInterface, typename Card>
            std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Card const& card)
            {
                // only the first figure is allowed
                if ( First<typename ModelInterface::FigureType>() != card.GetFigure() )
                {
                    spdlog::get("console")->error("FirstFigure card: {}", card.GetFigure()._to_string());
                    return -1;
                }
                return 0;
            }
        };

        class NextFigure
        {
        public:
            template <typename ModelInterface, typename Card>
            std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Card const& card)
            {
                // only next figure is allowed
                if (! isNext<typename ModelInterface::FigureType>(card.GetFigure(), modelInterface.Get(destinationId)->GetFigure()) )
                {
                    spdlog::get("console")->error("NextFigure card: {}, current: {}"
                                                              , card.GetFigure()._to_string()
                                                              , modelInterface.Get(destinationId)->GetFigure()._to_string()
                    );
                    return -1;
                }
                return 0;
            }
        };

        class PrevFigure
        {
        public:
            template <typename ModelInterface, typename Card>
            std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Card const& card)
            {
                spdlog::get("console")->debug("PrevFigure card: {}, current: {}"
                        , card.GetFigure()._to_string()
                        , modelInterface.Get(destinationId)->GetFigure()._to_string());

                // only prev figure is allowed
                if (! isPrevious<typename ModelInterface::FigureType>(card.GetFigure(), modelInterface.Get(destinationId)->GetFigure()) )
                {
                    spdlog::get("console")->error("PrevFigure card: {}, current: {}"
                            , card.GetFigure()._to_string()
                            , modelInterface.Get(destinationId)->GetFigure()._to_string()
                    );
                    return -1;
                }
                return 0;
            }
        };

        class SwitchColor
        {
        public:
            template <typename ModelInterface, typename Card>
            std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Card const& card)
            {
                // and color must switch
                if ( modelInterface.Get(destinationId)->GetColor() == card.GetColor() )
                {
                    spdlog::get("console")->error("SwitchColor card: {}, current: {}"
                                                               , card.GetColor()._to_string()
                                                               , modelInterface.Get(destinationId)->GetColor()._to_string()
                    );
                    return -1;
                }
                return 0;
            }
        };

        class SameSuit
        {
        public:
            template <typename ModelInterface, typename Card>
            std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Card const& card)
            {
                if ( modelInterface.Get(destinationId)->GetSuit() != card.GetSuit() )
                {
                    spdlog::get("console")->error("SameSuit card: {}, current: {}"
                            , card.GetSuit()._to_string()
                            , modelInterface.Get(destinationId)->GetSuit()._to_string()
                    );
                    return -1;
                }
                return 0;
            }
        };

    } // namespace internal

    class NotEmpty
    {
    public:
        template <typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            spdlog::get("console")->debug("NotEmpty destinationId: {}", destinationId);
            if ( modelInterface.IsEmpty(destinationId))
            {
                spdlog::get("console")->error("NotEmpty destinationId: {}", destinationId);
                return -1;
            }
            return 0;
        }
    };

    class OneCard
    {
    public:
        template <typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            if ( 1 != std::size(cards))
            {
                spdlog::get("console")->error("OneCard size: {}", std::size(cards));
                return -1;
            }
            return 0;
        }
    };

    class AllFaceUp
    {
    public:
        template <typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            static_assert(has_const_iterator<Container>::value, "Must have a const_iterator typedef: ");
            static_assert(has_begin_end<Container>::value, "Must have begin and end member functions");

            if (!std::all_of(std::begin(cards), std::end(cards), [](auto& card){ return  card.IsFaceUp();}) )
            {
                spdlog::get("console")->error("AllFaceUp destinationId: {}", destinationId);
                return -1;
            }
            return 0;
        }
    };

    class AllFaceDown
    {
    public:
        template <typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            static_assert(has_const_iterator<Container>::value, "Must have a const_iterator typedef: ");
            static_assert(has_begin_end<Container>::value, "Must have begin and end member functions");

            if (!std::all_of(std::begin(cards), std::end(cards), [](auto& card){ return  !card.IsFaceUp();}) )
            {
                spdlog::get("console")->error("AllFaceDown destinationId: {}", destinationId);
                return -1;
            }
            return 0;
        }
    };


    class LastFaceUp
    {
    public:
        template <typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            spdlog::get("console")->debug("LastFaceUp destinationId:{}", destinationId);
            if ( false == modelInterface.Get(destinationId)->IsFaceUp())
            {
                spdlog::get("console")->error("LastFaceUp destinationId:{}", destinationId);
                return -1;
            }
            return 0;
        }
    };

    class NotEmpty_LastFaceUp : NotEmpty
    {
    public:
        template<typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface &modelInterface, std::string const &destinationId, Container const &cards)
        {
            spdlog::get("console")->debug("NotEmpty_LastFaceUp destinationId:{}", destinationId);
            if (
                    (0 != NotEmpty::CheckBeforePush(modelInterface, destinationId, cards)) ||
                    ( true == modelInterface.Get(destinationId)->IsFaceUp())
                )
            {
                spdlog::get("console")->error("NotEmpty_LastFaceUp destinationId:{}", destinationId);
                return -1;
            }
            return 0;
        }
    };

    class OneCard_FaceUp : OneCard, AllFaceUp
    {
    public:
        template<typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface &modelInterface, std::string const &destinationId, Container const &cards)
        {
            spdlog::get("console")->debug("OneCard_FaceUp destinationId:{} size: {}", destinationId, std::size(cards));
            if (
                    (0 != OneCard::CheckBeforePush(modelInterface, destinationId, cards)) ||
                    (0 != AllFaceUp::CheckBeforePush(modelInterface, destinationId, cards))
                )
            {
                spdlog::get("console")->error("OneCard_FaceUp destinationId:{} size: {}", destinationId, std::size(cards));
                return -1;
            }
            return 0;
        }
    };

    class PileTurnUp : OneCard_FaceUp, NotEmpty_LastFaceUp
    {
    public:
        template<typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            spdlog::get("console")->debug("PileTurnUp: destinationId:{} size: {}", destinationId, std::size(cards));
            if (
                    (0 != OneCard_FaceUp::CheckBeforePush(modelInterface, destinationId, cards) ) ||
                    (0 !=  NotEmpty_LastFaceUp::CheckBeforePush(modelInterface, destinationId, cards) )
                )
            {
                spdlog::get("console")->error("PileTurnUp: destinationId:{} size: {}", destinationId, std::size(cards));
                return -1;
            }
            return 0;

        }
    };

    class ToPile : OneCard_FaceUp, NotEmpty , LastFaceUp, internal::LastFigure, internal::PrevFigure, internal::SwitchColor
    {
    public:
        template<typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            static_assert(has_begin<Container>::value, "Must have begin member functions");

            spdlog::get("console")->debug("ToPile: destinationId:{} size: {}", destinationId, std::size(cards));

            if (0 != OneCard_FaceUp::CheckBeforePush(modelInterface, destinationId, cards) )
            {
                spdlog::get("console")->error("ToPile: destinationId:{} size: {}", destinationId, std::size(cards));
                return -1;
            }

            auto card = *(std::begin(cards));
            if ( 0 == NotEmpty::CheckBeforePush(modelInterface, destinationId, cards))
            {//not empty
                //last card should be face up, only previous figure allowed and color must switch
                if (
                        ( 0 != LastFaceUp::CheckBeforePush(modelInterface, destinationId, cards)) ||
                        ( 0 != internal::PrevFigure::CheckBeforePush(modelInterface, destinationId, card)) ||
                        ( 0 != internal::SwitchColor::CheckBeforePush(modelInterface, destinationId, card))
                    )
                {
                    spdlog::get("console")->error("ToPile: destinationId:{} size: {}", destinationId, std::size(cards));
                    return -1;
                }
            }
            else
            {//empty
                if ( 0 != internal::LastFigure::CheckBeforePush(modelInterface,destinationId,card))
                {
                    spdlog::get("console")->error("ToPile: destinationId:{} size: {}", destinationId, std::size(cards));
                    return -1;
                }
            }
            return 0;
        }
    };

    class ToFoundation : OneCard_FaceUp, NotEmpty, internal::NextFigure, internal::SameSuit, internal::FirstFigure
    {
    public:
        template<typename ModelInterface, class Container>
        std::int32_t CheckBeforePush(ModelInterface& modelInterface, std::string const& destinationId, Container const& cards)
        {
            static_assert(has_begin<Container>::value, "Must have begin member functions");

            if ( 0 != OneCard_FaceUp::CheckBeforePush(modelInterface, destinationId, cards))
            {
                spdlog::get("console")->error("ToFoundation: destinationId:{} size: {}", destinationId, std::size(cards));
                return -1;
            }

            auto card = *(std::begin(cards));

            if ( 0 == NotEmpty::CheckBeforePush(modelInterface, destinationId, cards))
            {// not empty
                if (
                        ( 0 != internal::SameSuit::CheckBeforePush(modelInterface, destinationId, card)) ||
                        ( 0 != internal::NextFigure::CheckBeforePush(modelInterface, destinationId, card))
                    )
                {
                    spdlog::get("console")->error("ToFoundation: destinationId:{} size: {}", destinationId, std::size(cards));
                    return -1;
                }
            }
            else
            {// empty
                if ( 0 != internal::FirstFigure::CheckBeforePush(modelInterface, destinationId, card))
                {
                    spdlog::get("console")->error("ToFoundation: destinationId:{} size: {}", destinationId, std::size(cards));
                    return -1;
                }
            }

            return 0;
        }
    };
}

#endif// KLONDIKE_CONTROLLER_DESTINATiONCHECKPOLICIES_HPP