#include "HumanInteractionView.hpp"

#include "MenuMaker.hpp"
#include <cassert>

namespace com::jlom::klondike::view
{
    HumanInteractionView::HumanInteractionView(std::string const &id)
            : PlayerInteraction(id)
    {

    }
    Command HumanInteractionView::GetCommand(void)
    {
        MenuMaker maker;
        auto menu = maker.Build();
        return menu->GetCommand(*this);
    }
}
