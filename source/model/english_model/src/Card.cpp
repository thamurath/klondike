#include "english_model/Card.hpp"
namespace com::jlom::klondike::model::english_model
{
    Card::Card(FigureType const& figure, SuitType const& suit)
    : m_figure(figure)
    , m_suit(suit)
    {

    }
    Card::~Card()
    {
        
    }
    Card::FigureType Card::getFigure(void) const
    {
        return m_figure;
    }

    Card::SuitType Card::getSuit(void) const
    {
        return m_suit;
    }

    Card::ColorType Card::getColor(void) const
    {
        return color(m_suit);
    }

    Card::ColorType Card::color(SuitType const& suit)
    {
        return ((+SuitType::HEARTS == suit) || (+SuitType::DIAMONDS == suit))?ColorType::RED:ColorType::BLACK;
    }


    std::ostream& operator<< (std::ostream& os, Card const& card)
    {
        os << "fig:" << card.GetFigure() << "|suit:" << card.GetSuit();
        return os;
    }
}


//#endif //KLONDIKE_MODEL_ENGLISHMODEL_CARD_IMPL_HPP
