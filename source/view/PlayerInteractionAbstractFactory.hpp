#ifndef KLONDIKE_PLAYERINTERACTIONFACTORY_HPP
#define KLONDIKE_PLAYERINTERACTIONFACTORY_HPP

#include "PlayerInteraction.hpp"

#include <string>
namespace com::jlom::klondike::view
{
    class PlayerInteractionAbstractFactory
    {
    public:
        virtual PlayerInteraction* GetPlayerInteraction(std::string const& id) = 0;

        virtual ~PlayerInteractionAbstractFactory(void) = default;

    };
}

#endif //KLONDIKE_PLAYERINTERACTIONFACTORY_HPP
