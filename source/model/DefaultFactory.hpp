#ifndef _KLONDIKE_MODEL_DEFAULT_FACTORY_HPP__
#define _KLONDIKE_MODEL_DEFAULT_FACTORY_HPP__

#include "src/ModelFactory.hpp"

#include "CardHeap.hpp"
#include "english_model/Card.hpp"
#include "Card.hpp"
#include "Table.hpp"
#include "Deck.hpp"
#include "Pile.hpp"
#include "Waste.hpp"
#include "Foundation.hpp"


#include <vector>
#include <cstdlib>

namespace com::jlom::klondike::model
{

    template <typename Card, typename StatusEncoder, typename InformationHub>
    class DefaultFactory : public ModelFactory<DefaultFactory<Card, StatusEncoder, InformationHub >>
    {
    public:
        DefaultFactory(void) = default;

        DefaultFactory(DefaultFactory const&) = delete;
        DefaultFactory& operator=(DefaultFactory const&) = delete;

        DefaultFactory(DefaultFactory&&) = delete;
        DefaultFactory& operator=(DefaultFactory&&) = delete;

    private:
        friend class ModelFactory<DefaultFactory>;

        using CardType = Card;
        using DeckType = Deck <CardType, CardHeap>;
        using PileType = Pile<CardType, CardHeap>;
        using WasteType = Waste<CardType, CardHeap>;
        using FoundationType = Foundation<CardType, CardHeap>;

        using TableType = Table<CardType, DeckType , PileType, WasteType, FoundationType, StatusEncoder, InformationHub>;

        using SuitType = typename CardType::SuitType;
        using FigureType = typename CardType::FigureType;

        TableType createTable(DefaultFactory const& factory, StatusEncoder* encoder, InformationHub* hub) const;
        DeckType createDeck(void) const ;
        PileType createPile(std::uint8_t const& id, std::vector<CardType>&& cards) const;
        FoundationType createFoundation(void) const;
        WasteType createWaste(void) const;

    };
}

#include "src/DefaultFactory_impl.hpp"

#endif // _KLONDIKE_MODEL_DEFAULT_FACTORY_HPP__