#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "english_model/Card.hpp"

TEST_CASE("simple card test case")
{
    namespace model = com::jlom::klondike::model;
    namespace specific_model = model::english_model;
    using SpecificCard = specific_model::Card;


    SpecificCard three_diamonds{model::CardTraits<SpecificCard>::FigureType::THREE, model::CardTraits<SpecificCard>::SuitType::DIAMONDS};

    REQUIRE_FALSE(three_diamonds.IsFaceUp());

    three_diamonds.TurnUp();
    REQUIRE(three_diamonds.IsFaceUp());

    three_diamonds.TurnDown();
    REQUIRE_FALSE(three_diamonds.IsFaceUp());

    REQUIRE(model::CardTraits<SpecificCard>::FigureType::THREE == three_diamonds.GetFigure()._value);
    REQUIRE(model::CardTraits<SpecificCard>::SuitType::DIAMONDS == three_diamonds.GetSuit()._value);

}

