#ifndef KLONDIKE_VIEW_DECKVIEW_HPP
#define KLONDIKE_VIEW_DECKVIEW_HPP

#include "OutView.hpp"
#include "StatusDecoder.hpp"

namespace com::jlom::klondike::view
{
    class DeckView : public OutView
    {
    public:
        explicit DeckView(std::string id);
        virtual ~DeckView() = default;


        void DecodeStatus(const StatusDecoder &statusDecoder, Status const &status) override;
        void Show(void) override ;

    private:
        std::size_t numCards;
    };
}

#endif //KLONDIKE_VIEW_DECKVIEW_HPP
