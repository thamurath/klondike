#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "Table.hpp"
#include "Deck.hpp"
#include "Waste.hpp"
#include "Pile.hpp"
#include "DefaultFactory.hpp"


#include <memory>
#include <utility>
#include <iostream>
#include <variant>


#include <controller/InformationHub.hpp>
#include <common/JsonStatusEncoder.hpp>


TEST_CASE("table test")
{

    namespace common = com::jlom::klondike;
    namespace controller = common::controller;
    namespace model = common::model;


    std::cout << "factory" << std::endl;
    
    model::DefaultFactory<model::english_model::Card, common::JsonStatusEncoder, controller::InformationHub> factory;
    std::cout << "table" << std::endl;

    common::JsonStatusEncoder encoder;
    controller::InformationHub hub;

    auto table (factory.CreateTable(&encoder, &hub));
    constexpr std::size_t numDeckCards = ( decltype(table)::FigureType::_size() *
                                                       decltype(table)::SuitType::_size() );
    

    auto deck_src = table.Get<decltype(table)::DeckModelType>("deck");
    auto waste = table.Get<decltype(table)::WasteModelType>("waste");
    auto foundation_hearts = table.Get<decltype(table)::FoundationModelType>("foundation_HEARTS");
    if (deck_src)
    {
        auto card = deck_src->Get();
        deck_src->Pop();
        std::cout << *card << std::endl;
        deck_src->Push(std::move(*card));
    }

    else
    {
        std::cout << "nothing returns" << std::endl;
    }

    REQUIRE(false == table.IsEmpty("deck"));
    REQUIRE(table.IsEmpty("waste"));
    auto state = table.Begin("deck");
    std::cout << "deck" << std::endl;
    auto card = table.Get("deck");
    table.Pop("deck");
    std::cout << *card << std::endl;
    table.Push("deck", std::move(*card));
    std::cout << *table.Get("deck") << std::endl;
    table.Pop("deck");

    table.Commit("deck");
    table.Rollback("deck", state);
}

