#include "MenuChooseNumCards.hpp"

namespace com::jlom::klondike::view
{
    MenuItem::Options MenuChooseNumCards::GetOptions(void)
    {
        MenuItem::Options options;

        options.showText = "Enter the number of cards you wanna move";
        options.minOption = 1;
        options.maxOption = 10;

        return options;
    }

    void MenuChooseNumCards::DecodeOption(MenuContext& menuContext)
    {
        menuContext.Builder().SetNumberOfCards(menuContext.GetMenuOption());
    }
}
