#include "StatusDecoder.hpp"
#include <common/Status.hpp>
#include "FoundationView.hpp"

#include <spdlog/spdlog.h>

#include <sstream>
namespace com::jlom::klondike::view
{
    FoundationView::FoundationView(std::string id)
            : OutView(id)
            , topCard()
    {

    }

    void FoundationView::DecodeStatus(const StatusDecoder &statusDecoder, Status const &status)
    {
        spdlog::get("console")->trace("FoundationView::DecodeStatus id{} ", GetId());
        std::stringstream ss;
        auto cards = statusDecoder.Decode(GetId(), status);
        if (cards.empty())
        {
            topCard.reset();
            ss << "empty";
        }
        else
        {
            topCard = cards.back();
            ss << topCard.value();
        }
        spdlog::get("console")->trace("FoundationView::DecodeStatus id{}, topCard:{}", GetId(), ss.str());
    }

    void FoundationView::Show(void)
    {
        std::stringstream ss;

        ss << "id:"<< GetId()
           << ",topCard:";
        if (topCard.has_value())
        {
            ss << topCard.value();
        }
        else
        {
            ss << "empty";
        }

        GetView().ShowText(ss.str());
    }

}