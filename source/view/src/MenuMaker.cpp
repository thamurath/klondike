#include "MenuMaker.hpp"

#include "MenuPathChooser.hpp"
#include "MenuInitial.hpp"
#include "MenuTerminal.hpp"

#include "MenuMoveCard.hpp"
#include "MenuRedealFillIn.hpp"
#include "MenuMoveFromDeckFillIn.hpp"
#include "MenuMoveFromWaste.hpp"
#include "MenuChooseFoundation.hpp"
#include "MenuChooseDestinationPile.hpp"
#include "MenuChooseSourcePile.hpp"
#include "MenuChooseNumCards.hpp"
#include "MenuRevealFillIn.hpp"
#include "MenuMoveFromPile.hpp"

#include <memory>
#include <utility>
namespace com::jlom::klondike::view
{
    ///@jlom TODO: This is crappy, too complicated and error prone...
    /// I should not create the options without knowing the number and order by "inspiration" ...
    /// Maybe the Menus should be the ones creating and inserting the correct options ...
    /// I am pretty sure there should be an easy way to do this ... but I cannot find it now ...
    /// TODO: Try to do this in a better way
    std::unique_ptr<Menu> MenuMaker::Build(void)
    {
        // Read from botton up to try to give this a little bit of sense ...

        auto chooseFoundationFromPile = std::make_unique<MenuChooseFoundation>();
        chooseFoundationFromPile->AddChild(std::make_unique<MenuTerminal>());

        auto chooseNumCards = std::make_unique<MenuChooseNumCards>();
        chooseNumCards->AddChild(std::make_unique<MenuTerminal>());

        auto chooseDstPileFromPile = std::make_unique<MenuChooseDestinationPile>();
        chooseDstPileFromPile->AddChild(std::move(chooseNumCards));

        auto menuRevealFillIn = std::make_unique<MenuRevealFillIn>();
        menuRevealFillIn->AddChild(std::make_unique<MenuTerminal>());

        auto moveFromPileChooser = std::make_unique<MenuPathChooser>();
        moveFromPileChooser->AddOption(1, std::move(chooseFoundationFromPile));
        moveFromPileChooser->AddOption(2, std::move(chooseDstPileFromPile));
        moveFromPileChooser->AddOption(3, std::move(menuRevealFillIn));

        auto chooseFoundation = std::make_unique<MenuChooseFoundation>();
        chooseFoundation->AddChild(std::make_unique<MenuTerminal>());

        auto chooseDstPile = std::make_unique<MenuChooseDestinationPile>();
        chooseDstPile->AddChild(std::make_unique<MenuTerminal>());

        auto moveFromWasteChooser = std::make_unique<MenuPathChooser>();
        moveFromWasteChooser->AddOption(1, std::move(chooseFoundation));
        moveFromWasteChooser->AddOption(2, std::move(chooseDstPile));


        auto moveFromKnownPile = std::make_unique<MenuMoveFromPile>();
        moveFromKnownPile->AddChild(std::move(moveFromPileChooser));

        auto moveFromPile = std::make_unique<MenuChooseSourcePile>();
        moveFromPile->AddChild(std::move(moveFromKnownPile));

        auto moveFromWaste = std::make_unique<MenuMoveFromWaste>();
        moveFromWaste->AddChild(std::move(moveFromWasteChooser));

        auto moveFromDeckFillIn = std::make_unique<MenuMoveFromDeckFillIn>();
        moveFromDeckFillIn->AddChild(std::make_unique<MenuTerminal>());

        auto moveCardPathChooser = std::make_unique<MenuPathChooser>();
        moveCardPathChooser->AddOption(1, std::move(moveFromDeckFillIn));
        moveCardPathChooser->AddOption(2, std::move(moveFromWaste));
        moveCardPathChooser->AddOption(3, std::move(moveFromPile));

        auto menuMoveCard = std::make_unique<MenuMoveCard>();
        menuMoveCard->AddChild(std::move(moveCardPathChooser));

        auto menuRedealFillIn = std::make_unique<MenuRedealFillIn>();
        menuRedealFillIn->AddChild(std::make_unique<MenuTerminal>());

        auto initialPathChooser = std::make_unique<MenuPathChooser>();
        initialPathChooser->AddOption(1, std::move(menuMoveCard));
        initialPathChooser->AddOption(2, std::make_unique<MenuTerminal>());
        initialPathChooser->AddOption(3, std::move(menuRedealFillIn));


        auto initialMenu = std::make_unique<MenuInitial>();
        initialMenu->AddChild(std::move(initialPathChooser));

        return initialMenu;
    }
}

