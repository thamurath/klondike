#include "CommandBuilder.hpp"

namespace com::jlom::klondike::view {
    CommandBuilder &CommandBuilder::SetType(const CommandType &type)
    {
        this->type = type;
        return *this;
    }

    CommandBuilder &CommandBuilder::SetSource(std::string const &src)
    {
        this->src = src;
        return *this;
    }

    CommandBuilder &CommandBuilder::SetDestination(std::string const &dst)
    {
        this->dst = dst;
        return *this;
    }

    CommandBuilder& CommandBuilder::SetSourceType(ElementType const& srcType)
    {
        this->srcType = srcType;
        return *this;
    }
    CommandBuilder& CommandBuilder::SetDestinationType(ElementType const& dstType)
    {
        this->dstType = dstType;
        return *this;
    }

    CommandBuilder &CommandBuilder::SetNumberOfCards(std::size_t const &numCards)
    {
        this->numCards = numCards;
        return *this;
    }

    Command CommandBuilder::Build(void)
    {
        return Command(*type, src.value_or(""), srcType.value_or(ElementType::NOT_APLICABLE)
                , dst.value_or(""), dstType.value_or(ElementType::NOT_APLICABLE)
                , numCards);
    }


    std::optional<std::string> CommandBuilder::GetSource() const
    {
        return src;
    }
}