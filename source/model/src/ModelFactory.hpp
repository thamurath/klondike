#ifndef _KLONDIKE_MODEL_MODEL_FACTORY_HPP__
#define _KLONDIKE_MODEL_MODEL_FACTORY_HPP__

#include <utility>

///@TODO Should be built with a "json-configuration" parameter to know how many elements have to build of each one.
namespace com::jlom::klondike::model
{
    template <typename SpecificFactory>
    class ModelFactory
    {
    public:
        template <typename Encoder, typename Hub>
        auto CreateTable(Encoder* encoder, Hub* hub) const
        {
            return static_cast<SpecificFactory const *>(this)->createTable(*static_cast<SpecificFactory const*>(this), encoder, hub);
        }
        auto CreateDeck(void) const
        {
            return static_cast<SpecificFactory const *>(this)->createDeck();
        }

        template <template<typename T, typename ... Args > class container_type, typename card_type, typename id_type>
        auto CreatePile(id_type&& id, container_type<card_type>&& cards) const
        {
            return static_cast<SpecificFactory const *>(this)->createPile(std::forward<id_type>(id), std::forward<container_type<card_type>>(cards));
        }

        auto CreateFoundation(void) const
        {
            return static_cast<SpecificFactory const *>(this)->createFoundation();
        }

        auto CreateWaste(void) const
        {
            return static_cast<SpecificFactory const *>(this)->createWaste();
        }
    };
}

#endif // _KLONDIKE_MODEL_MODEL_FACTORY_HPP__