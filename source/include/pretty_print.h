#ifndef CPPLIB_DEBUG_PRETTY_PRINT_H
#define CPPLIB_DEBUG_PRETTY_PRINT_H
///@note: All this has been taken from https://stackoverflow.com/questions/4850473/pretty-print-c-stl-containers
/// the solution inside the question itself.
/// Note there is a link to a Channel9 video that could be very interesting ...

#include <type_traits> // for many things
#include <utility> // for pair
#include <string> // for char_traits

#include <set>
#include <map>

namespace debug
{
    namespace pretty_print
    {
        template <typename T>
        struct is_container_helper
        {
        private:
            template <typename C> static char test(typename C::const_iterator*);
            // esto no esta bien porque sizeof(int) puede ser igual sizeof (char)
            template <typename C> static int test(...);
        public:
            static const bool value = sizeof(test<T>(0) == sizeof(char)); // se me ocurre que puede ser constexpr
        };

        // Basic is_container struct
        // specialize for every container we want pretty_print to work with
        // Esto es un monton de curro me parece a mi ... es necesario ?
        // std::integral_constant<typename T, T v> es un wrapper sobre la constante estatica v de tipo T
        // es la base de los type_traits ya que:
        // true_type = std::integral_constant<bool, true>::value;
        template <typename T>
        struct is_container : public ::std::integral_constant<bool, is_container_helper<T>::value> {};


        // Ahora definimos la estructura para contener los delimitadores para un contenedor concreto

        // Holds delimiter values for a specific character type
        template <typename TChar>
        struct delimiters_values
        {
            typedef TChar char_type;
            const TChar* prefix;
            const TChar* delimiter;
            const TChar* postfix;
        };

        // delimiter values for a specific container and character type
        template <typename T, typename TChar>
        struct delimiters
        {
            typedef delimiters_values<TChar> type;
            static const type values;
        };



        // default delimiters for different character types ( especializaciones )
        template <typename T>
        struct delimiters<T, char>
        {
            static const delimiters_values<char> values; // declaracion
        };
        // definicion
        template <typename T>
        const delimiters_values<char> delimiters<T,char>::values = {
                "[", //prefix
                ",", //delimiter
                "]"  // postfix
        };
        // ahora la misma especializacion pero para wchar_t en lugar de solo para char
        template <typename T>
        struct delimiters<T, wchar_t>
        {
            static const delimiters_values<wchar_t> values; // declaracion
        };
        // definicion
        template <typename T>
        const delimiters_values<wchar_t> delimiters<T,wchar_t>::values = {
                L"[", //prefix
                L",", //delimiter
                L"]"  // postfix
        };

        // Delimiters for set
        template < typename T, typename TTraits, typename TAllocator>
        struct delimiters<std::set<T,TTraits,TAllocator>, char>
        {
            static const delimiters_values<char> values;// declaracion
        };
        // definicion
        template <typename T, typename TTraits, typename TAllocator>
        const delimiters_values<char> delimiters<std::set<T,TTraits,TAllocator>, char>::values = {
                "{", // prefix
                ", ", //delimiter
                "}", //postfix
        };
        // ahora exactamente lo mismo pero para el wchar_t
        template < typename T, typename TTraits, typename TAllocator>
        struct delimiters<std::set<T,TTraits,TAllocator>, wchar_t>
        {
            static const delimiters_values<wchar_t> values;// declaracion
        };
        // definicion
        template <typename T, typename TTraits, typename TAllocator>
        const delimiters_values<wchar_t> delimiters<std::set<T,TTraits,TAllocator>, wchar_t>::values = {
                L"{", // prefix
                L", ", //delimiter
                L"}", //postfix
        };

        // delimiters for pair
        template <typename T1, typename T2>
        struct delimiters<::std::pair<T1,T2>,char>
        {
            static const delimiters_values<char> values;
        };
        template <typename T1, typename T2>
        const delimiters_values<char> delimiters<::std::pair<T1,T2>, char>::values = {
                "(", // prefix
                ", ", // delimiter
                ")" // postfix
        };
        //the same but for wchat_t
        template <typename T1, typename T2>
        struct delimiters<::std::pair<T1,T2>,wchar_t>
        {
            static const delimiters_values<wchar_t> values;
        };
        template <typename T1, typename T2>
        const delimiters_values<wchar_t> delimiters<::std::pair<T1,T2>, wchar_t>::values = {
                L"(", // prefix
                L", ", // delimiter
                L")" // postfix
        };

        template <typename T, typename TChar = char , typename TCharTraits = std::char_traits<TChar>, typename TDelimiters = delimiters<T, TChar>>
        struct print_container_helper
        {
            typedef TChar char_type;
            typedef TDelimiters delimiters_type;
            typedef std::basic_ostream<TChar, TCharTraits>& ostream_type;

            // constructor
            print_container_helper(T const& container)
                    : m_container(container)
            {

            }

            inline void operator()(ostream_type& stream) const
            {
                if (nullptr != print_container_helper::delimiters_type::values.prefix)
                {
                    stream << delimiters_type::values.prefix;
                }

                for (auto begin = std::begin(m_container), it = begin, end = std::end(m_container); it != end; ++it)
                {
                    if ( (begin != it) && (nullptr != delimiters_type::values.delimiter))
                    {
                        stream << delimiters_type::values.delimiter;
                    }
                    stream << *it;
                }

                if ( nullptr != delimiters_type::values.postfix)
                {
                    stream << delimiters_type::values.postfix;
                }
            }
        private:
            T const& m_container;
        };
    }//namespace pretty_print
}// namespace debug

namespace std
{

    template<typename T
            , typename TChar
            , typename TCharTraits
            , typename TDelimiters
    >
    inline basic_ostream<TChar, TCharTraits> &operator<<(basic_ostream<TChar, TCharTraits> &stream ,
                                                         const ::debug::pretty_print::print_container_helper<T, TChar, TCharTraits, TDelimiters> &helper)
    {
        helper(stream); 
        return stream;
    };

    template <typename T, typename TChar, typename TCharTraits>
    inline typename enable_if<::debug::pretty_print::is_container<T>::value, basic_ostream<TChar,TCharTraits>&>::type
    operator<< (basic_ostream<TChar,TCharTraits>& stream, T const& container)
    {
        return stream << ::debug::pretty_print::print_container_helper<T, TChar, TCharTraits>(container);
    };


    template <typename TChar, typename TCharTraits, typename T1, typename T2>
    inline basic_ostream<TChar, TCharTraits>& operator<<( basic_ostream<TChar, TCharTraits>& stream, std::pair<T1,T2> const& pair)
    {
        if (nullptr != ::debug::pretty_print::delimiters<std::pair<T1,T2>,TChar>::values.prefix )
        {
            stream << ::debug::pretty_print::delimiters<std::pair<T1,T2>,TChar>::values.prefix;
        }

        stream << pair.first;

        if (nullptr != ::debug::pretty_print::delimiters<std::pair<T1,T2>,TChar>::values.delimiter)
        {
            stream << ::debug::pretty_print::delimiters<std::pair<T1,T2>,TChar>::values.delimiter;
        }

        stream << pair.second;

        if (nullptr != ::debug::pretty_print::delimiters<std::pair<T1,T2>,TChar>::values.postfix )
        {
            stream << ::debug::pretty_print::delimiters<std::pair<T1,T2>,TChar>::values.postfix;
        }
    };

} // namespace std


namespace debug
{
    namespace  pretty_print
    {
        struct tuple_dummy_t {};
        typedef std::pair<tuple_dummy_t,tuple_dummy_t> tuple_dummy_pair;

        template<typename TChar, typename TCharTraits, typename Tuple, size_t N>
        struct pretty_tuple_helper
        {
            static inline void print(::std::basic_ostream<TChar,TCharTraits>& stream, const Tuple& value)
            {
                pretty_tuple_helper<TChar,TCharTraits,Tuple,N-1>::print(stream, value);
                if (nullptr != delimiters<tuple_dummy_pair,TChar>::values.delimiter)
                {
                    stream << delimiters<tuple_dummy_pair,TChar>::values.delimiter;
                }

                stream << std::get<N-1>(value);
            }
        };

        // recursion end
        template<typename TChar, typename TCharTraits, typename Tuple>
        struct pretty_tuple_helper<TChar, TCharTraits, Tuple, 1>
        {
            static inline void print(::std::basic_ostream<TChar, TCharTraits> &stream, const Tuple &value)
            {
                stream << ::std::get<0>(value);
            }
        };
    }
}

namespace std
{
    template <typename TChar, typename TCharTraits, typename ... Args>
    inline basic_ostream<TChar,TCharTraits>& operator<<(basic_ostream<TChar,TCharTraits>& stream, tuple<Args...> const& value)
    {
        if (nullptr != ::debug::pretty_print::delimiters<::debug::pretty_print::tuple_dummy_pair,TChar>::values.prefix)
        {
            stream << ::debug::pretty_print::delimiters<::debug::pretty_print::tuple_dummy_pair,TChar>::values.prefix;
        }

        ::debug::pretty_print::pretty_tuple_helper<TChar, TCharTraits, tuple<Args...>const&, sizeof...(Args)>::print(stream,value);

        if (nullptr != ::debug::pretty_print::delimiters<::debug::pretty_print::tuple_dummy_pair,TChar>::values.postfix)
        {
            stream << ::debug::pretty_print::delimiters<::debug::pretty_print::tuple_dummy_pair,TChar>::values.postfix;
        }
    };
}


#endif //CPPLIB_DEBUG_PRETTY_PRINT_H
