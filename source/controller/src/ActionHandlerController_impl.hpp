#ifndef KLONDIKE_CONTROLLER_ACTIONHANDLERCONTROLLER_IMPL_HPP
#define KLONDIKE_CONTROLLER_ACTIONHANDLERCONTROLLER_IMPL_HPP


#ifndef KLONDIKE_CONTROLLER_ACTIONHANDLERCONTROLLER_HPP
#error "Should not include this file"
#endif

namespace com::jlom::klondike::controller
{
    template <typename ModelInterface>
    ActionHandleController<ModelInterface>::ActionHandleController(ModelInterface const& modelInterface)
    : modelInterface(modelInterface)
    {

    }
}

#endif //KLONDIKE_CONTROLLER_ACTIONHANDLERCONTROLLER_IMPL_HPP
