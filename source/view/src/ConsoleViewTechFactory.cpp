#include "ConsoleViewTechFactory.hpp"

#include "ConsoleViewTech.hpp"

namespace com::jlom::klondike::view
{
    ViewTechImplementation* ConsoleViewTechFactory::GetViewTechnology(void)
    {
        return new ConsoleViewTech;
    }
}
