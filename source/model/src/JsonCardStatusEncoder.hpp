#ifndef KLONDIKE_MODEL_JSONSTATUSENCODER_HPP
#define KLONDIKE_MODEL_JSONSTATUSENCODER_HPP


#include <json.hpp>

namespace com::jlom::klondike::model
{
    template <typename Card>
    void to_json(nlohmann::json& j, Card const& card)
    {
        j = nlohmann::json{ {"figure", card.GetFigure()._to_string() }
                , {"suit", card.GetSuit()._to_string()}
                , {"color", card.GetColor()._to_string() }
                , {"faceup", card.IsFaceUp()}};
    }
} //com::jlom::klondike::model

#endif //KLONDIKE_MODEL_JSONSTATUSENCODER_HPP
