#ifndef KLONDIKE_AIINTERACTIONFACTORY_HPP
#define KLONDIKE_AIINTERACTIONFACTORY_HPP

#include "PlayerInteraction.hpp"
#include "PlayerInteractionAbstractFactory.hpp"

namespace com::jlom::klondike::view
{
    class AIInteractionFactory : public PlayerInteractionAbstractFactory
    {
    public:
        PlayerInteraction* GetPlayerInteraction(std::string const& id) override ;
    };
}


#endif //KLONDIKE_AIINTERACTIONFACTORY_HPP
