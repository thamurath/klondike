#ifndef KLONDIKE_CONTROLLER_VIEWINTERFACE_HPP
#define KLONDIKE_CONTROLLER_VIEWINTERFACE_HPP


#include <common/Command.hpp>
#include <common/ElementTypes.hpp>
#include <common/Action.hpp>

#include <unordered_map>
#include <functional>
#include <tuple>
#include <cstdint>

namespace com::jlom::klondike::controller
{
    template <typename ActionHandlerController>
    class ViewInterface final
    {
    public:
        explicit ViewInterface(ActionHandlerController & actionHandlerController);
        ~ViewInterface() = default;


        std::int32_t ProcessCommand(Command const& command);

        //avoid copy
        ViewInterface(ViewInterface const&) = delete;
        ViewInterface& operator=(ViewInterface const&) = delete;

        // avoid move
        ViewInterface(ViewInterface&&) = delete;
        ViewInterface& operator=(ViewInterface&&) = delete;
    protected:
    private:
        ActionHandlerController& actionHandlerController;

        /// Need to specialize std::hash for std::tuple to be able to use it as a key in std::unordered_map
        ///@see https://stackoverflow.com/questions/20834838/using-tuple-in-unordered-map

        using key_t = std::tuple<ElementType, ElementType>;
        struct key_hash : public std::unary_function<key_t, std::size_t>
        {
            std::size_t operator()(key_t const& key) const
            {
                std::hash<std::uint32_t> uint32HashFn;
                return uint32HashFn(std::get<0>(key)) ^ uint32HashFn(std::get<1>(key));
            }
        };

        struct key_equal : public std::binary_function<key_t, key_t, bool>
        {
            bool operator()(key_t const& lhs, key_t const& rhs) const
            {
                return (std::get<0>(lhs) == std::get<0>(rhs) ) &&
                        (std::get<1>(lhs) == std::get<1>(rhs));
            }
        };
        using Translator = std::unordered_map<key_t, ACTION_TYPE,key_hash, key_equal >;

        Translator translator;
    };
}

#include "src/ViewInterface_impl.hpp"

#endif // KLONDIKE_CONTROLLER_VIEWINTERFACE_HPP