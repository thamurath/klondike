#ifndef KLONDIKE_MENUREDEALFILLIN_HPP
#define KLONDIKE_MENUREDEALFILLIN_HPP

#include "MenuFillIn.hpp"

namespace com::jlom::klondike::view
{
    class MenuRedealFillIn : public MenuFillIn
    {
        void FillIn(MenuContext& menuContext) override
        {
            ///@jlom TODO need configuration
            menuContext.Builder().SetSource("waste");
            menuContext.Builder().SetSourceType(ElementType::WASTE);
            menuContext.Builder().SetDestination("deck");
            menuContext.Builder().SetDestinationType(ElementType::DECK);
        }
    };
}


#endif //KLONDIKE_MENUREDEALFILLIN_HPP
