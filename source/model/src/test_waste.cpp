#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include "english_model/Card.hpp"

#include "Waste.hpp"
#include "CardHeap.hpp"

TEST_CASE("simple waste test case")
{
    namespace model = com::jlom::klondike::model;
    namespace specific_model = model::english_model;
    using SpecificCard = specific_model::Card;

    model::Waste<SpecificCard, model::CardHeap> waste;

    REQUIRE(waste.IsEmpty());

    SpecificCard three_diamonds{model::CardTraits<SpecificCard>::FigureType::THREE, model::CardTraits<SpecificCard>::SuitType::DIAMONDS};

    auto copy (three_diamonds);
    three_diamonds.TurnUp();
    // can insert face up card
    CHECK(0 == waste.Push(std::move(three_diamonds)));
    REQUIRE(1 == waste.Size());

}