#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "Pile.hpp"
#include "english_model/Card.hpp"
#include "CardHeap.hpp"

#include <optional.hpp>

TEST_CASE("simple test","[model] [pile]")
{
    namespace model = com::jlom::klondike::model;
    namespace specific_model = model::english_model;
    using SpecificCard = specific_model::Card;

    SpecificCard c1(model::CardTraits<SpecificCard>::FigureType::ACE, model::CardTraits<SpecificCard>::SuitType::DIAMONDS);
    SpecificCard c2(model::CardTraits<SpecificCard>::FigureType::TWO, model::CardTraits<SpecificCard>::SuitType::DIAMONDS);
    SpecificCard c3(model::CardTraits<SpecificCard>::FigureType::THREE, model::CardTraits<SpecificCard>::SuitType::DIAMONDS);

    c3.TurnUp();

    model::Pile<SpecificCard , model::CardHeap> pile(3,{c1,c2,c3} );

    REQUIRE( 3 == pile.Size());
    REQUIRE( !pile.IsEmpty());

    auto state = pile.Begin();


    auto card = pile.Get();
    pile.Pop();
    REQUIRE(card.has_value());
    CHECK(card->GetFigure() == +(model::CardTraits<SpecificCard>::FigureType::THREE));


    REQUIRE( 2 == pile.Size());
    REQUIRE( !pile.IsEmpty());

    pile.Rollback(state);

    REQUIRE( 3 == pile.Size());
    REQUIRE( !pile.IsEmpty());

}
