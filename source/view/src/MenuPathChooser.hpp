#ifndef KLONDIKE_MENUPATHCHOOSER_HPP
#define KLONDIKE_MENUPATHCHOOSER_HPP

#include "Menu.hpp"

#include <memory>
#include <unordered_map>
namespace com::jlom::klondike::view
{
    class MenuPathChooser : public Menu
    {
    public:
        Command Handle(MenuContext &menuContext) override ;

        void AddOption(std::uint32_t optionNumber, std::unique_ptr<Menu> menuOption);

    private:
        using MenuTable = std::unordered_map<std::uint32_t, std::unique_ptr<Menu>>;
        MenuTable table;
    };
}

#endif //KLONDIKE_MENUPATHCHOOSER_HPP
