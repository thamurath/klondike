#ifndef KLONDIKE_STATUSTRANSCODERFACTORY_HPP
#define KLONDIKE_STATUSTRANSCODERFACTORY_HPP

namespace com::jlom::klondike
{
    template <typename SpecificFactory>
    class StatusTranscoderFactory
    {
    public:
        auto CreateStatusEncoder(void) const
        {
            return asDerived().createStatusEncoder();
        }

        auto CreateStatusDecoder(void) const
        {
            return asDerived().createStatusDecoder();
        }

    private:
        constexpr SpecificFactory& asDerived(void)
        {
            return *static_cast<SpecificFactory*>(this);
        }

        constexpr SpecificFactory const& asDerived(void) const
        {
            return *static_cast<SpecificFactory const*>(this);
        }
    };
}

#endif //KLONDIKE_STATUSTRANSCODERFACTORY_HPP
