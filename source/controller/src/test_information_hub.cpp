#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "InformationHub.hpp"
#include <common/NotificationBase.hpp>
#include <common/ObserverAdapter.hpp>

#include <Poco/Notification.h>
#include <Poco/Observer.h>
#include <Poco/NObserver.h>

#include <memory>


class TestNotification : public com::jlom::klondike::NotificationBase
{
public:
    TestNotification(std::string const& data)
            :data (data)
    {

    }

    std::string GetData(void) const
    {
        return data;
    }
private:
    std::string data;
};

class Target
{
public:
    void HandleTestNotification(TestNotification* testNotification)
    {
        std::cout << "Target::HandleTestNotification:" << testNotification->name() << std::endl;
        std::cout << "Target::HandleTestNotification:" << testNotification->GetData() << std::endl;
    }
};

class SmartTarget
{
public:
    void HandleTestNotification(const Poco::AutoPtr<TestNotification>& testNotification)
    {
        std::cout << "SmartTarget::HandleTestNotification:" << testNotification->name() << std::endl;
        std::cout << "SmartTarget::HandleTestNotification:" << testNotification->GetData() << std::endl;
    }
    void HandleTestNotification_Raw(TestNotification* testNotification)
    {
        std::cout << "SmartTarget::HandleTestNotification:" << testNotification->name() << std::endl;
        std::cout << "SmartTarget::HandleTestNotification:" << testNotification->GetData() << std::endl;
    }
};

TEST_CASE("information_hub")
{
    namespace common = com::jlom::klondike;
    namespace controller = common::controller;

    controller::InformationHub informationHub;
    Target target;
    SmartTarget smartTarget;

    informationHub.AddObserver(common::ObserverAdapter<Target,TestNotification>(target, &Target::HandleTestNotification));
    informationHub.AddObserver(common::ObserverAdapter<SmartTarget,TestNotification>(smartTarget, &SmartTarget::HandleTestNotification_Raw));

    std::string information("Hola mundo");
    informationHub.PostNotification(new TestNotification(information));
}