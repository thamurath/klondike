#include "Command.hpp"


namespace com::jlom::klondike
{
    Command::Command(const CommandType &type, std::string const &srcId, ElementType const &srcType,
                     std::string const &dstId, ElementType const &dstType, std::size_t numCards)
    : type(type)
    , srcId(srcId)
    , dstId(dstId)
    , numCards(numCards)
    , srcType(srcType)
    , dstType(dstType)
    {

    }

    CommandType const& Command::GetCommandType(void) const
    {
        return type;
    }

    std::string const& Command::GetSourceId(void) const
    {
        return srcId;
    }

    std::string const& Command::GetDestinationId(void) const
    {
        return dstId;
    }

    ElementType const& Command::GetSourceType(void) const
    {
        return srcType;
    }

    ElementType const& Command::GetDestinationType(void) const
    {
        return dstType;
    }

    std::size_t const& Command::GetNumCards(void) const
    {
        return numCards;
    }

    std::ostream& operator<<(std::ostream& os, Command const& cmd)
    {
        os << "Command[type:" << cmd.GetCommandType()._to_string()
           << ",srcId:" << cmd.GetSourceId()
           << ",srcType:" << cmd.GetSourceType()._to_string()
           << ",dstId:" << cmd.GetDestinationId()
           << ",dstType:" << cmd.GetDestinationType()._to_string()
           << ",numCards:" << cmd.GetNumCards()
           << "];";

        return os;
    }
}

