#ifndef KLONDIKE_HUMANMENUMAKER_HPP
#define KLONDIKE_HUMANMENUMAKER_HPP

#include "Menu.hpp"

#include <memory>
namespace com::jlom::klondike::view
{
    class MenuMaker
    {
    public:
        std::unique_ptr<Menu> Build(void);
    };
}

#endif //KLONDIKE_HUMANMENUMAKER_HPP
