#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "DefaultFactory.hpp"
#include "Card.hpp"
namespace model = com::jlom::klondike::model;

#include "hotchpotch.hpp"

#include <variant>
#include <map>
#include <utility>
#include <vector>

#include <common/JsonStatusEncoder.hpp>
#include <controller/InformationHub.hpp>

template<typename Card, typename T>
auto extract_cards(std::size_t const& number,T& deck)
{
    std::vector<Card> cards;
    cards.reserve(number);
    for (std::size_t idx(0); idx < number; ++idx)
    {
        cards.push_back(*deck.Get());
        deck.Pop();
    }
    return cards;
}




TEST_CASE("testing default factory")
{
    namespace common = com::jlom::klondike;
    namespace controller = common::controller;
    namespace hp = common::hotchpotch;
    namespace model = common::model;
    namespace specific_model = model::english_model;
    using SpecificCard = specific_model::Card;


    using DeckType = model::Deck<model::english_model::Card, model::CardHeap>;
    using WasteType = model::Waste<model::english_model::Card, model::CardHeap>;
    using PileType = model::Pile<model::english_model::Card, model::CardHeap>;
    using FoundationType = model::Foundation<model::english_model::Card, model::CardHeap>;


    using DFact = model::DefaultFactory<model::english_model::Card, common::JsonStatusEncoder, controller::InformationHub>;
    DFact dfac;
    auto deck = dfac.CreateDeck();


    deck.Shuffle();


    auto pile_1 = dfac.CreatePile(1, hp::TurnUpFirst(extract_cards<SpecificCard >(1,deck)) );
    auto pile_2 = dfac.CreatePile(2, hp::TurnUpFirst(extract_cards<SpecificCard >(2,deck)) );
    
    auto foundation_hearts = dfac.CreateFoundation();


}