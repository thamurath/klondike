#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include <iostream>
#include <better_enums.hpp>

#include <iterator>

BETTER_ENUM(Words, int, Hello =10, World, mierda=24, puta);

Words::_enumerated GetHello(void)
{
    return Words::Hello;
}

bool CheckHello(Words::_enumerated const& word)
{
    return word == Words::Hello;
}
TEST_CASE("simple better enums test") {
    std::cout << +Words::Hello << ", " << +Words::World << "!" << std::endl;
    std::cout << (+Words::Hello)<< std::endl;
    std::cout <<+(GetHello()) << std::endl;

    REQUIRE(Words::Hello == GetHello());
    REQUIRE(true == CheckHello(Words::Hello));
    REQUIRE(false == CheckHello(Words::World));


    try {
        switch (Words::_from_string("Helloa")) {
            case Words::Hello:
                std::cout << "O, hai!" << std::endl;
                break;

            case Words::World:
                std::cout << "Wat?" << std::endl;
                break;

        }
    }
    catch(std::runtime_error e)
    {
        std::cerr << "error: " << e.what() << std::endl;
    }

    for (auto word : Words::_names())
        std::cout << word << std::endl;

    for (auto value : Words::_values())
        std::cout << value << "==" << value._to_integral() << std::endl;
}

TEST_CASE("const expr")
{
    Words::_enumerated word = Words::mierda;

    REQUIRE(Index<Words>(Words::mierda) == Index<Words>(word));

    REQUIRE(2 == Index<Words>(Words::mierda));
    static_assert(2 == Index<Words>(Words::mierda));
    REQUIRE(isPrevious<Words>(Words::mierda,Words::puta));
    static_assert(isPrevious<Words>(Words::mierda, Words::puta));
    REQUIRE(isPrevious<Words>(word,Words::puta));

}