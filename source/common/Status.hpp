#ifndef KLONDIKE_STATUS_HPP
#define KLONDIKE_STATUS_HPP

#include <string>
#include <utility>
namespace com::jlom::klondike::view
{
    class Status
    {
    public:
        Status(std::string data)
        : data(std::move(data))
        {

        }
        std::string const& str(void) const
        {
            return data;
        }
    private:
        std::string data;
    };
}

#endif //KLONDIKE_STATUS_HPP
