#include "MenuChooseDestinationPile.hpp"

#include <cassert>
namespace com::jlom::klondike::view
{
    void MenuChooseDestinationPile::DecodeOption(MenuContext& menuContext)
    {
        ///@jlom TODO: This needs access to configuration for the foundation ids
        menuContext.Builder().SetDestinationType(ElementType::PILE);
        switch (menuContext.GetMenuOption())
        {
            case 1:
            {
                menuContext.Builder().SetDestination("pile_1");
                break;
            }
            case 2:
            {
                menuContext.Builder().SetDestination("pile_2");
                break;
            }
            case 3:
            {
                menuContext.Builder().SetDestination("pile_3");
                break;
            }
            case 4:
            {
                menuContext.Builder().SetDestination("pile_4");
                break;
            }
            case 5:
            {
                menuContext.Builder().SetDestination("pile_5");
                break;
            }
            case 6:
            {
                menuContext.Builder().SetDestination("pile_6");
                break;
            }
            case 7:
            {
                menuContext.Builder().SetDestination("pile_7");
                break;
            }
            default:
                assert(false);
        }
    }
}