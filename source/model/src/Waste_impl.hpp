//
// Created by eojojos on 10/9/17.
//

#ifndef KLONDIKE_MODEL_WASTE_IMPL_HPP
#define KLONDIKE_MODEL_WASTE_IMPL_HPP

#ifndef __KLONDIKE_MODEL_WASTE_HPP__
#error "Should not include this file"
#endif

#include <cassert>
#include <algorithm>
#include <iterator>

namespace com::jlom::klondike::model
{
    template <typename Card, template <typename C> class Heap>
    auto Waste<Card, Heap>::IsEmpty(void) const
    {
        return heap.IsEmpty();
    }


    template <typename Card, template <typename C> class  Heap>
    auto Waste<Card, Heap>::Size(void) const
    {
        return heap.Size();
    }

    template <typename Card, template <typename C> class Heap>
    auto Waste<Card, Heap>::Get(void) const
    {
        assert(!heap.IsEmpty());
        return heap.Get();
    }
    template <typename Card, template <typename C> class Heap>
    auto Waste<Card, Heap>::GetCards(std::size_t const& numCards) const
    {
        return heap.GetCards(numCards);
    };

    template<typename Card, template <typename C> class Heap>
    void Waste<Card, Heap>::Pop(void)
    {
        assert(!heap.IsEmpty());
        heap.Pop();
    }

    template <typename Card, template <typename C> class Heap>
    auto Waste<Card, Heap>::Push(Card&& card)
    {
        assert(card.IsFaceUp());
        return heap.Push(std::move(card));
    }

    template <typename Card, template <typename C> class Heap>
    template <typename StatusEncoder>
    auto Waste<Card, Heap>::EncodeStatus(std::string const& id, StatusEncoder* statusEncoder) const
    {
        assert(nullptr != statusEncoder);
        return heap.EncodeStatus(id, statusEncoder);
    }


    /// Transactional Interface
    template <typename Card, template <typename C> class Heap>
    typename Waste<Card, Heap>::state_type Waste<Card, Heap>::begin(void) const
    {
        return Waste::state_type(heap.GetAllCards());
    }

    template <typename Card, template <typename C> class Heap>
    void Waste<Card, Heap>::commit(void)
    {
        auto cards = heap.GetAllCards();
        assert(std::all_of(std::begin(cards), std::end(cards),[](Card const& c) { return c.IsFaceUp();}) );
    }

    template <typename Card, template <typename C> class Heap>
    void Waste<Card, Heap>::rollback(Waste::state_type const& state)
    {
        auto cards = state.m_cards;
        heap.Reset(std::move(cards));
    }

}//namespace com::jlom::klondike::model

#endif //KLONDIKE_WASTE_IMPL_HPP
