
#ifndef TESTINGCPP_POCONOTIFICATIONEXAMPLE_HPP
#define TESTINGCPP_POCONOTIFICATIONEXAMPLE_HPP

#include <Poco/NotificationCenter.h>
#include <Poco/Notification.h>
#include <Poco/Observer.h>
#include <Poco/NObserver.h>
#include <Poco/AutoPtr.h>

#include <iostream>

using Poco::NotificationCenter;
using Poco::Notification;
using Poco::Observer;
using Poco::NObserver;
using Poco::AutoPtr;


class BaseNotification : public Notification
{
public:
    BaseNotification(void)
    {
        std::cout << "BaseNotification CTOR" << std::endl;
    }
    virtual ~BaseNotification(void)
    {
        std::cout << "BaseNotification DTOR" << std::endl;
    }

    BaseNotification(BaseNotification const& rhs)
    {
        std::cout << "BaseNotification COPY CTOR" << std::endl;
    }
    BaseNotification& operator=(BaseNotification const& rhs)
    {
        std::cout << "BaseNotification COPY operator" << std::endl;
        return *this;
    }

    BaseNotification(BaseNotification&& rhs)
    {
        std::cout << "BaseNotification MOVE CTOR" << std::endl;
    }
    BaseNotification& operator=(BaseNotification&& rhs)
    {
        std::cout << "BaseNotification MOVE operator" << std::endl;
        return *this;
    }
};

class SubNotification : public BaseNotification
{
public:
    SubNotification(void)
    {
        std::cout << "SubNotification CTOR" << std::endl;
    }
    virtual ~SubNotification(void)
    {
        std::cout << "SubNotification DTOR" << std::endl;
    }

    SubNotification(SubNotification const& rhs)
    {
        std::cout << "SubNotification COPY CTOR" << std::endl;
    }
    SubNotification& operator=(SubNotification const& rhs)
    {
        std::cout << "SubNotification COPY operator" << std::endl;
        return *this;
    }

    SubNotification(SubNotification&& rhs)
    {
        std::cout << "SubNotification MOVE CTOR" << std::endl;
    }
    SubNotification& operator=(SubNotification&& rhs)
    {
        std::cout << "SubNotification MOVE operator" << std::endl;
        return *this;
    }
};


class Target
{
public:
    Target(void)
    {
        std::cout << "Target CTOR" << std::endl;
    }
    virtual ~Target(void)
    {
        std::cout << "Target DTOR" << std::endl;
    }

    Target(Target const& rhs)
    {
        std::cout << "Target COPY CTOR" << std::endl;
    }
    Target& operator=(Target const& rhs)
    {
        std::cout << "Target COPY operator" << std::endl;
        return *this;
    }

    Target(Target&& rhs)
    {
        std::cout << "Target MOVE CTOR" << std::endl;
    }
    Target& operator=(Target&& rhs)
    {
        std::cout << "Target MOVE operator" << std::endl;
        return *this;
    }

    void handleBase(BaseNotification* baseNotification)
    {
        std::cout << "Target::handleBase: " << baseNotification->name() << std::endl;
        baseNotification->release(); // we got ownership?!?! so we must release
    }

    void handleSub(const AutoPtr<SubNotification>& subNotification)
    {
        std::cout << "Target::handleSub: " << subNotification->name() << std::endl;
    }
};

#endif //TESTINGCPP_POCONOTIFICATIONEXAMPLE_HPP