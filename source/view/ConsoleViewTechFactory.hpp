#ifndef KLONDIKE_CONSOLEVIEWTECHFACTORY_HPP
#define KLONDIKE_CONSOLEVIEWTECHFACTORY_HPP

#include "ViewTechAbstractFactory.hpp"

namespace com::jlom::klondike::view
{
    class ConsoleViewTechFactory : public ViewTechAbstractFactory
    {
    public:
        ViewTechImplementation* GetViewTechnology() override ;
    };
}

#endif //KLONDIKE_CONSOLEVIEWTECHFACTORY_HPP
