#ifndef KLONDIKE_NOTIFICATIONBASE_HPP
#define KLONDIKE_NOTIFICATIONBASE_HPP

#include <Poco/Notification.h>

namespace com::jlom::klondike
{
    class NotificationBase : public Poco::Notification
    {

    };
}

#endif //KLONDIKE_NOTIFICATIONBASE_HPP
