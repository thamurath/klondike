#include "WasteView.hpp"

#include "StatusDecoder.hpp"

#include <common/Status.hpp>

#include <spdlog/spdlog.h>

#include <sstream>

namespace com::jlom::klondike::view
{
    WasteView::WasteView(std::string id)
            : OutView(id)
    {

    }

    void WasteView::DecodeStatus(const StatusDecoder &statusDecoder, Status const &status)
    {
        spdlog::get("console")->trace("WasteView::DecodeStatus id:{} ", GetId());
        cards = statusDecoder.Decode(GetId(), status);
        spdlog::get("console")->trace("WasteView::DecodeStatus cards {} ", cards.empty());
    }

    void WasteView::Show(void)
    {
        spdlog::get("console")->trace("WasteView::Show id{}", GetId());
        std::stringstream ss;

        ss << "id:" << GetId()
           << ",TopCard:" ;
        if (cards.empty())
        {
            ss << "empty";
        }
        else
        {
            ss << cards.back();
        }
        ss << ",numCards:" << std::size(cards);

        GetView().ShowText(ss.str());
        spdlog::get("console")->trace("WasteView::Show id{} ", GetId());
    }
}
