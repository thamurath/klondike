#ifndef KLONDIKE_CONTROLLER_ACTIONHANDLERCONTROLLER_HPP
#define KLONDIKE_CONTROLLER_ACTIONHANDLERCONTROLLER_HPP

namespace com::jlom::klondike::controller
{
    template <typename ModelInterface>
    class ActionHandleController
    {
    public:
        explicit ActionHandleController(ModelInterface const& modelInterface);
        // avoid copy
        ActionHandleController(ActionHandleController const&) = delete;
        ActionHandleController& operator=(ActionHandleController const&) = delete;

        // avoid move
        ActionHandleController(ActionHandleController&&) = delete;
        ActionHandleController& operator=(ActionHandleController&&) = delete;
    protected:
    private:
        ModelInterface const& modelInterface;
    };
}

#include "src/ActionHandlerController_impl.hpp"

#endif //KLONDIKE_CONTROLLER_ACTIONHANDLERCONTROLLER_HPP
