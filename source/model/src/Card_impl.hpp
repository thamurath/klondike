

#ifndef KLONDIKE_MODEL_CARD_IMPL_HPP
#define KLONDIKE_MODEL_CARD_IMPL_HPP

#ifndef _KLONDIKE_MODEL_CARD_HPP__
#error "should NOT include this file"
#endif


namespace com::jlom::klondike::model
{

    template <typename SpecificCard>
    Card<SpecificCard>::Card(void)
    : face(FACE::DOWN)
    {

    }
    template <typename SpecificCard>
    Card<SpecificCard>::~Card(void)
    {

    }

    template <typename SpecificCard>
    bool Card<SpecificCard>::IsFaceUp(void) const
    {
        return (FACE::UP == face._value);
    }

    template <typename SpecificCard>
    void Card<SpecificCard>::TurnUp(void)
    {
        face = FACE::UP;
    }

    template <typename SpecificCard>
    void Card<SpecificCard>::TurnDown(void)
    {
        face = FACE::DOWN;
    }

    template <typename SpecificCard>
    std::ostream& operator<< (std::ostream& os, com::jlom::klondike::model::Card<SpecificCard> const& card)
    {
        os << static_cast<SpecificCard>(card);
        return os;
    }
}//namespace com::jlom::klondike::model

#endif //KLONDIKE_CARD_IMPL_HPP
