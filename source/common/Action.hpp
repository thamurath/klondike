#ifndef KLONDIKE_CONTROLLER_ACTION_HPP
#define KLONDIKE_CONTROLLER_ACTION_HPP

#include <better_enums.hpp>

#include <string>
#include <cstdlib>

namespace com::jlom::klondike
{
    BETTER_ENUM(ACTION_TYPE, std::uint32_t
                , REDEAL
                , PILE_TO_PILE
                , DECK_TO_WASTE
                , WASTE_TO_PILE
                , WASTE_TO_FOUNDATION
                , PILE_TURNUP_CARD
                , PILE_TO_FOUNDATION
                , RESIGN
                , REQUEST_SCORE
                , REQUEST_GAME_STATUS
    );

    class Action
    {


    public:
        Action(ACTION_TYPE type, std::string const& sourceId, std::string const& destinationID, std::size_t const& numCards);

        std::string GetSourceId(void) const;
        std::string GetDestinationId(void) const;
        std::size_t GetNumCards(void) const;
        ACTION_TYPE GetType(void) const;
    protected:
    private:
        const std::string sourceId;
        const std::string destinationId;
        const std::size_t numCards;
        ACTION_TYPE type;
    };
}

#endif //KLONDIKE_CONTROLLER_ACTION_HPP
