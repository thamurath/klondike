#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#include "ChainFactory.hpp"
#include "ControllerFactory.hpp"
#include "GeneralMoveController.hpp"
#include "ChainableController.hpp"
#include "InformationHub.hpp"

#include <model/Table.hpp>
#include <model/english_model/Card.hpp>
#include <model/Deck.hpp>
#include <model/Pile.hpp>
#include <model/Foundation.hpp>
#include <model/Waste.hpp>
#include <model/DefaultFactory.hpp>

#include <common/JsonStatusEncoderFactory.hpp>
#include <common/Action.hpp>
#include <common/StatusNotification.hpp>
#include <common/ObserverAdapter.hpp>

#include "test_dummy_policies.hpp"
#include "test_dummyTable.hpp"

#include "type_name.hpp"
#include "spdlog/spdlog.h"

#include <iostream>
#include <string>
#include <optional>
#include <ostream>
#include <exception>

namespace com::jlom::klondike::controller::unittest
{
    class Card
    {

    };
}

class Target
{
public:
    void HandleStatusNotification(com::jlom::klondike::StatusNotification* notification)
    {
        std::cout << "Status: " << notification->GetStatus().str() << std::endl;
    }
};

TEST_CASE("GenericMoveController")
{
    namespace common = com::jlom::klondike;
    namespace model = common::model;
    namespace controller = common::controller;
    namespace test = controller::unittest;

    auto console = spdlog::stdout_color_mt("console");
    console->set_level(spdlog::level::debug);
    console->flush_on(spdlog::level::debug);
    console->debug("Entering test case");
    console->error("Entering test case");
    spdlog::get("console")->info("testing logger");

    using EnglishModelFactory = model::DefaultFactory<model::english_model::Card, common::JsonStatusEncoder, controller::InformationHub>; // aqui estamos poniendo un tipo determinado.

    using StatusEncodingFactory = common::JsonStatusEncoderFactory;
    StatusEncodingFactory statusEncodingFactory;
    auto encoder = statusEncodingFactory.CreateStatusEncoder();
    controller::InformationHub informationHub;

    EnglishModelFactory modelFactory;
    auto table = modelFactory.CreateTable(encoder, &informationHub);

    Target target;

    informationHub.AddObserver(common::ObserverAdapter<Target,common::StatusNotification>(target,&Target::HandleStatusNotification));

    table.EncodeStatus();


    controller::ControllerFactory controllerFactory(table);
    controller::ChainFactory chainFactory(controllerFactory);

    auto handler = chainFactory.Create<common::ACTION_TYPE>();

    std::cout << debug::type_name<decltype(handler)>() << std::endl;

   
    common::Action p2pAction(common::ACTION_TYPE::PILE_TO_PILE,"pile_7", "pile_2", 5);

    try {
        std::cout << (*handler)(p2pAction) << std::endl;
    }
    catch (model::exceptions::ElementNotFound& e)
    {
        console->critical(e.what());
    }
    catch (model::exceptions::NonTransactionalItem& e)
    {
        console->critical(e.what());
    }
    catch (std::exception& e)
    {
        console->critical(e.what());
    }
}
