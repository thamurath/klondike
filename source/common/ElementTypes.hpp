#ifndef KLONDIKE_ELEMENTTYPES_HPP
#define KLONDIKE_ELEMENTTYPES_HPP

#include <better_enums.hpp>

#include <cinttypes>
namespace com::jlom::klondike
{
    BETTER_ENUM(ElementType,
                std::uint8_t,
                PILE,
                WASTE,
                FOUNDATION,
                DECK,
                NOT_APLICABLE
    );
}

#endif //KLONDIKE_ELEMENTTYPES_HPP
