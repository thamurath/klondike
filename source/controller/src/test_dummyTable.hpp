#ifndef KLONDIKE_CONTROLLER_UNITTEST_DUMMYTABLE_HPP
#define KLONDIKE_CONTROLLER_UNITTEST_DUMMYTABLE_HPP

#include <cstdlib>
#include <iterator>
#include <unordered_map>
#include <string>
#include <ostream>
#include <optional.hpp>

namespace com::jlom::klondike::controller::unittest
{

    class State final
    {
    public:
        explicit State(std::uint32_t st)
                : st(st)
        {

        }

        auto GetState(void) const
        {
            return st;
        }
    private:
        std::uint32_t st;
    };

    template <typename Card>
    class Table
    {
    private:
        static std::uint32_t counter;
        using StateTable  = std::unordered_map<std::string, State>;
        StateTable stateTable;

        using CounterTable = std::unordered_map<std::string, std::uint32_t>;
        CounterTable beginCounter;
        CounterTable commitCounter;
        CounterTable rollbackCounter;

        CounterTable doubleBeginCounter;
        CounterTable commitNoStateCounter;
        CounterTable rollbackNoStateCounter;

        CounterTable okBeginCounter;
        CounterTable okCommitCounter;
        CounterTable okRollbackCounter;


        using CounterList = std::unordered_map<std::string, CounterTable&>;
        CounterList counterList;

        using CardStorage = std::vector<Card>;
        CardStorage cardStorage;


        void incrementCounter(CounterTable& table, std::string const& id)
        {
            auto search = table.find(id);
            if (std::end(table) == search)
            {
                table.insert({id, 1});
            }
            else
            {
                ++(search->second);
            }
        }

        template<typename T>
        friend std::ostream& operator<<(std::ostream& os,Table<T> const& table );
    public:

        static const std::string TOTAL_BEGIN_COUNTER;
        static const std::string DOUBLE_BEGIN_COUNTER;
        static const std::string OK_BEGIN_COUNTER;
        static const std::string TOTAL_COMMIT_COUNTER;
        static const std::string NO_STATE_COMMIT_COUNTER;
        static const std::string OK_COMMIT_COUNTER;
        static const std::string TOTAL_ROLLBACK_COUNTER;
        static const std::string NO_STATE_ROLLBACK_COUNTER;
        static const std::string OK_ROLLBACK_COUNTER;

        Table(void)
                : counterList {
                  {TOTAL_BEGIN_COUNTER, beginCounter}
                , {OK_BEGIN_COUNTER, okBeginCounter}
                , {DOUBLE_BEGIN_COUNTER, doubleBeginCounter}
                , {TOTAL_COMMIT_COUNTER, commitCounter}
                , {OK_COMMIT_COUNTER, okCommitCounter}
                , {NO_STATE_COMMIT_COUNTER, commitNoStateCounter}
                , {TOTAL_ROLLBACK_COUNTER, rollbackCounter}
                , {OK_ROLLBACK_COUNTER, okRollbackCounter}
                , {NO_STATE_ROLLBACK_COUNTER, rollbackNoStateCounter}
        }
        {

        }

        CounterTable&  GetCounterTable(std::string const& counterId) const
        {
            return counterList.find(counterId)->second;
        }

        State Begin(std::string const& id)
        {
            incrementCounter(beginCounter, id);
            auto search = stateTable.find(id);
            if (std::end(stateTable) != search)
            {
                incrementCounter(doubleBeginCounter, id);
                return search->second;
            }
            else
            {
                incrementCounter(okBeginCounter,id);
                auto ret = stateTable.insert({id, State{++counter}});
                return (ret.first)->second;
            }

        }
        void Commit(std::string const& id)
        {
            incrementCounter(commitCounter, id);
            auto search = stateTable.find(id);
            if (std::end(stateTable) == search)
            {
                incrementCounter(commitNoStateCounter, id);
            }
            else
            {
                stateTable.erase(search);
                incrementCounter(okCommitCounter,id);
            }

        }

        void Rollback(std::string const& id, State&& state)
        {
            incrementCounter(rollbackCounter, id);
            auto search = stateTable.find(id);
            if (std::end(stateTable) == search)
            {
                incrementCounter(rollbackNoStateCounter, id);
            }
            else
            {
                stateTable.erase(search);
                incrementCounter(okRollbackCounter,id);
            }
        }

        std::int32_t Push(std::string const& id, Card&& card)
        {
            cardStorage.push_back(std::move(card));
            return 0;
        }

        std::optional<Card> Get(std::string const& id) const
        {
            if (!cardStorage.empty())
            {
                return cardStorage.back();
            }
            return {};
        }
        void Pop(std::string const& id)
        {
            if (!cardStorage.empty())
            {
                cardStorage.pop_back();
            }
        }
    };

    template<typename Card>
    std::uint32_t Table<Card>::counter = 0;
    template<typename Card>
    const std::string Table<Card>::TOTAL_BEGIN_COUNTER = "beginCounter";
    template<typename Card>
    const std::string Table<Card>::DOUBLE_BEGIN_COUNTER = "doubleBeginCounter";
    template<typename Card>
    const std::string Table<Card>::OK_BEGIN_COUNTER = "okBeginCounter";
    template<typename Card>
    const std::string Table<Card>::TOTAL_COMMIT_COUNTER = "commitCounter";
    template<typename Card>
    const std::string Table<Card>::NO_STATE_COMMIT_COUNTER = "commitNoStateCounter";
    template<typename Card>
    const std::string Table<Card>::OK_COMMIT_COUNTER = "okCommitCounter";
    template<typename Card>
    const std::string Table<Card>::TOTAL_ROLLBACK_COUNTER = "rollbackCounter";
    template<typename Card>
    const std::string Table<Card>::NO_STATE_ROLLBACK_COUNTER = "rollbackNoStateCounter";
    template<typename Card>
    const std::string Table<Card>::OK_ROLLBACK_COUNTER = "okRollbackCounter";

    template<typename Card>
    std::ostream& operator<<(std::ostream& os,Table<Card> const& table )
    {
        for (auto const& state : table.stateTable)
        {
            os << "id: " << state.first << ", ";
        }
        os << '\n';
        for (auto const& counter : table.counterList)
        {
            os << counter.first << ":\n";
            for (auto const& n : counter.second)
            {
                os << '\t' << n.first << " = " << n.second << '\n';
            }
        }
        return os;
    }

}
#endif //KLONDIKE_CONTROLLER_UNITTEST_DUMMYTABLE_HPP
