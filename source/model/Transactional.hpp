#ifndef __KLONDIKE_MODEL_TRANSACTIONAL_HPP__
#define __KLONDIKE_MODEL_TRANSACTIONAL_HPP__

namespace com::jlom::klondike::model
{
    template <typename T>
    class Transactional
    {
    public:
        virtual ~Transactional(void) = default;

        auto Begin(void) const
        {
            return asDerived().begin();
        }

        void Commit(void)
        {
            asDerived().commit();
        }

        template <typename state_type>
        void Rollback(state_type const& state)
        {
            asDerived().rollback(state);
        }

    private:

        constexpr T& asDerived(void)
        {
            return *static_cast<T*>(this);
        }

        constexpr T const& asDerived(void) const
        {
            return *static_cast<T const*>(this);
        }
    };

}//namespace model

#endif //__KLONDIKE_MODEL_TRANSACTIONAL_HPP__
