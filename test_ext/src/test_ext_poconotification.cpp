
#define CATCH_CONFIG_MAIN
#include <catch.hpp>


#include "PocoNotificationExample.hpp"



TEST_CASE("PocoNotificationCenter", "poco")
{
    using Poco::NotificationCenter;
    using Poco::Notification;
    using Poco::Observer;
    using Poco::NObserver;
    using Poco::AutoPtr;

    NotificationCenter notificationCenter;

    std::cout << "1" << std::endl;
    Target target;

    std::cout << "2" << std::endl;
    notificationCenter.addObserver(Observer<Target, BaseNotification>(target, &Target::handleBase));
    std::cout << "3" << std::endl;
    notificationCenter.addObserver(NObserver<Target, SubNotification>(target, &Target::handleSub));
    std::cout << "4" << std::endl;
    notificationCenter.postNotification(new BaseNotification);
    std::cout << "5" << std::endl;
    notificationCenter.postNotification(new SubNotification);
    std::cout << "6" << std::endl;

    notificationCenter.removeObserver(Observer<Target, BaseNotification>(target, &Target::handleBase));
    std::cout << "7" << std::endl;
    notificationCenter.removeObserver(NObserver<Target, SubNotification>(target, &Target::handleSub));
    std::cout << "8" << std::endl;
    return ;
}