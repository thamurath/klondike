#ifndef KLONDIKE_SHOWSTATUSCONTROLLER_HPP
#define KLONDIKE_SHOWSTATUSCONTROLLER_HPP

#include "ChainableController.hpp"

#include <common/Action.hpp>

#include <cstdint>

namespace com::jlom::klondike::controller
{
    template < typename ModelInterface>
    class ShowStatusController : public ChainableController
    {
    public:
        ShowStatusController(ModelInterface& modelInterface, ACTION_TYPE )
        : modelInterface(modelInterface)
        , type(ACTION_TYPE::REQUEST_GAME_STATUS)
        {

        }
    private:
        ModelInterface& modelInterface;
        const ACTION_TYPE type;

        bool shouldHandle(Action const& action )  override
        {
            spdlog::get("console")->debug("Should Handle: type:{}, action:{}, ret:{}", type._to_string(), action.GetType()._to_string(), (type==action.GetType()));
            return (type == action.GetType());
        }

        bool canHandle(Action const& action) override
        {
            spdlog::get("console")->debug("CanHandle: type:{}, action:{}, ret:{}", type._to_string(), action.GetType()._to_string(), (type==action.GetType()));
            return true;
        }

        std::int32_t handle(Action const& action ) override
        {
            spdlog::get("console")->info("handle: type:{}, action:{}", type._to_string(), action.GetType()._to_string());
            return modelInterface.EncodeStatus();
        }
    };
}

#endif //KLONDIKE_SHOWSTATUSCONTROLLER_HPP
