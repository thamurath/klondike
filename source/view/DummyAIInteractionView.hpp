#ifndef KLONDIKE_DUMMYAIINTERACTIONVIEW_HPP
#define KLONDIKE_DUMMYAIINTERACTIONVIEW_HPP

#include "PlayerInteraction.hpp"

#include <string>
namespace com::jlom::klondike::view
{
    class DummyAIInteractionView : public PlayerInteraction
    {
    public:
        explicit DummyAIInteractionView(std::string const& id);
        Command GetCommand() override ;

    private:
        std::uint32_t GetNumber(std::uint32_t const& min, std::uint32_t const& max) override ;
    };
}

#endif //KLONDIKE_DUMMYAIINTERACTIONVIEW_HPP
