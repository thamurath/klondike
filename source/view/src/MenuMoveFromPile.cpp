#include "MenuMoveFromPile.hpp"

#include <spdlog/spdlog.h>
namespace com::jlom::klondike::view
{
    MenuItem::Options MenuMoveFromPile::GetOptions(void)
    {
        spdlog::get("console")->debug("Entering MenuMoveFromPile::GetOptions");
        MenuItem::Options options;

        options.showText = R"(
Menu Move from Pile
1.- Foundation
2.- Pile
3.- Reveal Card
 )";

        options.minOption = 1;
        options.maxOption = 3;

        return options;
    }

    void MenuMoveFromPile::DecodeOption(MenuContext& menuContext)
    {
        //nothing to do ...
        spdlog::get("console")->debug("Entering MenuMoveFromPile::DecodeOptions");
    }
}

