#ifndef KLONDIKE_CONTROLLER_MOVECONTROLLER_HPP
#define KLONDIKE_CONTROLLER_MOVECONTROLLER_HPP

#include "src/ScopedState.hpp"


#include "ChainableController.hpp"

#include "type_name.hpp"
#include <common/Action.hpp>
#include <common/ReturnCodes.hpp>
#include <spdlog/spdlog.h>

#include <vector>
#include <cstdlib>
#include <algorithm>



namespace com::jlom::klondike::controller
{
    /**@brief Class to handle move operations
     *
     * @tparam ModelInterface: Interface to order operations to the Model
     * @tparam PreCheckPolicy: Will be called before the operation to check everything is ok.
     * @tparam PostCheckPolicy: Will be called after all the operation has been done to check everythins is ok.
     * @tparam SourceCheckPolicy: Will be called before every GET operation to the source to ensure get/pop operation is safe.
     * @tparam DestinationCheckPolicy: Will be called before any PUSH operation to the destination to ensure it is safe.
     * @tparam OperationPolicy: Each card from source will be handled to this policy and it should return a new one.
     * @tparam TransferenceOrderPolicy: A container with all the cards will be handle to this policy before moving them to destination
     *
     *
     * As concepts are not yet here ... I have to detail the interface expected from each policy in comments.
     *
     * ModelInterface: @see model::Table
     *
     * @note: If any of the "check" policies return a non-zero value operation will be cancelled and everything will be undone.
     *
     * @tparam PreCheckPolicy: std::int32_t CheckPreAction(ModelInterface& , controller::Action const& );
     *                 0 if everything is ok != 0 otherwise
     *
     * @tparam SourceCheckPolicy:  std::int32_t CheckBeforeGet(ModelInterface& , std::string const& sourceId);
     *                 0 if everything is ok != 0 otherwise
     *
     * @tparam DestinationCheckPolicy:  std::int32_t CheckBeforePush(ModelInterface& , std::string const& destinationId, ModelInterface::CardModelType const& card);
     *                 0 if everything is ok != 0 otherwise
     *
     * @tparam OperationPolicy:  ModelInterface::CardModelType Transform(ModelInterface::CardModelType&& card);
     *                 Every card getted from the source will be handle to this policy for it to transform it at will.
     *                 The card will be MOVED into the method and method should return a new one or modify and MOVE the original.
     *                 This is intened to turnup or down cards when moving from deck to waste for example
     *
     * @tparam TransferenceOrderPolicy: void ChangeOrder(STD_ContainerType& cards);
     *                 Once all the cards have been obtained from Source but before starting to push them into destination
     *                 a modificable container with all of then will be handled to this method.
     *
     *                 This method is mainly intended for those very special cases where card have to be reordered (reverded)
     *                 before pushin them into the source. Almost all the cases will do nothing here.
     */
    template < typename ModelInterface
            , typename PreCheckPolicy
            , typename SourceCheckPolicy
            , typename DestinationCheckPolicy
            , typename OperationPolicy
            , typename TransferenceOrderPolicy
    >
    class GenericMoveController : public ChainableController
            , PreCheckPolicy
            , SourceCheckPolicy
            , DestinationCheckPolicy
            , OperationPolicy
            , TransferenceOrderPolicy
    {
    public:
        GenericMoveController(ModelInterface& table,ACTION_TYPE type)
        : type(type)
        , table(table)
        {

        }

        virtual ~GenericMoveController(void) = default;

        // deny copy
        GenericMoveController( GenericMoveController const&) = delete;
        GenericMoveController& operator=( GenericMoveController const&) = delete;


        // ChainableController interface
        bool shouldHandle(Action const& action ) override
        {
            spdlog::get("console")->info("Should Handle: type:{}, action:{}, ret:{}", type._to_string(), action.GetType()._to_string(), (type==action.GetType()));
            return (type == action.GetType());
        }

        bool canHandle(Action const& action ) override
        {
            spdlog::get("console")->info("CanHandle: type:{}, action:{}, ret:{}", type._to_string(), action.GetType()._to_string(), (type==action.GetType()));
            if (!shouldHandle(action))
            {
                return false;
            }

            const auto preCheck (PreCheckPolicy::CheckPreAction(table, action) );
            const auto  sourceCheck ( SourceCheckPolicy::CheckBeforeGet(table, action.GetSourceId(), action.GetNumCards()) );
            if ((SUCCESS != preCheck) || (SUCCESS != sourceCheck))
            {
                spdlog::get("console")->error("CanHandle: preCheck:{}, sourceCheck:{}", preCheck, sourceCheck);
                return false;
            }
            auto cards = table.GetCards(action.GetSourceId(), action.GetNumCards());
            decltype(cards) transformedCards;
            for (auto card : cards)
            {
                transformedCards.push_back(OperationPolicy::Transform(std::move(card)));
            }
            TransferenceOrderPolicy::ChangeOrder(transformedCards);
            const auto destinationCheck ( DestinationCheckPolicy::CheckBeforePush(table, action.GetDestinationId(), transformedCards) );
            if (SUCCESS != destinationCheck)
            {
                spdlog::get("console")->error("CanHandle: destinationCheck:{}", destinationCheck);
            }
            return (SUCCESS == destinationCheck);
        }

        std::int32_t handle(Action const& action ) override
        {
            spdlog::get("console")->info("handle: type:{}, action:{}", type._to_string(), action.GetType()._to_string());
            //Open "transaction" on source and destination
            ScopedState sourcePreState(table,action.GetSourceId(), table.Begin(action.GetSourceId()) );
            ScopedState destinationPreState(table,action.GetDestinationId(), table.Begin(action.GetDestinationId()) );

            auto cards = ExtractCardsFromSource(action.GetSourceId(), action.GetNumCards());
            auto transformedCards = TransformCards(std::move(cards));
            TransferenceOrderPolicy::ChangeOrder(transformedCards);
            PutCardsInDestination(action.GetDestinationId(), cards);

            destinationPreState.Deactivate();
            sourcePreState.Deactivate();

            return SUCCESS;
        }
    protected:
    private:

        auto ExtractCardsFromSource(std::string const& sourceId, std::size_t const& numCards)
        {
            std::vector<typename ModelInterface::CardModelType> cards;
            cards.reserve(numCards);

            for (std::size_t idx(0); idx < numCards;  ++idx)
            {
                cards.push_back(*table.Get(sourceId));
                table.Pop(sourceId);
            }
            return cards;
        }

        auto TransformCards(auto&& cards)
        {
            std::vector<typename ModelInterface::CardModelType> transformedCards;
            transformedCards.reserve(std::size(cards));

            for (auto&& card : cards)
            {
                //Temporary store the cards after some additional handling
                auto transformedCard = OperationPolicy::Transform(std::move(card));
                transformedCards.push_back(std::move(transformedCard));
            }

            return transformedCards;
        }

        void PutCardsInDestination(std::string const& destinationId, auto && cards)
        {
            for (auto&& card : cards)
            {
                table.Push(destinationId, std::move(card));
            }
        }

        const ACTION_TYPE type;
        ModelInterface& table;
    };
}

#endif //KLONDIKE_CONTROLLER_MOVECONTROLLER_HPP
