#ifndef KLONDIKE_MENUREVEALFILLIN_HPP
#define KLONDIKE_MENUREVEALFILLIN_HPP

#include "MenuFillIn.hpp"

namespace com::jlom::klondike::view
{
    class MenuRevealFillIn : public MenuFillIn
    {
        void FillIn(MenuContext& menuContext) override
        {
            ///@jlom TODO Aqui tendria que tener la informacion de los id de los distintos tipos.
            menuContext.Builder().SetDestination(*menuContext.Builder().GetSource());
            menuContext.Builder().SetDestinationType(ElementType::PILE);
            menuContext.Builder().SetNumberOfCards(1);
        }
    };
}

#endif //KLONDIKE_MENUREVEALFILLIN_HPP
