#ifndef KLONDIKE_COMMAND_HPP
#define KLONDIKE_COMMAND_HPP

#include "ElementTypes.hpp"

#include <better_enums.hpp>

#include <string>
#include <ostream>

namespace com::jlom::klondike
{
    namespace view
    {
        class CommandBuilder;
    }

    BETTER_ENUM(CommandType, std::uint8_t,
                MOVE,
                RESIGN,
                REDEAL,
                SHOW_STATUS
    );

    class Command
    {
    public:
        CommandType const& GetCommandType(void) const;
        std::string const& GetSourceId(void) const;
        std::string const& GetDestinationId(void) const;
        ElementType const& GetSourceType(void) const;
        ElementType const& GetDestinationType(void) const;
        std::size_t const& GetNumCards(void) const;
    private:
        friend class view::CommandBuilder;
        Command(const CommandType &type
                , std::string const &srcId, ElementType const &srcType
                , std::string const &dstId, ElementType const &dstType
                , std::size_t numCards);

        CommandType type;
        std::string srcId;
        std::string dstId;
        std::size_t numCards;
        ElementType srcType;
        ElementType dstType;

    };

    std::ostream& operator<<(std::ostream& os, Command const& cmd);
}

#endif //KLONDIKE_COMMAND_HPP
