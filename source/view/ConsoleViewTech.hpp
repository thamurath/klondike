#ifndef KLONDIKE_CONSOLEVIEWTECH_HPP
#define KLONDIKE_CONSOLEVIEWTECH_HPP

#include "ViewTechImplementation.hpp"

namespace com::jlom::klondike::view
{
    class ConsoleViewTech : public ViewTechImplementation
    {
    public:
        std::string GetInput(void) override;
        void ShowText(std::string const& text) override ;
        std::uint32_t GetNumber(const uint32_t &min, const uint32_t &max) override ;
    };
}

#endif //KLONDIKE_CONSOLEVIEWTECH_HPP
